<?php
include ("access.php");
include 'includes/top.php';
include("main_includes/conf.class.php");
//$hotel = mysql_fetch_array(mysql_query("select theme_color,hotel_cover_image from bsi_hotels where hotel_id=".$_SESSION['hotel_id']));
?>
    <div id="wrapper">
<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->
                <div class="heading">
                    <h3>Payment Gateways</h3>

                </div><!-- End .heading-->
                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">
        <?php if(isset($_SESSION['val1'])): ?>
            <div class="row-fluid">
                <span class="span12">
                    <div class="alert marginT10">
                        <button class="close" data-dismiss="alert" type="button">&times;</button>
                        <strong>MESSAGE!</strong>
                        <?php echo $_SESSION['val1']; unset($_SESSION['val1']); ?>
                    </div>
                </span>
            </div>
        <?php endif; ?>
                            <div class="box">
                                <div class="title">
                                    <h4>
                                        <span class="icon16 "></span>
                                        <span>Payment Gateways</span>
                                    </h4>

                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                    <form style="margin-bottom:0px !important" enctype="multipart/form-data" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                        <?php $q = mysql_query("select * from bsi_payment_gateway where hotel_id=".$_SESSION['hotel_id']); ?>
                                        <?php while($r=mysql_fetch_array($q)): ?>
                                            <?php if($r['gateway_code'] == "ipay88"): ?>
                                                <?php $ipay88Acc = ($r['account']=="")? NULL : explode("||", $r['account']); ?>
                                                <div class="form-row row-fluid">
                                                    <div class="span12">
                                                        <div class="row-fluid">
                                                            <label class="form-label span3" for="normal"><?=$r['gateway_name']?></label>
                                                            <input class="styled tipB" name="ipay88_on" title="Enable/Disable" <?=($r['enabled']==1)? 'checked="checked"' : ''?> type="checkbox" value="ipay88" />
                                                            <input class="span3 tipB" style="display:inline;" value="<?=$ipay88Acc[0]?>" name="merchant_code" type="text" title="iPay88 Merchant Code" />
                                                            <input class="span3 tipB" style="display:inline;" value="<?=$ipay88Acc[1]?>" name="merchant_key" type="text" title="iPay88 Merchant Key" />
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php elseif($r['gateway_code']=="manual"): ?>
                                                <div class="form-row row-fluid">
                                                    <div class="span12">
                                                        <div class="row-fluid">
                                                            <label class="form-label span3" for="normal"><?=$r['gateway_name']?></label>
                                                            <input class="styled tipB" name="manual_on" title="Enable/Disable" <?=($r['enabled']==1)? 'checked="checked"' : ''?> type="checkbox" value="1" />
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <div class="form-actions" style="margin-bottom:0px">
                                                    <div class="span3"></div>
                                                    <div class="span9 controls">
                                                      <button type="submit" class="btn right marginR10" name="savePaymentGateways" id="sbt_details"><?php echo SUBMIT;?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- End .span6 -->
                    </div><!-- End .row-fluid -->
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    </div><!-- End #wrapper -->

    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>
    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>
    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->
    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript">
    $(function(){
    })
    </script>
    </body>
</html>
