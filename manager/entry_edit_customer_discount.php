<?php
include("access.php");
// if(isset($_POST['sbt_details'])){
// 	include("main_includes/db.conn.php");
// 	include("main_includes/conf.class.php");
//     include("main_includes/admin.class.php");
// 	$bsiAdminMain->add_edit_spe_off();
// 	header("location:view_special_offer.php");
// 	exit;
// }
include 'includes/top.php';
include("main_includes/conf.class.php");
include("main_includes/admin.class.php");
/*
 if(isset($_GET['id']))
 {
   $id=$_GET['id'];
   if($id!='0')
   {
	  $row1 = mysql_fetch_assoc(mysql_query("SELECT * from promotions where promo_id='$id' order by promo_id desc"));
      $start = date("m/d/Y",strtotime($row1['start_date']));
      $end = date("m/d/Y",strtotime($row1['end_date']));
   } else { $row1=NULL; $start=NULL; $end=NULL;}

 }
 */
?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3>Add User</h3>


                </div><!-- End .heading-->

                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">

                            <div class="box">

                                <div class="title">
                                    <h4>
                                        <span class="icon16 "></span>
                                        <span>Add User</span>
                                    </h4>
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                    <form style="margin-bottom:0px !important" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                        <input type="hidden" value="<?php //echo $id; ?>" name="id"  />
                                        <input type="hidden" value="<?php echo $_SESSION['hotel_id']; ?>" name="hotel_id"  />
										<div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Title</label>
                                                    <select  name="title">
														<option value="Mr.">Mr.</option>
														<option value="Mrs.">Mrs.</option>
														<option value="Miss">Miss</option>
													</select>
                                                </div>
                                            </div>
                                        </div>
										<div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" style="float:left;">Name</label>
                                                    <input class="span2"  name="first_name" type="text" placeholder="First name" style="float:left;margin-right: 1%;" required />
													<input class="span2"  name="last_name" type="text" placeholder="First name" style="float:left" />
                                                </div>
                                            </div>
                                        </div>
										<div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Email</label>
                                                    <input class="span4"  name="email" type="text" required />
                                                </div>
                                            </div>
                                        </div>
										<div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Password</label>
                                                    <input class="span4"  name="password" type="text" required />
                                                </div>
                                            </div>
                                        </div>
										<div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Address</label>
                                                    <input class="span4"  name="street_address" type="text" />
                                                </div>
                                            </div>
                                        </div>
										<div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
													<label class="form-label span3" style="float:left;">&nbsp;</label>
                                                    <input class="span2 offset3" name="city" type="text" placeholder="City" style="float:left;margin-right: 1%;" />
													<input class="span2" name="state" type="text" placeholder="Indonesia" style="float:left" />
                                                </div>
                                            </div>
                                        </div>
										<div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Phone</label>
                                                    <input class="span4" name="phone" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Select Room</label>
                                                    <select required name="room_id" style="opacity: 0;">
                                                        <option value="0" selected>All Rooms</option>
                                                        <?php

                                                        $roomq = mysql_query("select room_id,roomtype_id,capacity_id,room_rate from bsi_room where status=1 and hotel_id=".$_SESSION['hotel_id']);
                                                        while($room = mysql_fetch_array($roomq)){
                                                            if($room['room_id']==$row1['room_id']){$se="selected='selected'";}else{$se='';}
                                                            $cap = mysql_fetch_array(mysql_query("select title from bsi_capacity where id=".$room['capacity_id']));
                                                            $type = mysql_fetch_array(mysql_query("select type_name from bsi_roomtype where roomtype_id=".$room['roomtype_id']));
                                                            echo '<option '.$se.' value="'.$room['room_id'].'">'.$cap['title']." ".$type['type_name']." ($".$room['room_rate'].')</option>';
                                                        }
                                                        echo "</select>";
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Discount in Percentage</label>
                                                    <input class="span1 tipB" title="i.e 15" required name="amount" min="1" max="100" type="number" value="5" />
													<?=($id!=0)? "readonly='readonly'": ""?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row row-fluid">
											<div class="span12">
												<div class="row-fluid">
													<div class="form-actions" style="margin-bottom:0px">
													<div class="span3"></div>
													<div class="span9 controls">
													  <button type="submit" class="btn right marginR10" name="createCustomer" id="sbt_details"><?php echo SPECIAL_SUBMIT; ?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
													</div>
													</div>
												</div>
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- End .span6 -->
                    </div><!-- End .row-fluid -->
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    </div><!-- End #wrapper -->

    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    </body>
</html>
