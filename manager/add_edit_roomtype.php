<?php 
include("access.php");
if(isset($_POST['submitRoomtype'])){
	include("main_includes/db.conn.php"); 
	include("main_includes/conf.class.php");
	include("main_includes/admin.class.php");
	$bsiAdminMain->add_edit_roomtype();
	header("location:roomtype.php");	
	exit;
}

include 'includes/top.php';
include("main_includes/conf.class.php");
include("main_includes/admin.class.php");
if(isset($_GET['id']) && $_GET['id'] != ""){
	$id = $bsiCore->ClearInput($_GET['id']);
	if($id){
		$result = mysql_query($bsiAdminMain->getRoomtypesql($id));
		$row    = mysql_fetch_assoc($result);
		$readonly = 'readonly="readonly"';
	}else{
		$row    = NULL;
		$readonly = '';
	}
}else{
	header("location:admin_capacity.php");
	exit;
}
?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3><?php echo ROOM_TYPE_ADD_AND_EDIT; ?></h3>                    

                    <div class="resBtnSearch">
                        <a href="#"><span class="icon16 brocco-icon-search"></span></a>
                    </div>

                    <div class="search">

                        <form id="searchform" action="#" />
                            <input type="text" class="top-search" placeholder="Search here ..." />
                            <input type="submit" class="search-btn" value="" />
                        </form>
                
                    </div><!-- End search -->
                    
                    <ul class="breadcrumb">
                        <li>You are here:</li>
                        <li>
                            <a href="index.php" class="tip" title="back to dashboard">
                                <span class="icon16 icomoon-icon-screen"></span>
                            </a> 
                            <span class="divider">
                                <span class="icon16 icomoon-icon-arrow-right"></span>
                            </span>
                        </li>
                        <li class="active"><?php echo ROOM_TYPE_ADD_AND_EDIT; ?></li>
                    </ul>

                </div><!-- End .heading-->

                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">

 <?php 
if(isset($_GET['msg_type'])){
?>        
        <div class="alert marginT10 alert-<?php echo $_GET['msg_type'];?>">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong><?php echo strtoupper($_GET['msg_type']);?>!</strong>
            <?php echo $_GET['message'];?>
        </div>
<?php
}
?>
                            <div class="box">

                                <div class="title">

                                    <h4>
                                        <span class="icon16 "></span>
                                        <span><?php echo ROOM_TYPE_ADD_AND_EDIT; ?></span>
                                    </h4>
                                    
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                        <form style="margin-bottom:0px !important" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                          <input type="hidden" name="addedit" value="<?=$id?>">
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo ROOM_TYPE_TITLE;?></label>
                                                    <input class="span4" required name="roomtype_title" type="text" value="<?=$row['type_name']?>" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo DESCRIPTION_FACILITIES_TEXT;?></label>
                                                    <textarea rows="5" cols="3" required class="span8" name="description"><?php echo $row['description'];?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                                <div class="span12">
                                                    <div class="row-fluid">
                                                        <div class="form-actions" style="margin-bottom:0px">
                                                        <div class="span3"></div>
                                                        <div class="span9 controls">
                                                          <button type="submit" class="btn right marginR10" name="submitRoomtype" id="submitRoomtype"><?php echo ROOM_TYPE_SUBMIT; ?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                                                                

                                    </form>
                                </div>
                            </div>

                        </div><!-- End .span6 -->  
	
                    </div><!-- End .row-fluid -->

                
                
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>


    </body>
</html>
