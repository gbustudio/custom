<?php

/*
//booking_extra.php
define('BOOKING_EXTRA','Booking Extras');
define('ADD_NEW_EXTRA','Add New Extras');
define('EXTRAS_DESCRIPTION','Description');
define('EXTRAS_TITLE','Title');
define('EXTRAS_STATUS','Status');
define('EXTRAS_RATE','Rate');
define('EXTRAS_PER','Per');
define('TOTAL_ROOM','Total Room');
define('EDIT_BOOKING_EXTRA','Edit');
define('DELETE_BOOKING_EXTRA','Delete');
define('MAX_CHILD_PER_ROOM','Max Child / Room');
define('LEAVE_BLANK_IF_NONE_TEXT','leave blank if None!');

//add_edit_booking_extra.php
define('EXTRAS_ADD_AND_EDIT','Booking Extras Add/Edit');
define('NUMBER_OF_ROOM','Number of Room');
define('EXTRAS_DESCRIPTION_ADD_EDIT','Room Type');
define('NO_OF_ADULT','No of Adult');
define('ADD_EDIT_SUBMIT','Submit');
define('EXAMPLE','example');
define('ADD_EDIT_ROOM_SELECT','--- Select ---');
define('ADD_EDIT_CAPACITY_SELECT','--- Select ---');
define('SAME_COMBINATION_OF_ROOMTYPE_ALREADY_EXISTS_TEXT','Same Combination of Roomtype and capacity Already Exists.');

*/




include("access.php");
if(isset($_GET['disable']) and $_GET['disable']!="" and is_numeric($_GET['disable'])){
	include("main_includes/db.conn.php"); 
	include("main_includes/conf.class.php");
	include("main_includes/admin.class.php");
	mysql_query("update booking_extras set status=-1 where extras_id=".$_GET['disable']." limit 1");
	header("location: booking_extra.php");	
	exit;
}
include 'includes/top.php';
include("main_includes/admin.class.php");
?>
    <div id="wrapper">
<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->
                <div class="heading">
                    <h3><?php echo BOOKING_EXTRA;?></h3>                    
                    <div class="resBtnSearch">
                        <a href="#"><span class="icon16 brocco-icon-search"></span></a>
                    </div>
                    <div class="search">
                        <form id="searchform" action="#" />
                            <input type="text" class="top-search" placeholder="Search here ..." />
                            <input type="submit" class="search-btn" value="" />
                        </form>
                
                    </div><!-- End search -->
                    
                </div><!-- End .heading-->
                <!-- Build page from here: -->
 <?php 
if(isset($_GET['msg_type'])){
?>        
        <div class="alert marginT10 alert-<?php echo $_GET['msg_type'];?>">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong><?php echo strtoupper($_GET['msg_type']);?>!</strong>
            <?php echo $_GET['message'];?>
        </div>
<?php
}
?>
								<div class="row-fluid">
                        <div class="span12">
                            <div class="box gradient">
                                <div class="title">
                                    <h4>
                                        <span><?=BOOKING_EXTRA?></span>
                                        <form class="box-form right">
                                          <button class="btn btn-info btn-mini" onClick="window.location.href='add_edit_booking_extra.php?rid=0&cid=0'" type="button"><?php echo ADD_NEW_EXTRA;?></button>
                                        </form>
                                    </h4>
                                </div>
                                <div class="content noPad clearfix">
                                    <table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table" width="100%">
                                        <thead>
                                          <tr>
											<th><?php echo EXTRAS_TITLE; ?></th>
                                            <th><?php echo EXTRAS_DESCRIPTION;?></th>
                                            <th><?php echo EXTRAS_RATE;?></th>
                                            <th><?php echo EXTRAS_PER;?></th>
                                            <th><?php echo EXTRAS_STATUS;?></th>
                                            <th>&nbsp;</th>
                                          </tr>
                                        </thead>
                                        <?=$bsiAdminMain->generateBookingExtraListHtml()?>
                                    </table>
                                </div>
                            </div><!-- End .box -->
                        </div><!-- End .span12 -->
                    </div><!-- End .row-fluid -->
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>
    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>
    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->
    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    </body>
</html>
