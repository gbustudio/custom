<?php 
include ("access.php");
include 'includes/top.php';
include("main_includes/conf.class.php");
?>
    <div id="wrapper">
<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->
                <div class="heading">
                    <h3>Create Hotel</h3>                    
                    
                </div><!-- End .heading-->
                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">
<?php 
if(isset($_SESSION['val1'])){
?>        
    <div class="row-fluid">
            <span class="span12">
        <div class="alert marginT10">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong>MESSAGE!</strong>
            <?php echo $_SESSION['val1'];
                  unset($_SESSION['val1'])?>
        </div>
      </span>
    </div>
<?php
}
?>
                            <div class="box">
                                <div class="title">
                                    <h4>
                                        <span class="icon16 "></span>
                                        <span>Create Hotel</span>
                                    </h4>
                                    
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                        <form style="margin-bottom:0px !important" enctype="multipart/form-data" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo HOTEL_NAME;?> *</label>
                                                    <input class="span4" required name="hotel_name" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo STREET_ADDRESS;?> *</label>
                                                    <input class="span4" required  name="str_addr" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo CITY;?> *</label>
                                                    <input class="span4" required  name="city" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo COUNTRY; ?> *</label>
                                                    <select  required name="country">
                                                    <option value="">Select Country</option>
                                                    <?php 
                                                    $cq = mysql_query("select * from countries where status=1");
                                                    while($cr = mysql_fetch_array($cq)){
                                                        echo '<option value="'.$cr['country_id'].'">'.$cr['country_name'].'</option>';
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Latitude Longitude</label>
                                                    <input class="span4" name="hotel_latlong" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo PHONE_NUMBER; ?> *</label>
                                                    <input class="span4"  required name="phone" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo EMAIL_ID; ?> *</label>
                                                    <input class="span4"  required name="email" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Extra People</label>
                                                    <input class="span4"  name="extra_people" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Minimum Stay</label>
                                                    <input class="span4"   name="min_stay" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Security Deposit</label>
                                                    <input class="span4"   name="security_deposit" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Cancellation</label>
                                                    <input class="span4"   name="cancellation" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Type</label>
                                                    <select  name="hotel_type">
                                                    <?php 
                                                    for($t=0; $t<11; $t++){
                                                        echo '<option value="'.$t.' Star">'.$t.' Star</option>';
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Short Description</label>
                                                    <textarea class="span6" name="short_desc" rows="4" cols="70" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Full Description</label>
                                                    <textarea class="span6" name="full_desc" rows="4" cols="70" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Terms</label>
                                                    <textarea class="span6" name="hotel_terms" rows="4" cols="70" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Amenities List</label>
                                                    <div class="span8" style="margin-left:0 !important">
                                                        <input type="checkbox" value="WI_FI" name="amenity[]" /> WI_FI <br>
                                                        <input type="checkbox" value="SWIMMING POOL" name="amenity[]" /> SWIMMING POOL <br>
                                                        <input type="checkbox" value="TELEVISION" name="amenity[]" /> TELEVISION <br>
                                                        <input type="checkbox" value="COFFEE" name="amenity[]" /> COFFEE <br>
                                                        <input type="checkbox" value="AIR CONDITIONING" name="amenity[]" /> AIR CONDITIONING <br>
                                                        <input type="checkbox" value="FITNESS FACILITY" name="amenity[]" /> FITNESS FACILITY <br>
                                                        <input type="checkbox" value="FRIDGE" name="amenity[]" /> FRIDGE <br>
                                                        <input type="checkbox" value="WINE BAR" name="amenity[]" /> WINE BAR <br>
                                                        <input type="checkbox" value="SMOKING ALLOWED" name="amenity[]" /> SMOKING ALLOWED <br>
                                                        <input type="checkbox" value="ENTERTAINMENT" name="amenity[]" /> ENTERTAINMENT <br>
                                                        <input type="checkbox" value="SECURE VAULT" name="amenity[]" /> SECURE VAULT <br>
                                                        <input type="checkbox" value="PICK AND DROP" name="amenity[]" /> PICK AND DROP <br>
                                                        <input type="checkbox" value="ROOM SERVICE" name="amenity[]" /> ROOM SERVICE <br>
                                                        <input type="checkbox" value="PETS ALLOWED" name="amenity[]" /> PETS ALLOWED <br>
                                                        <input type="checkbox" value="PLAY PLACE" name="amenity[]" /> PLAY PLACE <br>
                                                        <input type="checkbox" value="COMPLIMENTARY BREAKFAST" name="amenity[]" /> COMPLIMENTARY BREAKFAST <br>
                                                        <input type="checkbox" value="FREE PARKING" name="amenity[]" /> FREE PARKING <br>
                                                        <input type="checkbox" value="CONFERENCE ROOM" name="amenity[]" /> CONFERENCE ROOM <br>
                                                        <input type="checkbox" value="FIRE PLACE" name="amenity[]" /> FIRE PLACE <br>
                                                        <input type="checkbox" value="HANDICAP ACCESSIBLE" name="amenity[]" /> HANDICAP ACCESSIBLE <br>
                                                        <input type="checkbox" value="DOORMAN" name="amenity[]" /> DOORMAN <br>
                                                        <input type="checkbox" value="HOT TUB" name="amenity[]" /> HOT TUB <br>
                                                        <input type="checkbox" value="ELEVATOR IN BUILDING" name="amenity[]" /> ELEVATOR IN BUILDING <br>
                                                        <input type="checkbox" value="SUITABLE FOR EVENTS" name="amenity[]" /> SUITABLE FOR EVENTS <br>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Amenities Description</label>
                                                    <textarea class="span6" name="amenities_desc" rows="4" cols="70" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Help Box Text</label>
                                                    <input class="span6" name="help_text" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Why Book with Us Text</label>
                                                    <textarea class="span6" name="why_book_us" rows="4" cols="70" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Cover Image</label>
                                                    <input class="span6" name="cover" type="file" />
                                                    <input type="hidden" name="file2" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Logo</label>
                                                    <input class="span6" name="logo" type="file" />
                                                    <input type="hidden" name="file">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Notifications Email *</label>
                                                    <input class="span6" required name="notify_email" type="email" />
                                                </div>
                                            </div>
                                        </div>
                                        
										<!--
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Select Package</label>
                                                    <select name="hotel_pkg">
                                                    <?php 
                                                    $cq = mysql_query("select * from hotel_packages where hotel_pkg_status=1");
                                                    while($cr = mysql_fetch_array($cq)){
                                                        echo '<option value="'.$cr['hotel_pkg_id'].'">'.$cr['hotel_pkg_name'].' ( $'.$cr['hotel_pkg_price'].'/month )</option>';
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Paid Upto</label>
                                                    <input class="span6" name="paid_upto" placeholder="mm/dd/yyyy" value="<?=date("Y-m-d",time()+60*60*24*30)?>" type="date" />
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <div class="form-actions" style="margin-bottom:0px">
                                                    <div class="span3"></div>
                                                    <div class="span9 controls">
                                                      <button type="submit" class="btn right marginR10" name="create_hotel" id="sbt_details"><?php echo SUBMIT;?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- End .span6 -->  
	
                    </div><!-- End .row-fluid -->
                
                
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>
    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>
    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->
    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    </body>
</html>
