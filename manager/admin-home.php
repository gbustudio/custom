<?php
include("access.php");


include 'includes/top.php';

include("main_includes/conf.class.php");	
include("main_includes/admin.class.php");
?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">
					
					<?php 
					if ($_SESSION['mngr_type'] == 1){
						echo "<h3>Admin Dashboard</h3>";
					} else if ($_SESSION['mngr_type'] == 2){
						echo "<h3>Manager Dashboard</h3>";
					}
					?>                  

                    <!-- <div class="resBtnSearch">
                        <a href="#"><span class="icon16 brocco-icon-search"></span></a>
                    </div>

                    <div class="search">

                        <form id="searchform" action="#" />
                            <input type="text" class="top-search" placeholder="Search here ..." />
                            <input type="submit" class="search-btn" value="" />
                        </form>
                
                    </div><!-- End search -->
                    
                    <!-- <ul class="breadcrumb">
                        <li>You are here:</li>
                        <li>
                            <a href="#" class="tip" title="back to dashboard">
                                <span class="icon16 icomoon-icon-screen"></span>
                            </a> 
                            <span class="divider">
                                <span class="icon16 icomoon-icon-arrow-right"></span>
                            </span>
                        </li>
                        <li class="active">Dashboard</li>
                    </ul> --> 

                </div><!-- End .heading-->

                <!-- Build page from here: -->
<?php 
if(isset($_GET['msg_type'])){
?>        
    <div class="row-fluid">
			<span class="span12">
        <div class="alert marginT10 alert-<?php echo $_GET['msg_type'];?>">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong><?php echo strtoupper($_GET['msg_type']);?>!</strong>
            <?php echo $_GET['message'];?>
        </div>
      </span>
    </div>
<?php
}
?>
				
                <div class="row-fluid">

                    <div class="span8">
                        <ul class="bigBtnIcon">
                            <li>
                                <a href="customerlookup.php" class="tipB" oldtitle="I`m with gradient" title="Total Clients" aria-describedby="ui-tooltip-21">
                                    <span class="icon entypo-icon-users"></span>
                                    <span class="txt">Clients</span>
                                    <span class="notification green"><?php echo users_count(); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#checkins" class="tipB" title="Today's Check Ins" aria-describedby="ui-tooltip-21">
                                    <span class="icon icomoon-icon-history"></span>
                                    <span class="txt">Check Ins</span>
                                    <span class="notification green"><?php echo today_checkins(); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="#checkouts" class="tipB" title="Today's Check Outs" aria-describedby="ui-tooltip-21">
                                    <span class="icon icomoon-icon-meter-fast"></span>
                                    <span class="txt">Check Outs</span>
                                    <span class="notification green"><?php echo today_checkouts(); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="advance_bookings.php" class="tipB" oldtitle="I`m with gradient" title="Reserved Rooms in Advance" aria-describedby="ui-tooltip-21">
                                    <span class="icon brocco-icon-grid"></span>
                                    <span class="txt">Pre-reserved</span>
                                    <span class="notification green"><?php echo pre_reserved_rooms(); ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="span4">
                        <div class="centerContent">
                            <div class="circle-stats">
                                <div class="circular-item tipB" title="Rooms Reserved %age">
                                    <span class="icon icomoon-icon-fire"></span>
                                    <input type="text" value="<?php echo ceil((reserved_rooms()/total_rooms())*100); ?>" class="greenCircle" />
                                </div>
                                <div class="circular-item tipB" title="Free Rooms %age">
                                    <span class="icon icomoon-icon-busy"></span>
                                    <input type="text" value="<?php echo ceil(((total_rooms()-reserved_rooms())/total_rooms())*100); ?>" class="redCircle" />
                                </div>
                            </div>
                        </div>

                    </div><!-- End .span4 -->

                  

                </div><!-- End .row-fluid -->

                <div class="row-fluid">
                <div class="span12">
                    <div class="box chart">
                        <div class="title">
                            <h4>
                                <span class="icon16 icomoon-icon-bars"></span>
                                <span>Check Ins / Check Outs</span>
                            </h4>
                            <!-- <a href="#" class="minimize">Minimize</a> -->
                        </div>
                        <div class="content">
                           <div class="au-lines-chart" style="height: 230px;width:100%;">
                            </div>
                        </div>
                    </div><!-- End .box -->
                </div><!-- End .span6 -->
                </div>
                <div class="row-fluid">

                        <div class="span12">

                            <div class="box gradient">

                                <div class="title">
                                    <h4>
                                        <span><?=LAST_10_BOOKING?></span>
                                    </h4>
                                </div>
                                <div class="content noPad clearfix">
                                    <table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table" width="100%">
                                        <?=$bsiAdminMain->homewidget(1)?>
                                    </table>
                                </div>

                            </div><!-- End .box -->

                        </div><!-- End .span12 -->

                    </div><!-- End .row-fluid -->
								<div class="row-fluid">

                        <div class="span12" id="checkins">

                            <div class="box gradient">

                                <div class="title">
                                    <h4>
                                        <span><?=TODAY_CHECK_IN?></span>
                                    </h4>
                                </div>
                                <div class="content noPad clearfix">
                                    <table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table" width="100%">
                                        <?=$bsiAdminMain->homewidget(2)?>
                                    </table>
                                </div>

                            </div><!-- End .box -->

                        </div><!-- End .span12 -->

                    </div><!-- End .row-fluid -->
								<div class="row-fluid">

                        <div class="span12" id="checkouts">

                            <div class="box gradient">

                                <div class="title">
                                    <h4>
                                        <span><?=TODAY_CHECK_OUT?></span>
                                    </h4>
                                </div>
                                <div class="content noPad clearfix">
                                    <table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table" width="100%">
                                        <?=$bsiAdminMain->homewidget(3)?>
                                    </table>
                                </div>

                            </div><!-- End .box -->

                        </div><!-- End .span12 -->

                    </div><!-- End .row-fluid -->
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    <script type="text/javascript">

    $(function () {

        //some data
        var d1 = [[1, 30], [2, 40], [3, 50], [4, 45],[5, 55],[6, 30],[7, 35],[8, 37],[9, 52],[10, 63],[11, 65],[12, 40],[13, 45],[14, 50],[15, 52],[16, 55],[17, 55],[18, 60],[19, 65],[20, 70],[21, 75],[22, 80],[23, 85],[24, 90],[25, 95],[26, 100],[27, 110],[28, 95],[29, 80], [30, 85]];
        var d2 = [[1, randNum()-5], [2, randNum()-4], [3, randNum()-4], [4, randNum()],[5, 4+randNum()],[6, 4+randNum()],[7, 5+randNum()],[8, 5+randNum()],[9, 6+randNum()],[10, 6+randNum()],[11, 6+randNum()],[12, 2+randNum()],[13, 3+randNum()],[14, 4+randNum()],[15, 4+randNum()],[16, 4+randNum()],[17, 5+randNum()],[18, 5+randNum()],[19, 2+randNum()],[20, 2+randNum()],[21, 3+randNum()],[22, 3+randNum()],[23, 3+randNum()],[24, 2+randNum()],[25, 4+randNum()],[26, 4+randNum()],[27,5+randNum()],[28, 2+randNum()],[29, 2+randNum()], [30, 3+randNum()]];
        //define placeholder class
        var placeholder = $(".au-lines-chart");
        //graph options
        var options = {
                grid: {
                    show: true,
                    aboveData: true,
                    color: "#3f3f3f" ,
                    labelMargin: 5,
                    axisMargin: 0, 
                    borderWidth: 0,
                    borderColor:null,
                    minBorderMargin: 5 ,
                    clickable: true, 
                    hoverable: true,
                    autoHighlight: true,
                    mouseActiveRadius: 20
                },
                series: {
                    grow: {active:true},
                    lines: {
                        show: true,
                        fill: false,
                        lineWidth: 4,
                        steps: false
                        },
                    points: {show:false}
                },
                legend: { position: "se" },
                yaxis: { min: 0 },
                xaxis: {ticks:11, tickDecimals: 0},
                colors: chartColours,
                shadowSize:1,
                tooltip: true, //activate tooltip
                tooltipOpts: {
                    content: "%s : %y.0",
                    shifts: {
                        x: -30,
                        y: -50
                    }
                }
            };   
    
            $.plot(placeholder, [ 
                {
                    label: "Check Ins", 
                    data: d1,
                    lines: {fillColor: "#f2f7f9"},
                    points: {fillColor: "#88bbc8"}
                }, 
                {   
                    label: "Check Outs", 
                    data: d2,
                    lines: {fillColor: "#fff8f2"},
                    points: {fillColor: "#ed7a53"}
                } 
            ], options);

    });
    </script>

    </body>
</html>
