<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <title><?php echo $bsiCore->config['conf_hotel_name']; ?></title>
    <meta name="author" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="application-name" content="" />
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Le styles -->
    
    <!-- Use new way for google web fonts 
    http://www.smashingmagazine.com/2012/07/11/avoiding-faux-weights-styles-google-web-fonts -->
    <!-- Headings -->
    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
    <!-- Text -->
    <!--<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' /> 
    <!--[if lt IE 9]>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
    <![endif]-->

    <link href="<?= CSS_PATH . 'bootstrap/bootstrap.css';?>" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_PATH . 'bootstrap/bootstrap-responsive.css';?>" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_PATH . 'bootstrap/bootstrap-datepicker.css';?>" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_PATH . 'supr-theme/jquery.ui.supr.css';?>" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_PATH . 'icons.css';?>" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_PATH . 'datepicker1.css';?>" rel="stylesheet" type="text/css" />
    <!-- Plugin stylesheets -->
    <link href="<?= PLUGIN_PATH . 'qtip/jquery.qtip.css';?>" rel="stylesheet" type="text/css" />
    <link href="<?= PLUGIN_PATH . 'fullcalendar/fullcalendar.css';?>" rel="stylesheet" type="text/css" />
    <link href="<?= PLUGIN_PATH . 'jpages/jPages.css';?>" rel="stylesheet" type="text/css" />
    <link href="<?= PLUGIN_PATH . 'prettify/prettify.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'inputlimiter/jquery.inputlimiter.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'ibutton/jquery.ibutton.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'uniform/uniform.default.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'color-picker/color-picker.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'select/select2.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'validate/validate.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'pnotify/jquery.pnotify.default.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'pretty-photo/prettyPhoto.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'smartWizzard/smart_wizard.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'dataTables/jquery.dataTables.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'elfinder/elfinder.css';?>" type="text/css" rel="stylesheet" />
    <link href="<?= PLUGIN_PATH . 'plupload/jquery.ui.plupload/css/jquery.ui.plupload.css';?>" type="text/css" rel="stylesheet" />

    <!-- Main stylesheets -->
    <link href="<?= CSS_PATH . 'main.css';?>" rel="stylesheet" type="text/css" /> 
    <link href="<?= CSS_PATH . 'flexslider.css';?>" rel="stylesheet" type="text/css" /> 

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="images/favicon.ico" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png" />
    
    <script type="text/javascript">
        //adding load class to body and hide page
        // document.documentElement.className += 'loadstate';
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
      
    <body>
    <!-- loading animation -->
    <div id="qLoverlay"></div>
    <div id="qLbar"></div>
    
    <div id="header">

        <div class="navbar">
            <div class="navbar-inner">
              <div class="container-fluid">
                <a class="brand" href="<?= BASE_URL;?>Home"><?php echo $bsiCore->config['conf_hotel_name']; ?> <span class="slogan">Hotel Reservation Company</span></a>
                <div class="nav-no-collapse">
                    <ul class="nav pull-right usernav">
                        <li class="dropdown">
                          <select onChange="langchange(this.value)">
                            <?php echo $lang_dd; ?>
                          </select>
                        </li>
                    </ul>
                </div><!-- /.nav-collapse -->
              </div>
            </div><!-- /navbar-inner -->
          </div><!-- /navbar --> 

    </div><!-- End #header -->
