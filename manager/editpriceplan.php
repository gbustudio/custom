<?php 
include("access.php");
include 'includes/top.php';
include("main_includes/conf.class.php");
include("main_includes/admin.class.php");
if(isset($_GET['edit']) && $_GET['edit'] != ""){
	$plan = $bsiCore->ClearInput($_GET['edit']);

	if($plan!=0){
		$row = mysql_fetch_assoc(mysql_query("select * from bsi_priceplan where plan_id=".$_GET['edit']));
		$rowrt = mysql_fetch_assoc(mysql_query($bsiAdminMain->getRoomtypesql($row['roomtype_id'])));
		$rowca = mysql_fetch_assoc(mysql_query($bsiAdminMain->getCapacitysql($row['capacity_id'])));
		$roomtypeCombo = $rowrt['type_name'].'<input type="hidden" name="roomtype_id" value="'.$row['roomtype_id'].'" />';
		$capacityCombo = $rowca['title'].'<input type="hidden" name="capacity_id" value="'.$row['capacity_id'].'" />';
	}else{
		$row = NULL;
	}
}else{
	header("location:room_list.php");
	exit;
}
?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3>Prices Plan add/edit</h3>                    

                    

                </div><!-- End .heading-->

                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">

 <?php 
if(isset($_GET['msg_type'])){
?>        
        <div class="alert marginT10 alert-<?php echo $_GET['msg_type'];?>">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong><?php echo strtoupper($_GET['msg_type']);?>!</strong>
            <?php echo $_GET['message'];?>
        </div>
<?php
}
?>
                            <div class="box">

                                <div class="title">

                                    <h4>
                                        <span class="icon16 "></span>
                                        <span>Price Plan add/edit</span>
                                    </h4>
                                    
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                        <form style="margin-bottom:0px !important" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Room Type</label>
                                                    <?php 
                                                    if($_GET['edit']==0){
                                                        $eq = mysql_query("select room_id from bsi_room where status=1 and hotel_id=".$_SESSION['hotel_id']);
                                                        $pq = mysql_query("select room_id from bsi_priceplan where status=1 and hotel_id=".$_SESSION['hotel_id']);
                                                        $roomsq = array(); $roomsp = array();
                                                        while($er = mysql_fetch_array($eq)){ $roomsq[] = $er['room_id']; }
                                                        while($pr = mysql_fetch_array($pq)){ $roomsp[] = $pr['room_id']; }
                                                        $c = array_diff($roomsq, $roomsp);
                                                        echo '<select required name="room_id">';
                                                        foreach($roomsq as $roomid){
                                                            $room = mysql_fetch_array(mysql_query("select roomtype_id,capacity_id,room_rate from bsi_room where room_id=".$roomid));
                                                            $cap = mysql_fetch_array(mysql_query("select title from bsi_capacity where id=".$room['capacity_id']));
                                                            $type = mysql_fetch_array(mysql_query("select type_name from bsi_roomtype where roomtype_id=".$room['roomtype_id']));
                                                            echo '<option value="'.$roomid.'">'.$cap['title']." ".$type['type_name']." (".$room['room_rate'].')</option>';
                                                        }
                                                        echo "</select><input type='hidden' name='page' value='add' />";
                                                    } else { 
                                                        $room = mysql_fetch_array(mysql_query("select roomtype_id,capacity_id,room_rate from bsi_room where room_id=".$row['room_id']));
                                                        $cap = mysql_fetch_array(mysql_query("select title from bsi_capacity where id=".$room['capacity_id']));
                                                        $type = mysql_fetch_array(mysql_query("select type_name from bsi_roomtype where roomtype_id=".$room['roomtype_id']));

                                                        echo '<input type="hidden" name="room_id" value="'.$row['room_id'].'" />'.$cap['title']." ".$type['type_name']." (".$room['room_rate'].")<input type='hidden' name='page' value='update' />"; 
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Start Date</label>
                                                    <input class="span4" id="txtFromDate" name="start_date" type="text" value="<?=($row['end_date']=='0000-00-00' or $_GET['edit']==0)?'':date("m/d/Y", strtotime($row['start_date']))?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">End Date</label>
                                                    <input class="span4" id="txtToDate" name="end_date" type="text" value="<?=($row['end_date']=='0000-00-00' or $_GET['edit']==0)?'':date("m/d/Y", strtotime($row['end_date']))?>" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Sunday</label>
                                                    <input class="span4" required name="sun" type="text" value="<?=$row['sun']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Monday</label>
                                                    <input class="span4" required name="mon" type="text" value="<?=$row['mon']?>" />
                                                    <input name="hotel"  type="hidden" value="<?=$_SESSION['hotel_id']?>" />
                                                     <input name="plan"  type="hidden" value="<?=$_GET['edit']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Tuesday</label>
                                                    <input class="span4" required name="tue" type="text" value="<?=$row['tue']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Wednesday</label>
                                                    <input class="span4" required name="wed" type="text" value="<?=$row['wed']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Thursday</label>
                                                    <input class="span4" required name="thu" type="text" value="<?=$row['thu']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Friday</label>
                                                    <input class="span4" required name="fri" type="text" value="<?=$row['fri']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Saturday</label>
                                                    <input class="span4" required name="sat" type="text" value="<?=$row['sat']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <div class="form-actions" style="margin-bottom:0px">
                                                    <div class="span3"></div>
                                                    <div class="span9 controls">
                                                      <button type="submit" class="btn right marginR10" name="submitPlan">Submit<span class="icon12 brocco-icon-forward"></span></button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div><!-- End .span6 -->  
	
                    </div><!-- End .row-fluid -->

                
                
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    <script type="text/javascript">
    $(function(){
         $.datepicker.setDefaults( $.datepicker.regional[ "en" ] );
        //$.datepicker.setDefaults({ dateFormat: '' });
       $("#txtFromDate").datepicker({
           maxDate: "+365D",
           numberOfMonths: 1,
           minDate: "<?=date('m/d/Y')?>",
           onSelect: function(selected) {
         var date = $(this).datepicker('getDate');
            if(date){
               date.setDate(date.getDate());
             }
             $("#txtToDate").datepicker("option","minDate", date);
             $("#txtToDate").val(selected);
           }
       });

       $("#txtToDate").datepicker({ 
           maxDate:"+365D",
           numberOfMonths: 1,
           onSelect: function(selected) {
              $("#txtFromDate").datepicker("option","maxDate", selected)
           }
       });  
   

    })
    </script>
    </body>
</html>
