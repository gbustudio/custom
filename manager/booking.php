<?php 
include("access.php");
if(!isset($_GET['id']) or $_GET['id']=="" or !is_numeric($_GET['id']) or $_GET['id']<1){
    header("location: view_bookings.php");
}
include 'includes/top.php';
include("main_includes/conf.class.php");
$row = mysql_fetch_array(mysql_query("select * from bsi_bookings where booking_id=".$_GET['id']));
$room = mysql_fetch_array(mysql_query("select roomtype_id,capacity_id from bsi_room where room_id=".$row['room_id']));
$cap = mysql_fetch_array(mysql_query("select title from bsi_capacity where id=".$room['capacity_id']));
$type = mysql_fetch_array(mysql_query("select type_name from bsi_roomtype where roomtype_id=".$room['roomtype_id']));
$user= mysql_fetch_array(mysql_query("select * from users where status=1 and user_id=".$row['user_id']));
$country = mysql_fetch_array(mysql_query("select country_name from countries where country_id=".$user['country_id']));
$promo = 'No';
$discount='No';
if($row['promo_id']!=0){ $prom=mysql_fetch_array(mysql_query("select coupon_code,discount,discount_type from promotions where promo_id=".$row['promo_id'])); $promo=$prom['coupon_code']; if($prom['discount_type']==1){$discount=$prom['discount']."%";}else{$discount="$".$prom['discount'];}}
$payment = mysql_fetch_array(mysql_query("select gateway_name from bsi_payment_gateway where gateway_code='".$row['payment_type']."' and hotel_id=".$row['hotel_id']));
?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3>Booking #<?=$_GET['id']?></h3>                    

                    

                </div><!-- End .heading-->

                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">

 <?php 
if(isset($_GET['msg_type'])){
?>        
        <div class="alert marginT10 alert-<?php echo $_GET['msg_type'];?>">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong><?php echo strtoupper($_GET['msg_type']);?>!</strong>
            <?php echo $_GET['message'];?>
        </div>
<?php
}
?>
<style type="text/css">
    td{text-align:left !important;}
</style>
                            <div class="box gradient">
                                <div class="title">
                                    <h4>
                                        <span class="icon16 "></span>
                                        <span>Booking Details</span>
                                    </h4>
                                </div>
                                <div class="content noPad clearfix">
                                    <div class="span6 pull-left">
                                    <table cellpadding="0" cellspacing="0" border="0" class="responsive table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Client Details</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Name</td>
                                                <td><?php echo $user['first_name']." ".$user['last_name']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Address</td>
                                                <td><?=$user['street_address'].', '.$user['state'].', '.$user['zip'].', '.$user['city']?></td>
                                            </tr>
                                            <tr>
                                                <td>City</td>
                                                <td><?=$user['city']?></td>
                                            </tr>
                                            <tr>
                                                <td>Country</td>
                                                <td><?=$country['country_name']?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?=$user['email']?></td>
                                            </tr>
                                            <tr>
                                                <td>Phone</td>
                                                <td><?=$user['phone']?></td>
                                            </tr>
                                            <tr>
                                                <td>Fax</td>
                                                <td><?=$user['fax']?></td>
                                            </tr>
                                            <tr>
                                                <td>Identity Type</td>
                                                <td><?=$user['identity_type']?></td>
                                            </tr>
                                            <tr>
                                                <td>Identity Number</td>
                                                <td><?=$user['  identity_number']?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </div>

                                    <div class="span6 pull-left">
                                        <table cellpadding="0" cellspacing="0" border="0" class="responsive table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Payment Details</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Total Amount</td>
                                                <td><?=$row['total_cost']?></td>
                                            </tr>
                                            <tr>
                                                <td>Promo Code</td>
                                                <td><?=$promo?></td>
                                            </tr>
                                            <tr>
                                                <td>Discount</td>
                                                <td><?=$discount?></td>
                                            </tr>
                                            <tr>
                                                <td>Payment Type</td>
                                                <td><?=$payment['gateway_name']?></td>
                                            </tr>
                                            <tr>
                                                <td>Transaction ID</td>
                                                <td><?=$row['payment_txnid']?></td>
                                            </tr>
                                            <tr>
                                                <td>Paypal Email</td>
                                                <td><?=$row['payment_method_detail']?></td>
                                            </tr>
                                            <tr>
                                                <td>Payment Status</td>
                                                <td><?php if($row['payment_success']==0){echo "Unpaid";}elseif($row['payment_success']==1){echo "Paid";}elseif($row['payment_success']==2){echo "Under Review";}elseif($row['payment_success']==-1){echo "Failed";} ?></td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                    </div>

                                    <br><br><table cellpadding="0" cellspacing="0" border="0" class="responsive table pull-left">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Booking Details</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Room Type</td>
                                                <td><?php echo $cap['title']." ".$type['type_name']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Check In Date</td>
                                                <td><?=$row['start_date']?></td>
                                            </tr>
                                            <tr>
                                                <td>Check Out Date</td>
                                                <td><?=$row['end_date']?></td>
                                            </tr>
                                            <tr>
                                                <td>Booking Time</td>
                                                <td><?=$row['booking_time']?></td>
                                            </tr>
                                            <tr>
                                                <td>Number of Rooms</td>
                                                <td><?php $x=json_decode($row['booking_details_json'],true);
                                                echo count($x); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Booking Extras</td>
                                                <td><?php $a=json_decode($row['booking_extras_json'],true);
												$flag = 0;
                                                foreach($a as $b=>$c){
													if ($flag % 2 == 0){
														$id_extra = ucwords(str_replace("extra_"," ",$b));
														$extrass = mysql_fetch_array(mysql_query("select * from booking_extras where extras_id=".$id_extra));

														echo $extrass['extras_title'].": ";
													} else {
														echo $c."<br>";
													}
													//echo ucwords(str_replace("_"," ",$b)).": ".$c."<br>";
													$flag++;
												} ?></td>
                                            </tr>
                                            <tr>
                                                <td>Reservation Status</td>
                                                <td><?php if($row['status']==1){ echo "Confirmed";}elseif($row['status']==0){echo "Pending Approval";}elseif($row['status']==-1){echo "Rejected";}?></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                   
                                        
                                </div>
                            </div>

                        </div><!-- End .span6 -->  
    
                    </div><!-- End .row-fluid -->

                
                
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    </body>
</html>
