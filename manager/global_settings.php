<?php 
include ("access.php");
// if(isset($_POST['sbt_details'])){
// 	include("main_includes/db.conn.php");
// 	include("main_includes/admin.class.php");
// 	include("main_includes/conf.class.php");
// 	$bsiAdminMain->hotel_details_post();
// 	header("location:admin_hotel_details.php");
// }
include 'includes/top.php';
include("main_includes/conf.class.php");
$hotel = mysql_fetch_array(mysql_query("select theme_color,hotel_cover_image,notify_email,timezone,default_language,default_currency from bsi_hotels where hotel_id=".$_SESSION['hotel_id']));
?>
    <div id="wrapper">
<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->
                <div class="heading">
                    <h3>Global Settings</h3>                    
                    
                </div><!-- End .heading-->
                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">
<?php 
if(isset($_SESSION['val1'])){
?>        
    <div class="row-fluid">
            <span class="span12">
        <div class="alert marginT10">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong>MESSAGE!</strong>
            <?php echo $_SESSION['val1'];
                  unset($_SESSION['val1'])?>
        </div>
      </span>
    </div>
<?php
}
?>
                            <div class="box">
                                <div class="title">
                                    <h4>
                                        <span class="icon16 "></span>
                                        <span>Global Settings</span>
                                    </h4>
                                    
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                        <form style="margin-bottom:0px !important" enctype="multipart/form-data" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Email Notification</label>
                                                    <input class="span6" name="notify_email" type="email" value="<?=$hotel['notify_email']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Color Theme</label>
                                                    <span style="margin:0 !important;" class="span6"><input type="text" readonly="readonly" required id="color" name="colorcode" value="<?=$hotel['theme_color']?>" /> <div class="picker"></div></span>
                                                    <button class="btn deflt marginL10" type="button">default</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Cover Image</label>
                                                    <input class="span6" name="cover" type="file" />
                                                    <input type="hidden" name="file2" value="<?=$hotel['hotel_cover_image']?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Timezone:</label>
                                                    <select name="timezone" id="timezone">
                                                    <?php foreach($zonelist as $k=>$v){ $st=($k==$hotel['timezone'])?"selected='selected'":""; ?>
                                                            <option <?=$st?> value="<?=$k?>"><?=$v?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                         <!-- <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Default Language</label>
                                                    <select  required name="language">
                                                    <option value="">Select Language</option>
                                                    <?php 
                                                    //$cq = mysql_query("select * from bsi_language where status=1");
                                                   // while($cr = mysql_fetch_array($cq)){
                                                    //    $dl=($cr['id']==$hotel['default_language'])?"selected='selected'":"";
                                                     //   echo '<option value="'.$cr['id'].'" '.$dl.'>'.$cr['lang_title'].'</option>';
                                                   // }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div> -->
                                         <!-- <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Default Currency</label>
                                                    <select  required name="currency">
                                                    <option value="">Select Currency</option>
                                                    <?php 
                                                    // $cq = mysql_query("select * from bsi_currency where status=1");
                                                    // while($cr = mysql_fetch_array($cq)){
                                                    //     $dc=($cr['currency_id']==$hotel['default_currency'])?"selected='selected'":"";
                                                    //     echo '<option value="'.$cr['currency_id'].'" '.$dc.'>'.$cr['currency_code'].'</option>';
                                                    // }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <div class="form-actions" style="margin-bottom:0px">
                                                    <div class="span3"></div>
                                                    <div class="span9 controls">
                                                      <button type="submit" class="btn right marginR10" name="saveConfig" id="sbt_details"><?php echo SUBMIT;?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- End .span6 -->  
	
                    </div><!-- End .row-fluid -->
                
                
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>
    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>
    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->
    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript">
    $(function(){
        $("div.picker").hide();
        $("#color").click(function(){
            $("div.picker").toggle('slow');
        })
        $(".deflt").click(function(e){
            
            $("#color").val("#F5F5F5");
        })
    })
    </script>
    </body>
</html>
