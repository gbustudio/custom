        <!--Responsive navigation button-->  
        <div class="resBtn">
            <a href="#"><span class="icon16 minia-icon-list-3"></span></a>
        </div>
        
        <!--Sidebar background-->
        <div id="sidebarbg"></div>
        <!--Sidebar content-->
        <div id="sidebar">          
            <div class="sidenav">
                <div class="shortcuts" style="display: none;">
					<ul>
						<li><a href="mailto:zaeemrizwan@live.com" title="Support section" class="tip"><span class="icon24 icomoon-icon-support"></span></a></li>
						<li><a href="#" title="Database backup" class="tip"><span class="icon24 icomoon-icon-database"></span></a></li>
						<li><a href="admin_home.php" title="Sales statistics" class="tip"><span class="icon24 iconic-icon-chart"></span></a></li>
						<li><a href="add_edit_room.php?rid=0&cid=0" title="Add New Room" class="tip"><span class="icon24 icomoon-icon-pencil"></span></a></li>
					</ul>
				</div><!-- End search -->   
                <div class="sidebar-widget" style="margin: -1px 0 0 0;">
                    <h5 class="title" style="margin-bottom:0">Navigation</h5>
                </div><!-- End .sidenav-widget -->
                <div class="mainnav">
                    <ul>
<?php
$sql_parent=mysql_query("select * from bsi_adminmenu where parent_id=0 and status='Y' and (type='".$_SESSION['mngr_type']."' or type=3) order by ord");
while($row_parent=mysql_fetch_array($sql_parent))
{
  if($row_parent['name']=='SETTING')
  echo '<li class="last"><a href="'.$row_parent['url'].'"><span class="icon16 entypo-icon-arrow-17"></span>'.$row_parent['name'].'</a>';
  else
  echo '<li><a href="'.$row_parent['url'].'"><span class="icon16 entypo-icon-arrow-17"></span>'.$row_parent['name'].'</a>';
  $sql_parent222=mysql_query("select * from bsi_adminmenu where parent_id=".$row_parent[0]." and status='Y' and (type='".$_SESSION['mngr_type']."' or type=3) order by ord");
  if(mysql_num_rows($sql_parent222))
  {
    echo '<ul class="sub">';
    while($row_parent222=mysql_fetch_array($sql_parent222))
    {
      echo '<li><a href="'.$row_parent222['url'].'"><span class="icon16 entypo-icon-arrow-17"></span>'.$row_parent222['name'].'</a></li>';
    }
    echo '</ul>';
  }else{
    echo '</li>';
  }
}
?>
                    </ul>
                </div>
            </div><!-- End sidenav -->
            <div class="sidebar-widget">
                <h5 class="title">Right now</h5>
                <div class="content">
                    <div class="rightnow">
                        <ul class="unstyled">
                            <li><span class="number"><?php echo reserved_rooms(); ?></span><span class="icon16 entypo-icon-users"></span>Reserved Rooms</li>
                            <li><span class="number"><?php echo (total_rooms()-reserved_rooms()); ?></span><span class="icon16 cut-icon-tree"></span>Free Rooms</li>
                            <li><span class="number"><?php echo pre_reserved_rooms(); ?></span><span class="icon16 icomoon-icon-newspaper"></span>Pre-reserved</li>
                            <li><span class="number"><?php echo pending_bookings(); ?></span><span class="icon16 cut-icon-tree"></span>Pending Bookings</li>
                            <li><span class="number"></span><span class="icon16 entypo-icon-clock"></span> <?=date("Y-m-d H:i:s")?></li>
                        </ul>
                    </div>
                </div>
            </div><!-- End .sidenav-widget -->
        </div><!-- End #sidebar -->
