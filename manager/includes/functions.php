<?php 
//timezone_start
	$zonelist = array('Kwajalein' => '(GMT-12:00) International Date Line West',
			'Pacific/Midway' => '(GMT-11:00) Midway Island',
			'Pacific/Samoa' => '(GMT-11:00) Samoa',
			'Pacific/Honolulu' => '(GMT-10:00) Hawaii',
			'America/Anchorage' => '(GMT-09:00) Alaska',
			'America/Los_Angeles' => '(GMT-08:00) Pacific Time (US &amp; Canada)',
			'America/Tijuana' => '(GMT-08:00) Tijuana, Baja California',
			'America/Denver' => '(GMT-07:00) Mountain Time (US &amp; Canada)',
			'America/Chihuahua' => '(GMT-07:00) Chihuahua',
			'America/Mazatlan' => '(GMT-07:00) Mazatlan',
			'America/Phoenix' => '(GMT-07:00) Arizona',
			'America/Regina' => '(GMT-06:00) Saskatchewan',
			'America/Tegucigalpa' => '(GMT-06:00) Central America',
			'America/Chicago' => '(GMT-06:00) Central Time (US &amp; Canada)',
			'America/Mexico_City' => '(GMT-06:00) Mexico City',
			'America/Monterrey' => '(GMT-06:00) Monterrey',
			'America/New_York' => '(GMT-05:00) Eastern Time (US &amp; Canada)',
			'America/Bogota' => '(GMT-05:00) Bogota',
			'America/Lima' => '(GMT-05:00) Lima',
			'America/Rio_Branco' => '(GMT-05:00) Rio Branco',
			'America/Indiana/Indianapolis' => '(GMT-05:00) Indiana (East)',
			'America/Caracas' => '(GMT-04:30) Caracas',
			'America/Halifax' => '(GMT-04:00) Atlantic Time (Canada)',
			'America/Manaus' => '(GMT-04:00) Manaus',
			'America/Santiago' => '(GMT-04:00) Santiago',
			'America/La_Paz' => '(GMT-04:00) La Paz',
			'America/St_Johns' => '(GMT-03:30) Newfoundland',
			'America/Argentina/Buenos_Aires' => '(GMT-03:00) Georgetown',
			'America/Sao_Paulo' => '(GMT-03:00) Brasilia',
			'America/Godthab' => '(GMT-03:00) Greenland',
			'America/Montevideo' => '(GMT-03:00) Montevideo',
			'Atlantic/South_Georgia' => '(GMT-02:00) Mid-Atlantic',
			'Atlantic/Azores' => '(GMT-01:00) Azores',
			'Atlantic/Cape_Verde' => '(GMT-01:00) Cape Verde Is.',
			'Europe/Dublin' => '(GMT) Dublin',
			'Europe/Lisbon' => '(GMT) Lisbon',
			'Europe/London' => '(GMT) London',
			'Africa/Monrovia' => '(GMT) Monrovia',
			'Atlantic/Reykjavik' => '(GMT) Reykjavik',
			'Africa/Casablanca' => '(GMT) Casablanca',
			'Europe/Belgrade' => '(GMT+01:00) Belgrade',
			'Europe/Bratislava' => '(GMT+01:00) Bratislava',
			'Europe/Budapest' => '(GMT+01:00) Budapest',
			'Europe/Ljubljana' => '(GMT+01:00) Ljubljana',
			'Europe/Prague' => '(GMT+01:00) Prague',
			'Europe/Sarajevo' => '(GMT+01:00) Sarajevo',
			'Europe/Skopje' => '(GMT+01:00) Skopje',
			'Europe/Warsaw' => '(GMT+01:00) Warsaw',
			'Europe/Zagreb' => '(GMT+01:00) Zagreb',
			'Europe/Brussels' => '(GMT+01:00) Brussels',
			'Europe/Copenhagen' => '(GMT+01:00) Copenhagen',
			'Europe/Madrid' => '(GMT+01:00) Madrid',
			'Europe/Paris' => '(GMT+01:00) Paris',
			'Africa/Algiers' => '(GMT+01:00) West Central Africa',
			'Europe/Amsterdam' => '(GMT+01:00) Amsterdam',
			'Europe/Berlin' => '(GMT+01:00) Berlin',
			'Europe/Rome' => '(GMT+01:00) Rome',
			'Europe/Stockholm' => '(GMT+01:00) Stockholm',
			'Europe/Vienna' => '(GMT+01:00) Vienna',
			'Europe/Minsk' => '(GMT+02:00) Minsk',
			'Africa/Cairo' => '(GMT+02:00) Cairo',
			'Europe/Helsinki' => '(GMT+02:00) Helsinki',
			'Europe/Riga' => '(GMT+02:00) Riga',
			'Europe/Sofia' => '(GMT+02:00) Sofia',
			'Europe/Tallinn' => '(GMT+02:00) Tallinn',
			'Europe/Vilnius' => '(GMT+02:00) Vilnius',
			'Europe/Athens' => '(GMT+02:00) Athens',
			'Europe/Bucharest' => '(GMT+02:00) Bucharest',
			'Europe/Istanbul' => '(GMT+02:00) Istanbul',
			'Asia/Jerusalem' => '(GMT+02:00) Jerusalem',
			'Asia/Amman' => '(GMT+02:00) Amman',
			'Asia/Beirut' => '(GMT+02:00) Beirut',
			'Africa/Windhoek' => '(GMT+02:00) Windhoek',
			'Africa/Harare' => '(GMT+02:00) Harare',
			'Asia/Kuwait' => '(GMT+03:00) Kuwait',
			'Asia/Riyadh' => '(GMT+03:00) Riyadh',
			'Asia/Baghdad' => '(GMT+03:00) Baghdad',
			'Africa/Nairobi' => '(GMT+03:00) Nairobi',
			'Asia/Tbilisi' => '(GMT+03:00) Tbilisi',
			'Europe/Moscow' => '(GMT+03:00) Moscow',
			'Europe/Volgograd' => '(GMT+03:00) Volgograd',
			'Asia/Tehran' => '(GMT+03:30) Tehran',
			'Asia/Muscat' => '(GMT+04:00) Muscat',
			'Asia/Baku' => '(GMT+04:00) Baku',
			'Asia/Yerevan' => '(GMT+04:00) Yerevan',
			'Asia/Yekaterinburg' => '(GMT+05:00) Ekaterinburg',
			'Asia/Karachi' => '(GMT+05:00) Karachi',
			'Asia/Tashkent' => '(GMT+05:00) Tashkent',
			'Asia/Calcutta' => '(GMT+05:30) Calcutta',
			'Asia/Colombo' => '(GMT+05:30) Sri Jayawardenepura',
			'Asia/Katmandu' => '(GMT+05:45) Kathmandu',
			'Asia/Dhaka' => '(GMT+06:00) Dhaka',
			'Asia/Almaty' => '(GMT+06:00) Almaty',
			'Asia/Novosibirsk' => '(GMT+06:00) Novosibirsk',
			'Asia/Rangoon' => '(GMT+06:30) Yangon (Rangoon)',
			'Asia/Krasnoyarsk' => '(GMT+07:00) Krasnoyarsk',
			'Asia/Bangkok' => '(GMT+07:00) Bangkok',
			'Asia/Jakarta' => '(GMT+07:00) Jakarta',
			'Asia/Brunei' => '(GMT+08:00) Beijing',
			'Asia/Chongqing' => '(GMT+08:00) Chongqing',
			'Asia/Hong_Kong' => '(GMT+08:00) Hong Kong',
			'Asia/Urumqi' => '(GMT+08:00) Urumqi',
			'Asia/Irkutsk' => '(GMT+08:00) Irkutsk',
			'Asia/Ulaanbaatar' => '(GMT+08:00) Ulaan Bataar',
			'Asia/Kuala_Lumpur' => '(GMT+08:00) Kuala Lumpur',
			'Asia/Singapore' => '(GMT+08:00) Singapore',
			'Asia/Taipei' => '(GMT+08:00) Taipei',
			'Australia/Perth' => '(GMT+08:00) Perth',
			'Asia/Seoul' => '(GMT+09:00) Seoul',
			'Asia/Tokyo' => '(GMT+09:00) Tokyo',
			'Asia/Yakutsk' => '(GMT+09:00) Yakutsk',
			'Australia/Darwin' => '(GMT+09:30) Darwin',
			'Australia/Adelaide' => '(GMT+09:30) Adelaide',
			'Australia/Canberra' => '(GMT+10:00) Canberra',
			'Australia/Melbourne' => '(GMT+10:00) Melbourne',
			'Australia/Sydney' => '(GMT+10:00) Sydney',
			'Australia/Brisbane' => '(GMT+10:00) Brisbane',
			'Australia/Hobart' => '(GMT+10:00) Hobart',
			'Asia/Vladivostok' => '(GMT+10:00) Vladivostok',
			'Pacific/Guam' => '(GMT+10:00) Guam',
			'Pacific/Port_Moresby' => '(GMT+10:00) Port Moresby',
			'Asia/Magadan' => '(GMT+11:00) Magadan',
			'Pacific/Fiji' => '(GMT+12:00) Fiji',
			'Asia/Kamchatka' => '(GMT+12:00) Kamchatka',
			'Pacific/Auckland' => '(GMT+12:00) Auckland',
			'Pacific/Tongatapu' => '(GMT+13:00) Nukualofa');
function pending_bookings(){
    $q = mysql_query("select booking_id from bsi_bookings where status=0 and hotel_id=".$_SESSION['hotel_id']);
    return mysql_num_rows($q);
}
function today_checkins(){
    $q = mysql_query("select booking_id from bsi_bookings where start_date=CURDATE() and status=1 and hotel_id=".$_SESSION['hotel_id']);
    return mysql_num_rows($q);
}
function today_checkouts(){
    $q = mysql_query("select booking_id from bsi_bookings where end_date=CURDATE() and status=1 and hotel_id=".$_SESSION['hotel_id']);
    return mysql_num_rows($q);
}
function total_rooms(){
	$q = mysql_query("select sum(no_of_rooms) as total from bsi_room where status=1 and hotel_id=".$_SESSION['hotel_id']);
    $r = mysql_fetch_array($q);
    return $r['total'];
}
function reserved_rooms(){
	$q = mysql_query("select booking_details_json from bsi_bookings where status=1 and hotel_id=".$_SESSION['hotel_id']." and CURDATE() between start_date and end_date");
	$total=0;
	$re = mysql_num_rows($q);
	if($re>0){
	    while($r = mysql_fetch_array($q)){
	    	$total += count(json_decode($r['booking_details_json'],true));
	    }
	}
    return $total;
}
function pre_reserved_rooms(){
	$q = mysql_query("select booking_details_json from bsi_bookings where status=1 and hotel_id=".$_SESSION['hotel_id']." and start_date>CURDATE()");
	$total=0;
	$re = mysql_num_rows($q);
	if($re>0){
	    while($r = mysql_fetch_array($q)){
	    	$total += count(json_decode($r['booking_details_json'],true));
	    }
	}
    return $total;
}
function users_count(){
	$q = mysql_query("select user_id from users where status=1");
	return mysql_num_rows($q);
}
function pkg_rooms($pkg_id){
	$q = mysql_query("select hotel_pkg_room_limit from hotel_packages where hotel_pkg_id=".$pkg_id." and hotel_pkg_status=1");
	$r = mysql_fetch_array($q);
	return $r['hotel_pkg_room_limit'];
}
function pkg_name($pkg_id){
	$q = mysql_query("select hotel_pkg_name from hotel_packages where hotel_pkg_id=".$pkg_id." and hotel_pkg_status=1");
	$r = mysql_fetch_array($q);
	return $r['hotel_pkg_name'];
}
function rooms_left(){
	$q = mysql_query("select package from bsi_hotels where hotel_id=".$_SESSION['hotel_id']);
	$r= mysql_fetch_array($q);
	$pkg_rooms = pkg_rooms($r['package']);
	return $pkg_rooms-total_rooms();
}
function room_available($room_id, $start_date, $end_date, $no_of_rooms, $total_rooms){
	//0=pending;1=confirmed;2=checkin;3=checkout;-1=rejected;-2=cancelled;-3=blocked;
	$diff = ($end_date-$start_date)/86400;
	$return=0;
	for($i=0; $i<=$diff; $i++){
		$res=0;
		$date = $start_date+($i*86400);
		$q=mysql_query("select booking_details_json from bsi_bookings where room_id={$room_id} and (status=1 or status=2 or status=-3) and '".date("Y-m-d",$date)."' between start_date and end_date");
		$no = mysql_num_rows($q);
		if($no>0){
			while($r=mysql_fetch_array($q)){
				$res += count(json_decode($r['booking_details_json'],true));
			}
			if(($total_rooms-$res)<$no_of_rooms){ $return=1; }
		}
	}
	if($return==0){return true;}else{return false;}
}
function createHotelPermalink($hotel_id, $hotel_name){
	return $hotel_id."-".str_replace(" ","_",strtolower(substr(preg_replace("/[^a-zA-Z0-9 ]+/", "", trim($hotel_name)),0,50)));
}
function emailnow($booking_id, $email_name){
	$booking = mysql_fetch_array(mysql_query("select * from bsi_bookings where booking_id=".$booking_id));
	$user = mysql_fetch_array(mysql_query("select title,first_name,last_name,email from users where user_id=".$booking['user_id']));
	$hotel = mysql_fetch_array(mysql_query("select notify_email from bsi_hotels where hotel_id=".$_SESSION['hotel_id']));
	$emailq = mysql_query("select email_subject,email_text from bsi_email_contents where email_name='{$email_name}' and hotel_id=".$_SESSION['hotel_id']);
	$to = $user['first_name']." ".$user['last_name']."<".$user['email'].">";
	if(mysql_num_rows($emailq)>0){
		$emailc=mysql_fetch_array($emailq);
		$email_subject = $emailc['email_subject'];
		$email_text = $emailc['email_text'];
	} else {
		$email_subject = "Booking Confirmation";
		$email_text = "Dear ".$user['title']." ".$user['first_name']." ".$user['last_name']."<br>Your Hotel Booking has been confirmed.<br>Regards";
	}
	$headers = 'From: Hotel Bookings <'.$hotel['notify_email'].'>' . "\r\n" .
	'Reply-To: '.$hotel['notify_email'] . "\r\n" .
	'Content-type:text/html' . "\r\n" .
	'MIME-Version: 1.0' . "\r\n" .
	'X-Mailer: PHP/' . phpversion();
	$email  = mail($to, $email_subject, $email_text, $headers);
	($email)? true:false;
}
if(isset($_POST['sbt_hotel_details'])){
	foreach($_POST as $key=>$value){$$key=$value;}
	if(isset($_FILES['logo']['name']) and $_FILES['logo']['name']!=""){
		$ex = explode(".", $_FILES['logo']['name']);
		if(end($ex)=="jpg" or end($ex)=="JPG" or end($ex)=="png" or end($ex)=="PNG" or end($ex)=="jpeg" or end($ex)=="JPEG" or end($ex)=="gif"){
			$file = $_SESSION['hotel_id'].".".end($ex);
			move_uploaded_file($_FILES['logo']['tmp_name'], "../../../hotel/images/hotels/logo/".$file);
		}
	}
	
	$q = mysql_query("update bsi_hotels set hotel_name='".mysql_real_escape_string($hotel_name)."', hotel_short_description='".mysql_real_escape_string($short_desc)."', hotel_full_description='".mysql_real_escape_string($full_desc)."', hotel_address='".mysql_real_escape_string($str_addr)."', hotel_latitude_longitude='".mysql_real_escape_string($hotel_latlong)."', hotel_logo='{$file}', hotel_amenities='".mysql_real_escape_string(implode("|",$amenity))."', hotel_amenities_description='".mysql_real_escape_string($amenities_desc)."', hotel_info_type='".mysql_real_escape_string($hotel_type)."', hotel_info_extra_people='".mysql_real_escape_string($extra_people)."', hotel_info_min_stay='".mysql_real_escape_string($min_stay)."', hotel_info_security_deposit='".mysql_real_escape_string($security_deposit)."', hotel_info_country_id='".mysql_real_escape_string($country)."', hotel_info_city='".mysql_real_escape_string($city)."', hotel_info_cancellation='".mysql_real_escape_string($cancellation)."', hotel_terms='".mysql_real_escape_string($hotel_terms)."', help_box_text='".mysql_real_escape_string($help_text)."', why_book_with_us='".mysql_real_escape_string($why_book_us)."', phone='".mysql_real_escape_string($phone)."', email='".mysql_real_escape_string($email)."' where hotel_id=".$_SESSION['hotel_id']." limit 1");
	if($q){ header("Location: admin_hotel_details.php?msg_type=success&message=Hotel%20Details%20updated%20successfully!"); }
	else{ header("Location: admin_hotel_details.php?msg_type=error&message=Failed%20to%20update!"); }
}
if(isset($_POST['old_create_hotel'])){
	foreach($_POST as $key=>$value){$$key=$value;}
	$qlast = mysql_fetch_array(mysql_query("select hotel_id from bsi_hotels order by hotel_id desc limit 1"));
	$last_id = $qlast['hotel_id']+1;
	if(isset($_FILES['logo']['name']) and $_FILES['logo']['name']!=""){
		$ex = explode(".", $_FILES['logo']['name']);
		if(end($ex)=="jpg" or end($ex)=="JPG" or end($ex)=="png" or end($ex)=="PNG" or end($ex)=="jpeg" or end($ex)=="JPEG" or end($ex)=="gif"){
			$file = $last_id.".".end($ex);
			move_uploaded_file($_FILES['logo']['tmp_name'], "../../../hotel/images/hotels/logo/".$file);
		}
	}
	if(isset($_FILES['cover']['name']) and $_FILES['cover']['name']!=""){
		$ex = explode(".", $_FILES['cover']['name']);
		if(end($ex)=="jpg" or end($ex)=="JPG" or end($ex)=="png" or end($ex)=="PNG" or end($ex)=="jpeg" or end($ex)=="JPEG" or end($ex)=="gif"){
			$file2 = $last_id.".".end($ex);
			move_uploaded_file($_FILES['cover']['tmp_name'], "../../../hotel/images/hotels/cover/".$file2);
		}
	}
	$perma = createHotelPermalink($last_id, $hotel_name);
	$q = mysql_query("insert into bsi_hotels (hotel_id,hotel_name,hotel_short_description,hotel_full_description,hotel_address,hotel_latitude_longitude,hotel_logo,hotel_cover_image,hotel_amenities,hotel_amenities_description,hotel_info_type,hotel_info_extra_people,hotel_info_min_stay,hotel_info_security_deposit,hotel_info_country_id,hotel_info_city,hotel_info_cancellation,hotel_terms,help_box_text,why_book_with_us,phone,email,notify_email,theme_color,permalink,paid_upto,status,hotel_added) values ('{$last_id}', '".mysql_real_escape_string($hotel_name)."','".mysql_real_escape_string($short_desc)."','".mysql_real_escape_string($full_desc)."', '".mysql_real_escape_string($str_addr)."', '".mysql_real_escape_string($hotel_latlong)."', '{$file}', '{$file2}', '".mysql_real_escape_string(implode("|",$amenity))."', '".mysql_real_escape_string($amenities_desc)."', '".mysql_real_escape_string($hotel_type)."', '".mysql_real_escape_string($extra_people)."', '".mysql_real_escape_string($min_stay)."', '".mysql_real_escape_string($security_deposit)."', '".mysql_real_escape_string($country)."', '".mysql_real_escape_string($city)."', '".mysql_real_escape_string($cancellation)."', '".mysql_real_escape_string($hotel_terms)."', '".mysql_real_escape_string($help_text)."', '".mysql_real_escape_string($why_book_us)."', '".mysql_real_escape_string($phone)."', '".mysql_real_escape_string($email)."','{$notify_email}','#f5f5f5','{$perma}','3000-01-01 23:59:59','1','".date("Y-m-d H:i:s")."')");
	mysql_query("insert into bsi_payment_gateway (hotel_id,gateway_name,gateway_code,enabled) values ('{$last_id}','PayPal','pp','0')");
	mysql_query("insert into bsi_payment_gateway (hotel_id,gateway_name,gateway_code,enabled) values ('{$last_id}','2Checkout','co','0')");
	mysql_query("insert into bsi_payment_gateway (hotel_id,gateway_name,gateway_code,enabled) values ('{$last_id}','Authorize.Net(DPM)','auth','0')");
	mysql_query("insert into bsi_payment_gateway (hotel_id,gateway_name,gateway_code,enabled) values ('{$last_id}','Stripe','stripe','0')");
	mysql_query("insert into bsi_payment_gateway (hotel_id,gateway_name,gateway_code,enabled) values ('{$last_id}','Manual : Pay on Arrival','manual','1')");
	mysql_query("insert into bsi_payment_gateway (hotel_id,gateway_name,gateway_code,enabled) values ('{$last_id}','Offline Credit Card','cc','0')");
	if($q){ $_SESSION['val1'] = "Hotel Added Successfully!"; }
	else{ $_SESSION['val1'] = "Failed to add hotel!"; }
	header("Location: create_hotel.php");
	exit();
}
if(isset($_POST['create_hotel'])){
	foreach($_POST as $key=>$value){$$key=$value;}
	$qlast = mysql_fetch_array(mysql_query("select hotel_id from bsi_hotels order by hotel_id desc limit 1"));
	$last_id = $qlast['hotel_id']+1;
	if(isset($_FILES['logo']['name']) and $_FILES['logo']['name']!=""){
		$ex = explode(".", $_FILES['logo']['name']);
		if(end($ex)=="jpg" or end($ex)=="JPG" or end($ex)=="png" or end($ex)=="PNG" or end($ex)=="jpeg" or end($ex)=="JPEG" or end($ex)=="gif"){
			$file = $last_id.".".end($ex);
			move_uploaded_file($_FILES['logo']['tmp_name'], "../../../hotel/images/hotels/logo/".$file);
		}
	}
	if(isset($_FILES['cover']['name']) and $_FILES['cover']['name']!=""){
		$ex = explode(".", $_FILES['cover']['name']);
		if(end($ex)=="jpg" or end($ex)=="JPG" or end($ex)=="png" or end($ex)=="PNG" or end($ex)=="jpeg" or end($ex)=="JPEG" or end($ex)=="gif"){
			$file2 = $last_id.".".end($ex);
			move_uploaded_file($_FILES['cover']['tmp_name'], "../../../hotel/images/hotels/cover/".$file2);
		}
	}
	$perma = createHotelPermalink($last_id, $hotel_name);
	$q = mysql_query("insert into bsi_hotels (hotel_id,hotel_name,hotel_short_description,hotel_full_description,hotel_address,hotel_latitude_longitude,hotel_logo,hotel_cover_image,hotel_amenities,hotel_amenities_description,hotel_info_type,hotel_info_extra_people,hotel_info_min_stay,hotel_info_security_deposit,hotel_info_country_id,hotel_info_city,hotel_info_cancellation,hotel_terms,help_box_text,why_book_with_us,phone,email,notify_email,theme_color,permalink,paid_upto,status,hotel_added) values ('{$last_id}', '".mysql_real_escape_string($hotel_name)."','".mysql_real_escape_string($short_desc)."','".mysql_real_escape_string($full_desc)."', '".mysql_real_escape_string($str_addr)."', '".mysql_real_escape_string($hotel_latlong)."', '{$file}', '{$file2}', '".mysql_real_escape_string(implode("|",$amenity))."', '".mysql_real_escape_string($amenities_desc)."', '".mysql_real_escape_string($hotel_type)."', '".mysql_real_escape_string($extra_people)."', '".mysql_real_escape_string($min_stay)."', '".mysql_real_escape_string($security_deposit)."', '".mysql_real_escape_string($country)."', '".mysql_real_escape_string($city)."', '".mysql_real_escape_string($cancellation)."', '".mysql_real_escape_string($hotel_terms)."', '".mysql_real_escape_string($help_text)."', '".mysql_real_escape_string($why_book_us)."', '".mysql_real_escape_string($phone)."', '".mysql_real_escape_string($email)."','{$notify_email}','#f5f5f5','{$perma}','3000-01-01 23:59:59','1','".date("Y-m-d H:i:s")."')");
	mysql_query("insert into bsi_payment_gateway (hotel_id,gateway_name,gateway_code,enabled) values ('{$last_id}','iPay88','ipay88','0')");
	mysql_query("insert into bsi_payment_gateway (hotel_id,gateway_name,gateway_code,enabled) values ('{$last_id}','Manual : Pay on Arrival','manual','1')");
	if($q){ $_SESSION['val1'] = "Hotel Added Successfully!"; }
	else{ $_SESSION['val1'] = "Failed to add hotel!"; }
	header("Location: create_hotel.php");
	exit();
}
if(isset($_POST['submitPackages'])){
	foreach($_POST as $key=>$value){$$key=$value;}
	if($pid==0){ mysql_query("insert into hotel_packages (hotel_pkg_name, hotel_pkg_price, hotel_pkg_room_limit, hotel_pkg_status) values ('{$pkg_name}','{$pkg_price}','{$pkg_rooms}','1')"); }
	else{ mysql_query("update hotel_packages set hotel_pkg_name='{$pkg_name}', hotel_pkg_price='{$pkg_price}', hotel_pkg_room_limit='{$pkg_rooms}' where hotel_pkg_id={$pid} limit 1"); }
	header("Location: hotel_packages_list.php");
	exit();
}
if(isset($_POST['submitRoom'])){
	foreach($_POST as $key=>$value){$$key=$value;}
	$left = rooms_left();
	if($page==0){ $qm = mysql_fetch_array(mysql_query("select max(room_id) as maxi from bsi_room")); $room = $qm['maxi']+1; }
	else { $qm = mysql_fetch_array(mysql_query("select no_of_rooms,room_id from bsi_room where roomtype_id={$roomtype_id} and capacity_id={$capacity_id} and hotel_id={$hotel} limit 1")); $room = $qm['room_id']; $rooms=$qm['no_of_rooms']; }
	if(isset($_FILES['room_pic']['name']) and $_FILES['room_pic']['name']!=""){
		$ex = explode(".", $_FILES['room_pic']['name']);
		if(end($ex)=="jpg" or end($ex)=="JPG" or end($ex)=="png" or end($ex)=="PNG" or end($ex)=="jpeg" or end($ex)=="JPEG" or end($ex)=="gif"){
			$file = $room.".".end($ex);
			
			move_uploaded_file($_FILES['room_pic']['tmp_name'], "../hotel/images/hotels/room/".$file);
		}
	}
	if(!isset($_POST['hot'])){$hot=0;}else{$hot=1;}
	if($page!=0){
		if($no_of_room>($rooms+$left) OR $no_of_room<0){ $no_of_room=$rooms+$left; }
		$q = mysql_query("update bsi_room set room_rate='".mysql_real_escape_string($room_rate)."', room_description='".mysql_real_escape_string($room_desc)."', room_basic_pic='{$file}', room_basic_amenities='".mysql_real_escape_string(implode("|",$amenity))."', room_guests_capacity='".mysql_real_escape_string($room_guests)."', no_of_rooms='".mysql_real_escape_string($no_of_room)."', hot='{$hot}' where room_id={$room} limit 1");
	} else {
		if($no_of_room>$left OR $no_of_room<0){$no_of_room=$left;}
		$q = mysql_query("insert into bsi_room (hotel_id, roomtype_id, capacity_id, room_rate, room_description, room_basic_pic, room_basic_amenities, room_guests_capacity, no_of_rooms, hot, status) values ('".mysql_real_escape_string($hotel)."', '".mysql_real_escape_string($roomtype_id)."', '".mysql_real_escape_string($capacity_id)."', '".mysql_real_escape_string($room_rate)."', '".mysql_real_escape_string($room_desc)."', '".mysql_real_escape_string($file)."', '".mysql_real_escape_string(implode("|",$amenity))."', '".mysql_real_escape_string($room_guests)."', '".mysql_real_escape_string($no_of_room)."', '".mysql_real_escape_string($hot)."', '1')");
	}
	//var_dump($_POST);
	if($q){ header("Location: room_list.php?msg_type=success&message=Room%20Details%20updated%20successfully! "); }
	else{ header("Location: room_list.php?msg_type=error&message=Failed%20to%20update!"); }
}
if(isset($_POST['submitExtras'])){
	foreach($_POST as $key=>$value){$$key=$value;}
	if(!isset($_POST['status'])){$status=0;}else{$status=1;}
	
   $slug=preg_replace('/[^A-Za-z0-9-]+/', '_', $extras_title);
	
	if($page!=0){
		$q = mysql_query("update booking_extras set slug='{$slug}', extras_title='".mysql_real_escape_string($extras_title)."', extras_description='".mysql_real_escape_string($extras_description)."', extras_rate='".mysql_real_escape_string($extras_rate)."', extras_per='".mysql_real_escape_string($extras_per)."', status='{$status}' where extras_id={$page} limit 1");
	} else {
		$q = mysql_query("insert into booking_extras (hotel_id, slug, extras_title, extras_description, extras_rate, extras_per, status) values ('".mysql_real_escape_string($hotel)."', '{$slug}', '".mysql_real_escape_string($extras_title)."', '".mysql_real_escape_string($extras_description)."', '".mysql_real_escape_string($extras_rate)."', '".mysql_real_escape_string($extras_per)."', '".$status."')");
	}
	//var_dump($_POST);
	//var_dump("insert into booking_extras (hotel_id, extras_title, extras_description, extras_rate, extras_per, status) values ('".mysql_real_escape_string($hotel)."', '".mysql_real_escape_string($extras_title)."', '".mysql_real_escape_string($extras_description)."', '".mysql_real_escape_string($extras_rate)."', '".mysql_real_escape_string($extras_per)."', '".$status."')");
	//var_dump($_POST);
	if($q){ header("Location: booking_extra.php?msg_type=success&message=Booking%20Extras%20updated%20successfully!"); }
	else{ header("Location: booking_extra.php?msg_type=error&message=Failed%20to%20update!"); }
}
if(isset($_POST['submitPlan'])){
	foreach($_POST as $key=>$value){$$key=$value;}
	if($start_date!=""){$start=date("Y-m-d", strtotime($start_date));}else{$start=NULL;}
	if($end_date!=""){$end=date("Y-m-d", strtotime($end_date));}else{$end=NULL;}
	if($page=="add"){
		$q = mysql_query("insert into bsi_priceplan (room_id,hotel_id,start_date,end_date,sun,mon,tue,wed,thu,fri,sat,status) values ('{$room_id}', '{$hotel}', '{$start}','{$end}', '{$sun}', '{$mon}', '{$tue}', '{$wed}', '{$thu}', '{$fri}', '{$sat}', '1')");
	} else {
		$q = mysql_query("update bsi_priceplan set room_id='{$room_id}',hotel_id='{$hotel}',start_date='{$start}',end_date='{$end}',sun='{$sun}',mon='{$mon}',tue='{$tue}',wed='{$wed}',thu='{$thu}',fri='{$fri}',sat='{$sat}' where plan_id=".$plan." limit 1");
	}
	if($q){ header("location: priceplan.php?msg_type=success&message=Price%20Plan%20updated%20successfully! "); }
}
if(isset($_GET['delplan']) and $_GET['delplan']!="" and is_numeric($_GET['delplan']) and $_GET['delplan']>0){
	mysql_query("update bsi_priceplan set status=-1 where plan_id=".$_GET['delplan']." limit 1");
	header("location: priceplan.php");
}
if(isset($_GET['confirm']) and $_GET['confirm']!="" and is_numeric($_GET['confirm']) and $_GET['confirm']>0){
    $q = mysql_query("update bsi_bookings set status=1 where booking_id=".$_GET['confirm']." limit 1");
    emailnow($_GET['confirm'], "Confirmation Email");
    $_SESSION['val1'] = "Booking confirmed and Confirmation email has been sent.";
    header("location: view_bookings.php");
    exit();
}
if(isset($_GET['reject']) and $_GET['reject']!="" and is_numeric($_GET['reject']) and $_GET['reject']>0){
    $q = mysql_query("update bsi_bookings set status=-1 where booking_id=".$_GET['reject']." limit 1");
    emailnow($_GET['reject'], "Cancellation Email");
    $_SESSION['val1'] = "Booking Rejected and cancellation email has been sent.";
    header("location: view_bookings.php");
    exit();
}
if(isset($_POST['saveConfig'])){
	foreach($_POST as $key=>$value){$$key=$value;}
	if(isset($_FILES['cover']['name']) and $_FILES['cover']['name']!=""){
		$ex = explode(".", $_FILES['cover']['name']);
		if(end($ex)=="jpg" or end($ex)=="JPG" or end($ex)=="png" or end($ex)=="PNG" or end($ex)=="jpeg" or end($ex)=="JPEG" or end($ex)=="gif"){
			$file2 = $_SESSION['hotel_id'].".".end($ex);
			move_uploaded_file($_FILES['cover']['tmp_name'], "../../../hotel/images/hotels/cover/".$file2);
		}
	}
	//,default_language='{$language}',default_currency='{$currency}'
	mysql_query("update bsi_hotels set theme_color='{$colorcode}', hotel_cover_image='{$file2}', notify_email='{$notify_email}', timezone='{$timezone}' where hotel_id=".$_SESSION['hotel_id']." limit 1");
}
if(isset($_POST['uploadImages'])){
	$room_id = $_POST['room'];
	if(isset($_FILES['roomimg']['name'][0]) and $_FILES['roomimg']['name'][0]!=""){
		$co = count($_FILES['roomimg']['name']);
		$q = mysql_fetch_array(mysql_query("select max(pic_id) as maxi from bsi_gallery where hotel_id=".$_SESSION['hotel_id']));
		for($i=0; $i<$co; $i++){
			$ex = explode(".", $_FILES['roomimg']['name'][$i]);
			if(end($ex)=="jpg" or end($ex)=="JPG" or end($ex)=="png" or end($ex)=="PNG" or end($ex)=="jpeg" or end($ex)=="JPEG" or end($ex)=="gif"){
				$file2 = ($q['maxi']+1).".".end($ex);
				move_uploaded_file($_FILES['roomimg']['tmp_name'][$i], "../../../hotel/images/hotels/galleries/".$_SESSION['hotel_id']."/".$file2);
				mysql_query("insert into bsi_gallery (room_id, hotel_id, img_path, status) values ('{$room_id}', '".$_SESSION['hotel_id']."', '{$file2}', '1' )");
				//make_thumbnails("../../hotel/images/hotels/galleries/".$_SESSION['hotel_id']."/", $file2);
						
				$thumbnail_width	= 69;
				$thumbnail_height	= 69;
				$thumb_preword		= "thumb_";
				$arr_image_details	= GetImageSize("../../../hotel/images/hotels/galleries/".$_SESSION['hotel_id']."/".$file2);
				$original_width		= $arr_image_details[0];
				$original_height	= $arr_image_details[1];
				if( $original_width > $original_height ){
					$new_width	= $thumbnail_width;
					$new_height	= intval($original_height*$new_width/$original_width);
				} else {
					$new_height	= $thumbnail_height;
					$new_width	= intval($original_width*$new_height/$original_height);
				}
				$dest_x = intval(($thumbnail_width - $new_width) / 2);
				$dest_y = intval(($thumbnail_height - $new_height) / 2);
				
				if($arr_image_details[2]==1) { $imgt = "ImageGIF"; $imgcreatefrom = "ImageCreateFromGIF";  }
				if($arr_image_details[2]==2) { $imgt = "ImageJPEG"; $imgcreatefrom = "ImageCreateFromJPEG";  }
				if($arr_image_details[2]==3) { $imgt = "ImagePNG"; $imgcreatefrom = "ImageCreateFromPNG";  }
						
				if( $imgt ) { 
					$old_image	= $imgcreatefrom("../../../hotel/images/hotels/galleries/".$_SESSION['hotel_id']."/".$file2);
					$new_image	= imagecreatetruecolor($thumbnail_width, $thumbnail_height);
					imageCopyResized($new_image,$old_image,0, 0,0,0,69,69,$original_width,$original_height);
					$imgt($new_image,"../../../hotel/images/hotels/galleries/".$_SESSION['hotel_id']."/thumbnail/".$file2);
				}	
			}
		}
	}
}
if(isset($_GET['delpic']) and $_GET['delpic']!=""){
	mysql_query("update bsi_gallery set status=-1 where pic_id=".$_GET['delpic']." limit 1");
	header("location: ".$_SERVER['HTTP_REFERER']);
}
if(isset($_POST['submitBlockRoom'])){
	foreach($_POST as $key=>$value){$$key=$value;}
	if(room_available($room_id, strtotime($start_date), strtotime($end_date), $nums, $total_rooms)){
		$details=array();
		for($i=1; $i<=$nums; $i++){ $details[$i]['Blocked'] = ""; }
		mysql_query("insert into bsi_bookings (room_id, hotel_id, booking_time, start_date, end_date, booking_details_json, status) values ('{$room_id}', '".$_SESSION['hotel_id']."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d',strtotime($start_date))."', '".date('Y-m-d',strtotime($end_date))."', '".json_encode($details)."', '-3')");
	} else { $_SESSION['val1']="Rooms not available in these dates."; }
}
if(isset($_GET['unblock']) and $_GET['unblock']!="" and is_numeric(base64_decode($_GET['unblock'])-123) and base64_decode($_GET['unblock'])-123>0){
	mysql_query("update bsi_bookings set status=-4 where booking_id=".(base64_decode($_GET['unblock'])-123)." limit 1");
	$_SESSION['val1']="Unblocked Successfully!";
	header("location: room_block.php");
	exit();
}
// if(isset($_POST['old_savePaymentGateways'])){
// 	foreach($_POST as $key=>$value){$$key=trim($value);}
// 	if(isset($_POST['paypal_on'])){$enabled1=1;}else{$enabled1=0;}
// 	if($_POST['method_on']=="co"){$enabled2=1;}else{$enabled2=0;}
// 	if($_POST['method_on']=="auth"){$enabled3=1;}else{$enabled3=0;}
// 	if($_POST['method_on']=="stripe"){$enabled4=1;}else{$enabled4=0;}
// 	if(isset($_POST['manual_on'])){$enabled5=1;}else{$enabled5=0;}
// 	if($_POST['method_on']=="cc"){$enabled6=1;}else{$enabled6=0;}
// 	mysql_query("update bsi_payment_gateway set account='{$paypal_id}',enabled={$enabled1} where gateway_code='pp' and hotel_id=".$_SESSION['hotel_id']." limit 1");
// 	mysql_query("update bsi_payment_gateway set account='".$checkout_id."||".$checkout_word."',enabled={$enabled2} where gateway_code='co' and hotel_id=".$_SESSION['hotel_id']." limit 1");
// 	mysql_query("update bsi_payment_gateway set account='".$authorize_login_id."||".$authorize_md5_hash."||".$authorize_trans_key."',enabled={$enabled3} where gateway_code='auth' and hotel_id=".$_SESSION['hotel_id']." limit 1");
// 	mysql_query("update bsi_payment_gateway set account='".$stripe_secret_key."||".$stripe_publishable_key."',enabled={$enabled4} where gateway_code='stripe' and hotel_id=".$_SESSION['hotel_id']." limit 1");
// 	mysql_query("update bsi_payment_gateway set enabled={$enabled5} where gateway_code='manual' and hotel_id=".$_SESSION['hotel_id']." limit 1");
// 	mysql_query("update bsi_payment_gateway set enabled={$enabled6} where gateway_code='cc' and hotel_id=".$_SESSION['hotel_id']." limit 1");
// 	$_SESSION['val1'] = "Payment Gateways updated successfully!";
// 	header("location: payment_gateway.php");
// 	exit();
// }
if(isset($_POST['savePaymentGateways'])){
	foreach($_POST as $key=>$value){$$key=trim($value);}
	if(isset($_POST['ipay88_on'])){$enabled1=1;}else{$enabled1=0;}
	if(isset($_POST['manual_on'])){$enabled2=1;}else{$enabled2=0;}
	mysql_query("update bsi_payment_gateway set account='".$merchant_code."||".$merchant_key."', enabled={$enabled1} where gateway_code='ipay88' and hotel_id=".$_SESSION['hotel_id']." limit 1");
	mysql_query("update bsi_payment_gateway set enabled={$enabled2} where gateway_code='manual' and hotel_id=".$_SESSION['hotel_id']." limit 1");
	$_SESSION['val1'] = "Payment Gateways updated successfully!";
	header("location: payment_gateway.php");
	exit();
}
if(isset($_GET['delCoupon']) and $_GET['delCoupon']!="" and is_numeric($_GET['delCoupon']) and $_GET['delCoupon']>0){
	mysql_query("update promotions set status=-1 where promo_id=".$_GET['delCoupon']." limit 1");
	$_SESSION['val1'] = "Promotion deleted successfully!";
	header("location: view_special_offer.php");
	exit();
}
if(isset($_POST['updateCoupon'])){
	foreach($_POST as $key=>$value){$$key=trim($value);}
	if($id==0){
		$num = mysql_num_rows(mysql_query("select promo_id from promotions where coupon_code='{$coupon_code}'"));
		if($num>0){ $_SESSION['val1'] = "Coupon code already Exists!"; }
		else { mysql_query("insert into promotions (hotel_id, discount, discount_type, coupon_code, room_id, start_date, end_date, status) values ('".$_SESSION['hotel_id']."', '{$amount}', '1', '{$coupon_code}', '".$room_id."', '".date("Y-m-d",strtotime($fromDate))."', '".date("Y-m-d",strtotime($toDate))."', '1')");
		$_SESSION['val1'] = "Promotion Added successfully!"; }
	} else {
		mysql_query("update promotions set discount='{$amount}', room_id={$room_id}, start_date='".date("Y-m-d",strtotime($fromDate))."', end_date='".date("Y-m-d",strtotime($toDate))."' where promo_id={$id} limit 1");
		$_SESSION['val1'] = "Promotion Updated successfully!";
	}
	header("location: view_special_offer.php");
	exit();
}
if(isset($_POST['entryGroupDiscount'])){
	foreach($_POST as $key=>$value){$$key=trim($value);}

	if (isset($id)) {
		// UPDATE
		$num = mysql_num_rows(mysql_query("select id from bsi_discount_group where lower(name)=lower('{$name}') AND hotel_id='{$hotel_id}' AND id<>'{$id}'"));
	} else {
		$num = mysql_num_rows(mysql_query("select id from bsi_discount_group where lower(name)=lower('{$name}') AND hotel_id='{$hotel_id}'"));
	}

	if ($num > 0 ) {
		$_SESSION['msg_error'] = "Group name has been used!";
	} else {
		if (isset($id)) {
			mysql_query("UPDATE bsi_discount_group SET room_id='{$room_id}', name='{$name}', discount='{$discount}' WHERE id='{$id}' AND hotel_id='{$hotel_id}'");
		} else {
			mysql_query("INSERT INTO bsi_discount_group (hotel_id, room_id, name, discount) VALUES ('{$hotel_id}', '{$room_id}', '{$name}', '{$discount}')");
		}
		$_SESSION['msg_success'] = "Group successfully saved!";
	}
	header("location: manage_group.php");
	exit();
}
if(isset($_POST['saveCustomer'])){
	foreach($_POST as $key=>$value){$$key=trim($value);}

	if (isset($id)) {
		$num = mysql_num_rows(mysql_query("select user_id from users where lower(email)=lower('{$email}') AND id<>'{$id}'"));
	} else {
		$num = mysql_num_rows(mysql_query("select user_id from users where lower(email)=lower('{$email}')"));
	}

	if ($num > 0) {
		$_SESSION['msg_error'] = "Email already registered!";
	} else {
		if (isset($id)) {
			mysql_query("UPDATE users SET title='{$titled}', first_name='{$first_name}', last_name='{$last_name}', email='{$email}', street_address='{$address}', country_id='{$country}', city='{$city}', state='{$state}', phone='{$phone}' WHERE user_id='{$id}'");
			if ($group_id > 0) {
				$num2 = mysql_num_rows(mysql_query("SELECT user_id FROM bsi_discount_group_user WHERE user_id='{$id}' AND hotel_id='{$_SESSION['hotel_id']}'"));
				if ($num2 > 0) {
					mysql_query("UPDATE bsi_discount_group_user SET group_id='{$group_id}' WHERE hotel_id='{$_SESSION['hotel_id']}' AND user_id='{$id}'");
				} else {
					mysql_query("INSERT INTO bsi_discount_group_user (hotel_id, group_id, user_id) VALUES ('{$_SESSION['hotel_id']}', '{$group_id}', '{$id}')");
				}
			} else {
				mysql_query("DELETE FROM bsi_discount_group_user WHERE user_id='{$id}' AND hotel_id='{$_SESSION['hotel_id']}'");
			}
		} else {
			mysql_query("INSERT INTO users (title, first_name, last_name, email, password, street_address, country_id, city, state, phone) VALUES ('{$titled}', '{$first_name}', '{$last_name}', '{$email}', '{$password}', '{$address}', '{$country}', '{$city}', '{$state}', '{$phone}')");
			if ($groupId > 0) {
				mysql_query("INSERT INTO bsi_discount_group_user (group_id, user_id) VALUES ('{$groupId}', LAST_INSERT_ID())");
			}
		}

		$_SESSION['msg_success'] = "Customer successfully saved!";
	}
	header("location: manage_customer.php");
	exit();
}
if(isset($_POST['createCustomer'])){
	foreach($_POST as $key=>$value){$$key=trim($value);}
	//mysql_query("update users set discount='{$amount}', room_id={$room_id}, hotel_id={$hotel_id} where user_id={$id} limit 1") or die ('Error updating database: '.mysql_error());
	
	if ($state == "") $state="Indonesia";
	mysql_query("insert into users (title, first_name, last_name, email, password, street_address, city, state, phone) values ('{$title}', '{$first_name}', '{$last_name}', '{$email}', '{$password}', '{$street_address}', '{$city}', '{$state}', '{$phone}')");
	mysql_query("insert into user_discount (user_id, hotel_id, room_id, discount) values (LAST_INSERT_ID(),'{$hotel_id}', '{$room_id}', '{$amount}')");
	$_SESSION['val1'] = "Promotion Added successfully!";

	//var_dump($row = mysql_fetch_assoc(mysql_query("SELECT LAST_INSERT_ID()")));
	header("location: customerdiscount.php");
	exit();
}
if(isset($_POST['updateDiscount'])){
	foreach($_POST as $key=>$value){$$key=trim($value);}
	//mysql_query("update users set discount='{$amount}', room_id={$room_id}, hotel_id={$hotel_id} where user_id={$id} limit 1") or die ('Error updating database: '.mysql_error());
	
	if($did==0){
		mysql_query("insert into user_discount (user_id, hotel_id, room_id, discount) values ('{$id}','{$hotel_id}', '{$room_id}', '{$amount}')");
		$_SESSION['val1'] = "Promotion Added successfully!";
	} else {
		mysql_query("update user_discount set discount='{$amount}', room_id='{$room_id}' where hotel_id='{$hotel_id}' AND user_id = '{$id}' limit 1");
		$_SESSION['val1'] = "Promotion Updated successfully!";
	}

	header("location: customerdiscount.php");
	exit();
}
if(isset($_POST['saveEmailConfig'])){
	foreach($_POST as $key=>$value){$$key=trim($value);}
	if($emailid==0){
		if($emailtype==1){$email_name="Confirmation Email";}
		else{$email_name="Cancellation Email";}
		mysql_query("insert into bsi_email_contents (email_name, email_subject, email_text, hotel_id) values ('{$email_name}', '".mysql_real_escape_string($subject)."', '".mysql_real_escape_string($email_text)."', '".$_SESSION['hotel_id']."')");
	} else {
		mysql_query("update bsi_email_contents set email_subject='".mysql_real_escape_string($subject)."', email_text='".mysql_real_escape_string($email_text)."' where id={$emailid} limit 1");
	}
	$_SESSION['val1'] = "Email Content Updated successfully!";
	header("location: email_content.php");
	exit();
}
if(isset($_POST['add_manager'])){
	foreach($_POST as $key=>$value){$$key=trim($value);}
	if($id=="0"){
		$already = mysql_num_rows(mysql_query("select manager_id from managers where username='{$username}'"));
		if($already>0){ $_SESSION['val1'] = "Manager already exists!"; }
		else{
			$q = mysql_query("insert into managers (fname, lname, email, username, pass, hotel_id, dated, status) values ('{$fname}', '{$lname}', '{$email}', '{$username}', '".md5($password)."', '{$hotel_id}', '".date("Y-m-d H:i:s", strtotime("now"))."', '1')");
			$_SESSION['val1'] = ($q)? "Manager Added successfully!" : "There is some problem adding this manager";
		}
	} else {
		if($password==""){ $qp = mysql_fetch_array(mysql_query("select pass from managers where manager_id=".$id." limit 1")); $pass=$qp['pass']; } else { $pass= md5($password); }
		$q = mysql_query("update managers set fname='{$fname}', lname='{$lname}', email='{$email}', username='{$username}', pass='{$pass}', hotel_id='{$hotel_id}' where manager_id={$id} limit 1");
		$_SESSION['val1'] = ($q)? "Manager Updated successfully!" : "There is some problem updating manager";
	}
	header("location: managers_list.php");
	exit();
}
if(isset($_POST['update_payments'])){
	foreach($_POST as $key=>$value){$$key=trim($value);}
	$q = mysql_query("update bsi_hotels set package='{$hotel_pkg}', paid_upto='".date("Y-m-d", strtotime($paid_upto)).' 23:59:59'."' where hotel_id='{$hotel_id}' limit 1");
	$_SESSION['val1'] = ($q)? "Hotel Payments Updated successfully!" : "There is some problem updating manager";
	header("location: hotels_payments.php");
	exit();
}
?>