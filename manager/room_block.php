<?php
include("access.php");
include 'includes/top.php';
include("main_includes/admin.class.php");
include("main_includes/conf.class.php");
?>
    <div id="wrapper">
<?php include 'includes/left.php'; ?>

        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">
                    <h3>Room Blocking</h3>                    
                </div><!-- End .heading-->
                <!-- Build page from here: -->
<?php 
if(isset($_SESSION['val1'])){
?>        
    <div class="row-fluid">
            <span class="span12">
        <div class="alert marginT10">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong>MESSAGE!</strong>
            <?php echo $_SESSION['val1'];
                  unset($_SESSION['val1'])?>
        </div>
      </span>
    </div>
<?php
}
?>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="box gradient">
                                <div class="title">
                                    <h4>
                                        <span>Room Blocking</span>
                                        <form class="box-form right">
                                          <button class="btn btn-info btn-mini" id="add_room_block" type="button">Block another Room</button>
                                        </form>
                                    </h4>
                                </div>
                                <div class="content noPad clearfix">
                                    <form id="add_room_block_form" action="" method="post">
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Select Room</label>
                                                    <select required name="room_id" style="opacity: 0;">
                                                        <?php 
                                                        
                                                        $roomq = mysql_query("select room_id,no_of_rooms,roomtype_id,capacity_id,room_rate from bsi_room where status=1 and hotel_id=".$_SESSION['hotel_id']);
                                                        while($room = mysql_fetch_array($roomq)){
                                                            $cap = mysql_fetch_array(mysql_query("select title from bsi_capacity where id=".$room['capacity_id']));
                                                            $type = mysql_fetch_array(mysql_query("select type_name from bsi_roomtype where roomtype_id=".$room['roomtype_id']));
                                                            echo '<option value="'.$room['room_id'].'">'.$cap['title']." ".$type['type_name']." (".$room['no_of_rooms'].' Rooms)</option>';
                                                        }
                                                        echo "</select>";
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Start Date</label>
                                                    <input class="span2" required id="txtFromDate" name="start_date" type="text" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">End Date</label>
                                                    <input class="span2" required id="txtToDate" name="end_date" type="text" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">No. of Rooms</label>
                                                    <input class="span2" value="1" required name="nums" min="1" type="number" placeholder="i.e 2" />
                                                    <input type="hidden" name="total_rooms" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <div class="form-actions" style="margin-bottom:0px">
                                                    <div class="span3"></div>
                                                    <div class="span9 controls">
                                                      <button type="submit" class="btn right marginR10" name="submitBlockRoom">Submit<span class="icon12 brocco-icon-forward"></span></button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table" width="100%">
                                      <thead>
                                        <tr>
                                          <th>Room Name</th>
                                          <th>Date Range</th>
                                          <th>Total Rooms</th>
                                          <th>Blocked On</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                            $qb = mysql_query("select * from bsi_bookings where status=-3 and hotel_id=".$_SESSION['hotel_id']);
                                            while($rb = mysql_fetch_array($qb)){
                                                $roomq = mysql_fetch_array(mysql_query("select room_id,no_of_rooms,roomtype_id,capacity_id from bsi_room where room_id=".$rb['room_id']));
                                                $cap = mysql_fetch_array(mysql_query("select title from bsi_capacity where id=".$roomq['capacity_id']));
                                                $type = mysql_fetch_array(mysql_query("select type_name from bsi_roomtype where roomtype_id=".$roomq['roomtype_id']));
                                                echo "<tr><td>".$cap['title']." ".$type['type_name']." (".$roomq['no_of_rooms'].")</td>
                                                <td>".date("M/d/Y",strtotime($rb['start_date']))." - ".date("M/d/Y",strtotime($rb['end_date']))."</td><td>".count(json_decode($rb['booking_details_json'],true))."</td><td>".date("M/d/Y H:i:s",strtotime($rb['booking_time']))."</td><td><a href='room_block.php?unblock=".base64_encode($rb['booking_id']+123)."'>unblock</a></td></tr>";
                                            }
                                      ?>

                                      </tbody>
                                    </table>
                                </div>

                            </div><!-- End .box -->

                        </div><!-- End .span12 -->

                    </div><!-- End .row-fluid -->
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    <script type="text/javascript">
        $(function(){

        $("#add_room_block_form").hide();
        $("#add_room_block").click(function(){
            $("#add_room_block_form").toggle("slow");
        })
        var m = parseInt($("select[name=room_id] option:selected").text().replace(/[^0-9]/g, ''));
        $("input[name=nums]").attr("max", m);
        $("input[name=total_rooms]").val(m);
        $("select[name=room_id]").on("change", function(){
            var m = parseInt($("select[name=room_id] option:selected").text().replace(/[^0-9]/g, ''));
            $("input[name=nums]").attr("max", m);
            $("input[name=total_rooms]").val(m);
        })

         $.datepicker.setDefaults( $.datepicker.regional[ "en" ] );
        //$.datepicker.setDefaults({ dateFormat: '' });
       $("#txtFromDate").datepicker({
           maxDate: "+365D",
           numberOfMonths: 1,
           minDate: "<?=date('m/d/Y')?>",
           onSelect: function(selected) {
         var date = $(this).datepicker('getDate');
            if(date){
               date.setDate(date.getDate());
             }
             $("#txtToDate").datepicker("option","minDate", date);
             $("#txtToDate").val(selected);
           }
       });

       $("#txtToDate").datepicker({ 
           maxDate:"+365D",
           numberOfMonths: 1,
           onSelect: function(selected) {
              $("#txtFromDate").datepicker("option","maxDate", selected)
           }
       });  
   

    })
    </script>

    </body>
</html>