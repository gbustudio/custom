<?php 
include ("access.php");
include 'includes/top.php';
include("main_includes/conf.class.php");
if(isset($_GET['mid']) and $_GET['mid']!="" and is_numeric($_GET['mid']) and $_GET['mid']>0){
    $q=mysql_query("select * from managers where manager_id=".$_GET['mid']." limit 1");
    if(mysql_num_rows($q)<1){ echo "<script>window.location='add_manager.php';</script>"; exit(); }
    else{ $row=mysql_fetch_array($q); }
} else { $row=NULL; }
?>
    <div id="wrapper">
<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->
                <div class="heading">
                    <h3>Add/Edit Manager</h3>                    
                    
                </div><!-- End .heading-->
                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">
<?php 
if(isset($_SESSION['val1'])){
?>        
    <div class="row-fluid">
            <span class="span12">
        <div class="alert marginT10">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong>MESSAGE!</strong>
            <?php echo $_SESSION['val1'];
                  unset($_SESSION['val1'])?>
        </div>
      </span>
    </div>
<?php
}
?>
                            <div class="box">
                                <div class="title">
                                    <h4>
                                        <span class="icon16 "></span>
                                        <span>Add/Edit Manager</span>
                                    </h4>
                                    
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                        <form style="margin-bottom:0px !important" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">First Name</label>
                                                    <input class="span4" required name="fname" type="text" value="<?=$row['fname']?>" placeholder="Manager First Name" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Last Name</label>
                                                    <input class="span4" required name="lname" type="text" value="<?=$row['lname']?>" placeholder="Manager Last Name" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Email</label>
                                                    <input class="span4" required name="email" type="email" value="<?=$row['email']?>" placeholder="Manager Email ID" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Username</label>
                                                    <input class="span4" required name="username" type="text" value="<?=$row['username']?>" placeholder="Username to Login" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Password</label>
                                                    <input class="span4" <?=$row==NULL? "required":""?> name="password" type="password" placeholder="Password" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Assign Hotel</label>
                                                    <select <?=$row==NULL? "required":""?> name="hotel_id">
                                                    <option value="">Select Hotel</option>
                                                    <?php 
                                                    $cq = mysql_query("select hotel_id,hotel_name, hotel_info_city from bsi_hotels where status=1");
                                                    while($cr = mysql_fetch_array($cq)){
                                                        if($cr['hotel_id']==$row['hotel_id']){$se=" selected='selected'";}else{$se="";}
                                                        echo '<option value="'.$cr['hotel_id'].'" '.$se.'>'.$cr['hotel_name'].",".$cr['hotel_info_city'].'</option>';
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                                <div class="span12">
                                                    <div class="row-fluid">
                                                        <div class="form-actions" style="margin-bottom:0px">
                                                        <div class="span3"></div>
                                                        <div class="span9 controls">
                                                        <input type="hidden" name="id" value="<?=$row==NULL?"0":$row['manager_id']?>" />
                                                          <button type="submit" class="btn right marginR10" name="add_manager" id="sbt_details"><?php echo SUBMIT;?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                                                                
                                    </form>
                                </div>
                            </div>
                        </div><!-- End .span6 -->  
	
                    </div><!-- End .row-fluid -->
                
                
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>
    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>
    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->
    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    </body>
</html>
