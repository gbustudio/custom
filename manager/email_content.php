<?php 
include ("access.php");
include 'includes/top.php';
$q = mysql_query("select * from bsi_email_contents where hotel_id=".$_SESSION['hotel_id']);
if(mysql_num_rows($q)>0){
    $id=$email_name=$email_subject=$email_text=array();
    while($ra = mysql_fetch_array($q)){
        $id[] = $ra['id'];
        $email_name[] = $ra['email_name'];
        $email_subject[] = $ra['email_subject'];
        $email_text[] = $ra['email_text'];
    }
    if($email_name[0]=="Confirmation Email"){$email_name[1]="Cancellation Email";$t1=0;$t2=1;}
    else{$email_name[1]="Confirmation Email";$t1=1;$t2=0;}
} else {
    $email_subject=$email_text=NULL;
    $email_name[0] = "Confirmation Email";
    $email_name[1] = "Cancellation Email";
    $id[0]=0;$id[1]=0;
}

include("main_includes/conf.class.php");
//$hotel = mysql_fetch_array(mysql_query("select theme_color,hotel_cover_image from bsi_hotels where hotel_id=".$_SESSION['hotel_id']));
?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3>Email Content</h3>                    

                    

                </div><!-- End .heading-->

                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">

<?php 
if(isset($_SESSION['val1'])){
?>        
    <div class="row-fluid">
            <span class="span12">
        <div class="alert marginT10">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong>MESSAGE!</strong>
            <?php echo $_SESSION['val1'];
                  unset($_SESSION['val1'])?>
        </div>
      </span>
    </div>
<?php
}
?>
                            <div class="box">

                                <div class="title">

                                    <h4>
                                        <span class="icon16 "></span>
                                        <span>Email Content</span>
                                    </h4>
                                    
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
                                <script>tinymce.init({selector:'textarea',plugins:"link",menubar:false,statusbar: false,width:"525px",height:"300px"});</script>
                                        <form style="margin-bottom:0px !important" enctype="multipart/form-data" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" id="emailform" method="post">
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">

                                                    <label class="form-label span3" for="normal">Email Type</label>
                                                    <select name="emailtype">
                                                        <option value="">Select Email</option>
                                                        <?php
                                                        echo '<option value="1">'.$email_name[0].'</option>';    
                                                        echo '<option value="2">'.$email_name[1].'</option>';  
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Subject</label>
                                                    <input class="span6" name="subject" type="text" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Email Content</label>
                                                    <div class="span9" style="margin-left: 0px !important;">
														<textarea rows="15" id="email_text" name="email_text" type="text"></textarea>
													</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <div class="form-actions" style="margin-bottom:0px">
                                                    <div class="span3"></div>
                                                    <div class="span9 controls">
                                                    <input type="hidden" value="<?=$id[0]?>" name="emailid" />
                                                      <button type="submit" class="btn right marginR10" name="saveEmailConfig" id="sbt_details"><?php echo SUBMIT;?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- End .span6 -->  
                    </div><!-- End .row-fluid -->
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>

    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    <script type="text/javascript">
    $(function(){
    <?php if($email_text!=NULL and is_array($email_text)){ ?>
        $(document).on("change", "select[name=emailtype]", function(){
            var val = $(this).val();
            if(val==1){
               $("input[name=subject]").val("<?=$email_subject[$t1]?>");
               $("input[name=emailid]").val("<?=$id[$t1]?>");
               tinyMCE.activeEditor.setContent('<?=str_replace("\r\n","",$email_text[$t1])?>');
            } else if(val==2) {
                $("input[name=subject]").val("<?=$email_subject[$t2]?>");
                $("input[name=emailid]").val("<?=$id[$t2]?>");
                tinyMCE.activeEditor.setContent('<?=str_replace("\r\n","",$email_text[$t2])?>');
            } else {
                $("input[name=subject]").val("");
                tinyMCE.activeEditor.setContent('');
            }
        })
    <?php } ?>
    })
    </script>

    </body>
</html>
