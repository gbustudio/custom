<?php 
include("access.php");

if(isset($_POST['act'])){
	include("main_includes/db.conn.php");
	include("main_includes/admin.class.php");
	$bsiAdminMain->updateCustomerLookup();
	header("location:customerlookup.php"); 
	exit;
}

$update=base64_decode($_GET['update'])-123;

include 'includes/top.php';

if(isset($update)){
	include("main_includes/conf.class.php");
	include("main_includes/admin.class.php");
	$row   = $bsiAdminMain->getCustomerLookup($update);
	$title = $bsiAdminMain->getTitle($row['title']);
}else{
	header("location:customerlookup.php");
}

?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3>Customer Details Edit</h3>                    


                </div><!-- End .heading-->

                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">

 <?php 
if(isset($_GET['msg_type'])){
?>        
        <div class="alert marginT10 alert-<?php echo $_GET['msg_type'];?>">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong><?php echo strtoupper($_GET['msg_type']);?>!</strong>
            <?php echo $_GET['message'];?>
        </div>
<?php
}
?>
                            <div class="box">

                                <div class="title">

                                    <h4>
                                        <span class="icon16 "></span>
                                        <span>Customer Details Edit</span>
                                    </h4>
                                    
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                        <form style="margin-bottom:0px !important" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                          
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Title</label>
                                                    <?=$title?>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">First Name</label>
                                                    <input type="text" class="required" required value="<?=$row['first_name']?>" style="width:200px;" name="fname" id="fname"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Last Name</label>
                                                    <input type="text" class="required" required value="<?=$row['last_name']?>" style="width:200px;" name="sname" id="sname"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Street Address</label>
                                                    <input type="text" class="required" required value="<?=$row['street_address']?>" style="width:250px;" name="sadd" id="sadd"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">City</label>
                                                    <input type="text" class="required" required value="<?=$row['city']?>" style="width:250px;"  name="city" id="city"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">State</label>
                                                    <input type="text" class="required" required value="<?=$row['state']?>" style="width:250px;"  name="province" id="province"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Zip / Post code</label>
                                                    <input type="text" class="required" required value="<?=$row['zip']?>" style="width:150px;"  name="zip" id="zip"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Country</label>
                                                    <select  name="country">
                                                    <?php 
                                                    $cq = mysql_query("select * from countries where status=1");
                                                    while($cr = mysql_fetch_array($cq)){
                                                        if($cr['country_id']==$row['country_id']){$se=" selected='selected'";}else{$se="";}
                                                        echo '<option value="'.$cr['country_id'].'" '.$se.'>'.$cr['country_name'].'</option>';
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Identity Type</label>
                                                    <input type="text" class="required" required value="<?=$row['identity_type']?>"  style="width:250px;" name="identity_type" id="country"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Identity Number</label>
                                                    <input type="text" class="required" required value="<?=$row['identity_number']?>"  style="width:250px;" name="identity_number" id="country"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Phone Number</label>
                                                    <input type="text" class="required" required value="<?=$row['phone']?>" style="width:250px;" name="phone" id="phone"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Fax</label>
                                                    <input type="text" value="<?=$row['fax']?>" style="width:250px;" name="fax" id="fax"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Email Id</label>
                                                    <input type="text" value="<?=$row['email']?>" style="width:250px;" required name="email" id="email" style="width:250px;" readonly="readonly"/>
                                                    <input type="hidden" name="httpreffer" value="<?=$_SERVER['HTTP_REFERER']?>" />
                                                    <input type="hidden" name="cid" value="<?=$row['user_id']?>">
                                                    <input type="hidden" name="act" value="1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            
                                                <div class="span12">
                                                    <div class="row-fluid">
                                                        <div class="form-actions" style="margin-bottom:0px">
                                                        <div class="span3"></div>
                                                        <div class="span9 controls">
                                                          <button type="submit" class="btn right marginR10" name="submitCapacity" id="submitCapacity">Submit&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                                                                

                                    </form>
                                </div>
                            </div>

                        </div><!-- End .span6 -->  
	
                    </div><!-- End .row-fluid -->

                
                
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>


    </body>
</html>
