<?php
include("access.php");

include("includes/top.php");
include("main_includes/conf.class.php");
include("main_includes/admin.class.php");

if(isset($_GET['id'])) {
    $id = intval(base64_decode($_GET['id']))-678;
    // $id = intval($_GET['id']);

    $userId = 0;
    $groupId = 0;
    $title = '';
    $firstName = '';
    $lastName = '';
    $email = '';
    $address = '';
    $countryId = 0;
    $state = '';
    $city = '';
    $phone = '';

    if ($id > 0) {
        $customer = $bsiAdminMain->getCustomer($id);

        $userId = $customer['id'];
        $title = $customer['title'];
        $firstName = $customer['firstName'];
        $lastName = $customer['lastName'];
        $email = $customer['email'];
        $address = $customer['address'];
        $countryId = $customer['country'];
        $state = $customer['state'];
        $city = $customer['city'];
        $phone = $customer['phone'];
        $groupId = $customer['groupId'];
    }
}

?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3><?php echo CUST_MANAGE; ?></h3>


                </div><!-- End .heading-->

                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">

                            <div class="box">

                                <div class="title">

                                    <h4>
                                        <span class="icon16 "></span>
                                        <span><?php echo ($userId > 0) ? CUST_EDIT : CUST_NEW; ?></span>
                                    </h4>

                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                    <form style="margin-bottom:0px !important" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                        <?php if ($userId > 0): ?>
                                        <input type="hidden" value="<?php echo $userId; ?>" name="id" />
                                        <?php endif; ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Discount Group</label>
                                                    <select required name="group_id" style="opacity: 0;">
                                                        <?php
                                                            $groups = $bsiAdminMain->renderGroupOptionsHtml($groupId);
                                                            foreach ($groups as $group) {
                                                                echo $group;
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
										<div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Title</label>
                                                    <?php
                                                        echo $bsiAdminMain->getTitle($title);
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" style="float:left;">Name</label>
                                                    <input class="span2"  name="first_name" type="text" placeholder="<?php echo CUST_FIRST_NAME; ?>" style="float:left;margin-right: 1%;" value="<?php echo $firstName; ?>" required />
                                                    <input class="span2"  name="last_name" type="text" placeholder="<?php echo CUST_LAST_NAME; ?>" style="float:left" value="<?php echo $lastName; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Email</label>
                                                    <input class="span4"  name="email" type="email" placeholder="example@gmail.com" value="<?php echo $email; ?>" required />
                                                </div>
                                            </div>
                                        </div>
                                        <?php if ($id <= 0): ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Password</label>
                                                    <input class="span4"  name="password" type="text" required />
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Phone</label>
                                                    <input class="span4" name="phone" type="text" value="<?php echo $phone; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Address</label>
                                                    <input class="span4"  name="address" type="text" value="<?php echo $address; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Country</label>
                                                    <select required name="country" style="opacity: 0;">
                                                        <?php
                                                            $countries = $bsiAdminMain->renderCountryOptionsHtml($countryId);
                                                            foreach ($countries as $country) {
                                                                echo $country;
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" style="float:left;">State</label>
                                                    <input class="span4" name="state" placeholder="i.e. East Java" type="text" value="<?php echo $state; ?>" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" style="float:left;">City</label>
                                                    <input class="span4" name="city" type="text" placeholder="i.e. Surabaya" value="<?php echo $city; ?>" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <div class="form-actions" style="margin-bottom:0px">
                                                    <div class="span3"></div>
                                                    <div class="span9 controls">
                                                        <button type="submit" class="btn right marginR10" name="saveCustomer" id="sbt_details"><?php echo CUST_SUBMIT; ?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div><!-- End .span6 -->

                    </div><!-- End .row-fluid -->



            </div><!-- End contentwrapper -->
        </div><!-- End #content -->

    </div><!-- End #wrapper -->

    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    </body>
</html>
