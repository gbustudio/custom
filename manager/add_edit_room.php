<?php 
include("access.php");
include 'includes/top.php';
include("main_includes/conf.class.php");
include("main_includes/admin.class.php");
if(isset($_GET['rid']) && $_GET['rid'] != ""){
    $rid = $bsiCore->ClearInput($_GET['rid']);
    $cid = $bsiCore->ClearInput($_GET['cid']);
    if($rid != 0 && $cid != 0){
        $row = mysql_fetch_assoc(mysql_query($bsiAdminMain->getRoomsql($rid, $cid)));
        $rowrt = mysql_fetch_assoc(mysql_query($bsiAdminMain->getRoomtypesql($row['roomtype_id'])));
        $rowca = mysql_fetch_assoc(mysql_query($bsiAdminMain->getCapacitysql($row['capacity_id'])));
        $roomtypeCombo = $rowrt['type_name'].'<input type="hidden" name="roomtype_id" value="'.$row['roomtype_id'].'" />';
        $capacityCombo = $rowca['title'].'<input type="hidden" name="capacity_id" value="'.$row['capacity_id'].'" />';
    }else{
        if(rooms_left()<1){ echo "<script>window.location.href='room_list.php';</script>"; exit(); }
        $row = NULL;
        $roomtypeCombo = $bsiAdminMain->generateRoomtypecombo();
        $capacityCombo = $bsiAdminMain->generateCapacitycombo();
    }
} //else{
//     header("location: room_list.php");
//     exit;
// }
?>
    <div id="wrapper">
<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->
                <div class="heading">
                    <h3><?php echo ROOM_ADD_AND_EDIT; ?></h3>                    
                    
                </div><!-- End .heading-->
                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">
 <?php 
if(isset($_GET['msg_type'])){
?>        
        <div class="alert marginT10 alert-<?php echo $_GET['msg_type'];?>">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong><?php echo strtoupper($_GET['msg_type']);?>!</strong>
            <?php echo $_GET['message'];?>
        </div>
<?php
}
?>
                            <div class="box">
                                <div class="title">
                                    <h4>
                                        <span class="icon16 "></span>
                                        <span><?php echo ROOM_ADD_AND_EDIT; ?></span>
                                    </h4>
                                    
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                        <form style="margin-bottom:0px !important" enctype="multipart/form-data" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo ROOM_TYPE_ADD_EDIT;?></label>
                                                    <?=$roomtypeCombo;?>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo NO_OF_ADULT;?></label>
                                                    <?=$capacityCombo;?>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Number of Rooms</label>
                                                    <input class="span4"  name="no_of_room" type="number" min="1" max="<?=rooms_left()+$row['no_of_rooms']?>" value="<?=$row['no_of_rooms']?>" />
                                                    <input type="hidden" name="hotel" value="<?php echo $_SESSION['hotel_id']; ?>" />
                                                    <input type="hidden" name="page" value="<?=$_GET['rid']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Room Rate</label>
                                                    <input class="span4"  name="room_rate" type="number" min="1" value="<?=$row['room_rate']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Room Description</label>
                                                    <textarea class="span6" rows="5" name="room_desc"><?=$row['room_description']?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Room Basic Image</label>
                                                    <input class="span4" type="file" name="room_pic" />
                                                    <input type="hidden" name="file" value="<?=$row['room_basic_pic']?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Room Amenities List</label>
                                                    <div class="span8">
                                                    <?php $amn = explode("|",$row['room_basic_amenities']); 
                                                    ?>
                                                        <input type="checkbox" <?php if(in_array("WI_FI", $amn)){echo "checked";} ?> value="WI_FI" name="amenity[]" /> WI_FI <br>
                                                        <input type="checkbox" <?php if(in_array("SWIMMING POOL", $amn)){echo "checked";} ?> value="SWIMMING POOL" name="amenity[]" /> SWIMMING POOL <br>
                                                        <input type="checkbox" <?php if(in_array("TELEVISION", $amn)){echo "checked";} ?> value="TELEVISION" name="amenity[]" /> TELEVISION <br>
                                                        <input type="checkbox" <?php if(in_array("COFFEE", $amn)){echo "checked";} ?> value="COFFEE" name="amenity[]" /> COFFEE <br>
                                                        <input type="checkbox" <?php if(in_array("AIR CONDITIONING", $amn)){echo "checked";} ?> value="AIR CONDITIONING" name="amenity[]" /> AIR CONDITIONING <br>
                                                        <input type="checkbox" <?php if(in_array("FITNESS FACILITY", $amn)){echo "checked";} ?> value="FITNESS FACILITY" name="amenity[]" /> FITNESS FACILITY <br>
                                                        <input type="checkbox" <?php if(in_array("FRIDGE", $amn)){echo "checked";} ?> value="FRIDGE" name="amenity[]" /> FRIDGE <br>
                                                        <input type="checkbox" <?php if(in_array("WINE BAR", $amn)){echo "checked";} ?> value="WINE BAR" name="amenity[]" /> WINE BAR <br>
                                                        <input type="checkbox" <?php if(in_array("SMOKING ALLOWED", $amn)){echo "checked";} ?> value="SMOKING ALLOWED" name="amenity[]" /> SMOKING ALLOWED <br>
                                                        <input type="checkbox" <?php if(in_array("ENTERTAINMENT", $amn)){echo "checked";} ?> value="ENTERTAINMENT" name="amenity[]" /> ENTERTAINMENT <br>
                                                        <input type="checkbox" <?php if(in_array("SECURE VAULT", $amn)){echo "checked";} ?> value="SECURE VAULT" name="amenity[]" /> SECURE VAULT <br>
                                                        <input type="checkbox" <?php if(in_array("PICK AND DROP", $amn)){echo "checked";} ?> value="PICK AND DROP" name="amenity[]" /> PICK AND DROP <br>
                                                        <input type="checkbox" <?php if(in_array("ROOM SERVICE", $amn)){echo "checked";} ?> value="ROOM SERVICE" name="amenity[]" /> ROOM SERVICE <br>
                                                        <input type="checkbox" <?php if(in_array("PETS ALLOWED", $amn)){echo "checked";} ?> value="PETS ALLOWED" name="amenity[]" /> PETS ALLOWED <br>
                                                        <input type="checkbox" <?php if(in_array("PLAY PLACE", $amn)){echo "checked";} ?> value="PLAY PLACE" name="amenity[]" /> PLAY PLACE <br>
                                                        <input type="checkbox" <?php if(in_array("COMPLIMENTARY BREAKFAST", $amn)){echo "checked";} ?> value="COMPLIMENTARY BREAKFAST" name="amenity[]" /> COMPLIMENTARY BREAKFAST <br>
                                                        <input type="checkbox" <?php if(in_array("FREE PARKING", $amn)){echo "checked";} ?> value="FREE PARKING" name="amenity[]" /> FREE PARKING <br>
                                                        <input type="checkbox" <?php if(in_array("CONFERENCE ROOM", $amn)){echo "checked";} ?> value="CONFERENCE ROOM" name="amenity[]" /> CONFERENCE ROOM <br>
                                                        <input type="checkbox" <?php if(in_array("FIRE PLACE", $amn)){echo "checked";} ?> value="FIRE PLACE" name="amenity[]" /> FIRE PLACE <br>
                                                        <input type="checkbox" <?php if(in_array("HANDICAP ACCESSIBLE", $amn)){echo "checked";} ?> value="HANDICAP ACCESSIBLE" name="amenity[]" /> HANDICAP ACCESSIBLE <br>
                                                        <input type="checkbox" <?php if(in_array("DOORMAN", $amn)){echo "checked";} ?> value="DOORMAN" name="amenity[]" /> DOORMAN <br>
                                                        <input type="checkbox" <?php if(in_array("HOT TUB", $amn)){echo "checked";} ?> value="HOT TUB" name="amenity[]" /> HOT TUB <br>
                                                        <input type="checkbox" <?php if(in_array("ELEVATOR IN BUILDING", $amn)){echo "checked";} ?> value="ELEVATOR IN BUILDING" name="amenity[]" /> ELEVATOR IN BUILDING <br>
                                                        <input type="checkbox" <?php if(in_array("SUITABLE FOR EVENTS", $amn)){echo "checked";} ?> value="SUITABLE FOR EVENTS" name="amenity[]" /> SUITABLE FOR EVENTS <br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Room Guests Capacity
                                                    <span class="help-block">(<?php echo LEAVE_BLANK_IF_NONE_TEXT; ?>)</span></label>
                                                    <input class="span3" name="room_guests" type="number" min="1" value="<?php echo $row['room_guests_capacity']; ?>" />
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hot ? </label>
                                                    <input <?php if($row['hot']==1){echo "checked";} ?> name="hot" type="checkbox" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <div class="form-actions" style="margin-bottom:0px">
                                                    <div class="span3"></div>
                                                    <div class="span9 controls">
                                                      <button type="submit" class="btn right marginR10" name="submitRoom" id="sbt_details"><?php echo ADD_EDIT_SUBMIT; ?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- End .span6 -->  
    
                    </div><!-- End .row-fluid -->
                
                
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>
    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>
    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->
    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    </body>
</html>