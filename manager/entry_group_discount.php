<?php
include("access.php");

include("includes/top.php");
include("main_includes/conf.class.php");
include("main_includes/admin.class.php");

if(isset($_GET['id'])) {
    $id = intval(base64_decode($_GET['id']))-678;
    // $id = intval($_GET['id']);

    $groupId = 0;
    $hotelId = 0;
    $roomId = 0;
    $name = '';
    $discount = 5;
    if ($id > 0) {
        $groupDiscount = $bsiAdminMain->getGroupDiscount($id);
        $groupId = $groupDiscount['groupId'];
        $hotelId = $groupDiscount['hotelId'];
        $roomId = $groupDiscount['roomId'];
        $name = $groupDiscount['name'];
        $discount = $groupDiscount['discount'];
    }
} else {
    $hotelId = $_SESSION['hotel_id'];
}

?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3><?php echo ($groupId > 0) ? GROUP_EDIT : GROUP_NEW; ?></h3>


                </div><!-- End .heading-->

                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">

                            <div class="box">

                                <div class="title">

                                    <h4>
                                        <span class="icon16 "></span>
                                        <span><?php echo ($groupId > 0) ? GROUP_EDIT : GROUP_NEW; ?></span>
                                    </h4>

                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                    <form style="margin-bottom:0px !important" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                        <?php if ($groupId > 0): ?>
                                        <input type="hidden" value="<?php echo $groupId; ?>" name="id" />
                                        <?php endif; ?>
                                        <input type="hidden" value="<?php echo $hotelId ?>" name="hotel_id" />
										<div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Name</label>
                                                    <input class="span4"  name="name" type="text" value="<?php echo $name; ?>" required/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Select Room</label>
                                                    <select required name="room_id" style="opacity: 0;">
                                                        <?php
                                                            $rooms = $bsiAdminMain->renderGroupRoomTypeOptions($roomId);
                                                            foreach ($rooms as $room) {
                                                                echo $room;
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3">Discount (%)</label>
                                                    <input class="span1 tipB" title="i.e 15" required name="discount" min="1" max="100" type="number" value="<?php echo $discount; ?>" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row row-fluid">
											<div class="span12">
												<div class="row-fluid">
													<div class="form-actions" style="margin-bottom:0px">
													<div class="span3"></div>
													<div class="span9 controls">
													   <button type="submit" class="btn right marginR10" name="entryGroupDiscount" id="sbt_details"><?php echo GROUP_SUBMIT; ?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
													</div>
													</div>
												</div>
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div><!-- End .span6 -->

                    </div><!-- End .row-fluid -->



            </div><!-- End contentwrapper -->
        </div><!-- End #content -->

    </div><!-- End #wrapper -->

    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    </body>
</html>
