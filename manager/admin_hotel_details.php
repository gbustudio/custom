<?php 
include ("access.php");
// if(isset($_POST['sbt_details'])){
// 	include("main_includes/db.conn.php");
// 	include("main_includes/admin.class.php");
// 	include("main_includes/conf.class.php");
// 	$bsiAdminMain->hotel_details_post();
// 	header("location:admin_hotel_details.php");
// }
include 'includes/top.php';
include("main_includes/conf.class.php");

$qh = mysql_query("select * from bsi_hotels where hotel_id=".$_SESSION['hotel_id']." and status=1");
$hotelr = mysql_fetch_array($qh);

?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3><?php echo HOTEL_DETAILS; ?></h3>                    

                    
                </div><!-- End .heading-->

                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">

 <?php 
if(isset($_GET['msg_type'])){
?>        
        <div class="alert marginT10 alert-<?php echo $_GET['msg_type'];?>">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong><?php echo strtoupper($_GET['msg_type']);?>!</strong>
            <?php echo $_GET['message'];?>
        </div>
<?php
}
?>
                            <div class="box">

                                <div class="title">

                                    <h4>
                                        <span class="icon16 "></span>
                                        <span><?php echo HOTEL_DETAILS; ?></span>
                                    </h4>
                                    
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                        <form style="margin-bottom:0px !important" enctype="multipart/form-data" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo HOTEL_NAME;?></label>
                                                    <input class="span4"  name="hotel_name" type="text" value="<?php echo $hotelr['hotel_name']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo STREET_ADDRESS;?></label>
                                                    <input class="span4"  name="str_addr" type="text" value="<?php echo $hotelr['hotel_address']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo CITY;?></label>
                                                    <input class="span4"  name="city" type="text" value="<?php echo $hotelr['hotel_info_city']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo COUNTRY; ?></label>
                                                    <select  name="country">
                                                    <?php 
                                                    $cq = mysql_query("select * from countries where status=1");
                                                    while($cr = mysql_fetch_array($cq)){
                                                        if($cr['country_id']==$hotelr['hotel_info_country_id']){$se=" selected='selected'";}else{$se="";}
                                                        echo '<option value="'.$cr['country_id'].'" '.$se.'>'.$cr['country_name'].'</option>';
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Latitude Longitude</label>
                                                    <input class="span4"  name="hotel_latlong" type="text" value="<?php echo $hotelr['hotel_latitude_longitude']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo PHONE_NUMBER; ?></label>
                                                    <input class="span4"  name="phone" type="text" value="<?php echo $hotelr['phone']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?php echo EMAIL_ID; ?></label>
                                                    <input class="span4"  name="email" type="text" value="<?php echo $hotelr['email']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Extra People</label>
                                                    <input class="span4"  name="extra_people" type="text" value="<?php echo $hotelr['hotel_info_extra_people']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Minimum Stay</label>
                                                    <input class="span4"  name="min_stay" type="text" value="<?php echo $hotelr['hotel_info_min_stay']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Security Deposit</label>
                                                    <input class="span4"  name="security_deposit" type="text" value="<?php echo $hotelr['hotel_info_security_deposit']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Cancellation</label>
                                                    <input class="span4"  name="cancellation" type="text" value="<?php echo $hotelr['hotel_info_cancellation']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Type</label>
                                                    <select  name="hotel_type">
                                                    <?php 
                                                    for($t=1; $t<6; $t++){
                                                    $cq = array("1 ");
                                                        if($t==str_replace(" Star","",$hotelr['hotel_info_type'])){$se=" selected='selected'";}else{$se="";}
                                                        echo '<option value="'.$t.' Star" '.$se.'>'.$t.' Star</option>';
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Short Description</label>
                                                    <textarea class="span6" name="short_desc" rows="4" cols="70" ><?php echo $hotelr['hotel_short_description']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Full Description</label>
                                                    <textarea class="span6" name="full_desc" rows="4" cols="70" ><?php echo $hotelr['hotel_full_description']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Terms</label>
                                                    <textarea class="span6" name="hotel_terms" rows="4" cols="70" ><?php echo $hotelr['hotel_terms']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Amenities List</label>
                                                    <div class="span8">
                                                    <?php $amn = explode("|",$hotelr['hotel_amenities']); 
                                                    ?>
                                                        <input type="checkbox" <?php if(in_array("WI_FI", $amn)){echo "checked";} ?> value="WI_FI" name="amenity[]" /> WI_FI <br>
                                                        <input type="checkbox" <?php if(in_array("SWIMMING POOL", $amn)){echo "checked";} ?> value="SWIMMING POOL" name="amenity[]" /> SWIMMING POOL <br>
                                                        <input type="checkbox" <?php if(in_array("TELEVISION", $amn)){echo "checked";} ?> value="TELEVISION" name="amenity[]" /> TELEVISION <br>
                                                        <input type="checkbox" <?php if(in_array("COFFEE", $amn)){echo "checked";} ?> value="COFFEE" name="amenity[]" /> COFFEE <br>
                                                        <input type="checkbox" <?php if(in_array("AIR CONDITIONING", $amn)){echo "checked";} ?> value="AIR CONDITIONING" name="amenity[]" /> AIR CONDITIONING <br>
                                                        <input type="checkbox" <?php if(in_array("FITNESS FACILITY", $amn)){echo "checked";} ?> value="FITNESS FACILITY" name="amenity[]" /> FITNESS FACILITY <br>
                                                        <input type="checkbox" <?php if(in_array("FRIDGE", $amn)){echo "checked";} ?> value="FRIDGE" name="amenity[]" /> FRIDGE <br>
                                                        <input type="checkbox" <?php if(in_array("WINE BAR", $amn)){echo "checked";} ?> value="WINE BAR" name="amenity[]" /> WINE BAR <br>
                                                        <input type="checkbox" <?php if(in_array("SMOKING ALLOWED", $amn)){echo "checked";} ?> value="SMOKING ALLOWED" name="amenity[]" /> SMOKING ALLOWED <br>
                                                        <input type="checkbox" <?php if(in_array("ENTERTAINMENT", $amn)){echo "checked";} ?> value="ENTERTAINMENT" name="amenity[]" /> ENTERTAINMENT <br>
                                                        <input type="checkbox" <?php if(in_array("SECURE VAULT", $amn)){echo "checked";} ?> value="SECURE VAULT" name="amenity[]" /> SECURE VAULT <br>
                                                        <input type="checkbox" <?php if(in_array("PICK AND DROP", $amn)){echo "checked";} ?> value="PICK AND DROP" name="amenity[]" /> PICK AND DROP <br>
                                                        <input type="checkbox" <?php if(in_array("ROOM SERVICE", $amn)){echo "checked";} ?> value="ROOM SERVICE" name="amenity[]" /> ROOM SERVICE <br>
                                                        <input type="checkbox" <?php if(in_array("PETS ALLOWED", $amn)){echo "checked";} ?> value="PETS ALLOWED" name="amenity[]" /> PETS ALLOWED <br>
                                                        <input type="checkbox" <?php if(in_array("PLAY PLACE", $amn)){echo "checked";} ?> value="PLAY PLACE" name="amenity[]" /> PLAY PLACE <br>
                                                        <input type="checkbox" <?php if(in_array("COMPLIMENTARY BREAKFAST", $amn)){echo "checked";} ?> value="COMPLIMENTARY BREAKFAST" name="amenity[]" /> COMPLIMENTARY BREAKFAST <br>
                                                        <input type="checkbox" <?php if(in_array("FREE PARKING", $amn)){echo "checked";} ?> value="FREE PARKING" name="amenity[]" /> FREE PARKING <br>
                                                        <input type="checkbox" <?php if(in_array("CONFERENCE ROOM", $amn)){echo "checked";} ?> value="CONFERENCE ROOM" name="amenity[]" /> CONFERENCE ROOM <br>
                                                        <input type="checkbox" <?php if(in_array("FIRE PLACE", $amn)){echo "checked";} ?> value="FIRE PLACE" name="amenity[]" /> FIRE PLACE <br>
                                                        <input type="checkbox" <?php if(in_array("HANDICAP ACCESSIBLE", $amn)){echo "checked";} ?> value="HANDICAP ACCESSIBLE" name="amenity[]" /> HANDICAP ACCESSIBLE <br>
                                                        <input type="checkbox" <?php if(in_array("DOORMAN", $amn)){echo "checked";} ?> value="DOORMAN" name="amenity[]" /> DOORMAN <br>
                                                        <input type="checkbox" <?php if(in_array("HOT TUB", $amn)){echo "checked";} ?> value="HOT TUB" name="amenity[]" /> HOT TUB <br>
                                                        <input type="checkbox" <?php if(in_array("ELEVATOR IN BUILDING", $amn)){echo "checked";} ?> value="ELEVATOR IN BUILDING" name="amenity[]" /> ELEVATOR IN BUILDING <br>
                                                        <input type="checkbox" <?php if(in_array("SUITABLE FOR EVENTS", $amn)){echo "checked";} ?> value="SUITABLE FOR EVENTS" name="amenity[]" /> SUITABLE FOR EVENTS <br>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Amenities Description</label>
                                                    <textarea class="span6" name="amenities_desc" rows="4" cols="70" ><?php echo $hotelr['hotel_amenities_description']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Help Box Text</label>
                                                    <input class="span6" name="help_text" type="text" value="<?php echo $hotelr['help_box_text']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Why Book with Us Text</label>
                                                    <textarea class="span6" name="why_book_us" rows="4" cols="70" ><?php echo $hotelr['why_book_with_us']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal">Hotel Logo</label>
                                                    <input class="span6" name="logo" type="file" />
                                                    <input type="hidden" name="file" value="<?php echo $hotelr['hotel_logo']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row row-fluid">
                                                <div class="span12">
                                                    <div class="row-fluid">
                                                        <div class="form-actions" style="margin-bottom:0px">
                                                        <div class="span3"></div>
                                                        <div class="span9 controls">
                                                          <button type="submit" class="btn right marginR10" name="sbt_hotel_details" id="sbt_details"><?php echo SUBMIT;?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                                                                

                                    </form>
                                </div>
                            </div>

                        </div><!-- End .span6 -->  
	
                    </div><!-- End .row-fluid -->

                
                
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>


    </body>
</html>
