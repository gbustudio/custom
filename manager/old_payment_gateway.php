<?php 
include ("access.php");
include 'includes/top.php';
include("main_includes/conf.class.php");
//$hotel = mysql_fetch_array(mysql_query("select theme_color,hotel_cover_image from bsi_hotels where hotel_id=".$_SESSION['hotel_id']));
?>
    <div id="wrapper">
<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->
                <div class="heading">
                    <h3>Payment Gateways</h3>                    
                    
                </div><!-- End .heading-->
                <!-- Build page from here: -->
                <div class="row-fluid">
                       <div class="span12">
<?php 
if(isset($_SESSION['val1'])){
?>        
    <div class="row-fluid">
            <span class="span12">
        <div class="alert marginT10">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong>MESSAGE!</strong>
            <?php echo $_SESSION['val1'];
                  unset($_SESSION['val1'])?>
        </div>
      </span>
    </div>
<?php
}
?>
                            <div class="box">
                                <div class="title">
                                    <h4>
                                        <span class="icon16 "></span>
                                        <span>Payment Gateways</span>
                                    </h4>
                                    
                                </div>
                                <div class="content" style="padding: 0px !important; padding-top: 10px !important">
                                        <form style="margin-bottom:0px !important" enctype="multipart/form-data" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                <?php
                                    $q = mysql_query("select * from bsi_payment_gateway where hotel_id=".$_SESSION['hotel_id']);
                                    while($r=mysql_fetch_array($q)){
                                        if($r['gateway_code']=="pp"){
                                ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?=$r['gateway_name']?></label>
                                                    <input class="styled tipB" name="paypal_on" title="Enable/Disable" <?=($r['enabled']==1)? 'checked="checked"' : ''?> type="checkbox" value="1" />
                                                    <input class="span6 tipB" style="display:inline;" name="paypal_id" value="<?=$r['account']?>" type="text" title="paypal email id" />
                                                </div>
                                            </div>
                                        </div>
                                    <?php } elseif($r['gateway_code']=="co"){ $exc = ($r['account']=="")? NULL : explode("||", $r['account']); ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?=$r['gateway_name']?></label>
                                                    <input class="styled tipB" name="method_on" title="Enable/Disable" <?=($r['enabled']==1)? 'checked="checked"' : ''?> type="radio" value="co" />
                                                    <input class="span3 tipB" style="display:inline;" value="<?=$exc[0]?>" name="checkout_id" type="text" title="2checkout user id" />
                                                    <input class="span3 tipB" style="display:inline;" value="<?=$exc[1]?>" name="checkout_word" type="text" title="2checkout secret word" />
                                                </div>
                                            </div>
                                        </div>
                                    <?php } elseif($r['gateway_code']=="manual"){ ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?=$r['gateway_name']?></label>
                                                    <input class="styled tipB" name="manual_on" readonly title="Enable/Disable" <?=($r['enabled']==1)? 'checked="checked"' : ''?> type="checkbox" value="1" />
                                                </div>
                                            </div>
                                        </div>
                                    <?php } elseif($r['gateway_code']=="cc"){ ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?=$r['gateway_name']?></label>
                                                    <input class="styled tipB" name="method_on" title="Enable/Disable" <?=($r['enabled']==1)? 'checked="checked"' : ''?> type="radio" value="cc" />
                                                </div>
                                            </div>
                                        </div>
                                    <?php } elseif($r['gateway_code']=="stripe"){ $exS = ($r['account']=="")? NULL : explode("||", $r['account']); ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?=$r['gateway_name']?></label>
                                                    <input class="styled tipB" name="method_on" title="Enable/Disable" <?=($r['enabled']==1)? 'checked="checked"' : ''?> type="radio" value="stripe" />
                                                    <input class="span2 tipB" style="display:inline;" value="<?=$exS[0]?>" name="stripe_secret_key" type="text" title="stripe secret key" />
                                                    <input class="span2 tipB" style="display:inline;" value="<?=$exS[1]?>" name="stripe_publishable_key" type="text" title="stripe publishable key" />
                                                </div>
                                            </div>
                                        </div>
                                    <?php } elseif($r['gateway_code']=="auth"){ $exA = ($r['account']=="")? NULL : explode("||", $r['account']); ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <label class="form-label span3" for="normal"><?=$r['gateway_name']?></label>
                                                    <input class="styled tipB" name="method_on" title="Enable/Disable" <?=($r['enabled']==1)? 'checked="checked"' : ''?> type="radio" value="auth" />
                                                    <input class="span2 tipB" style="display:inline;" value="<?=$exA[0]?>" name="authorize_login_id" type="text" title="Authorize.Net API Login ID" />
                                                    <input class="span2 tipB" style="display:inline;" value="<?=$exA[1]?>" name="authorize_md5_hash" type="text" title="Authorize.Net MD5 Hash" />
                                                    <input class="span2 tipB" style="display:inline;" value="<?=$exA[2]?>" name="authorize_trans_key" type="text" title="Authorize.Net Transaction Key" />
                                                </div>
                                            </div>
                                        </div>
                                        <?php } } ?>
                                        <div class="form-row row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <div class="form-actions" style="margin-bottom:0px">
                                                    <div class="span3"></div>
                                                    <div class="span9 controls">
                                                      <button type="submit" class="btn right marginR10" name="savePaymentGateways" id="sbt_details"><?php echo SUBMIT;?>&nbsp;<span class="icon12 brocco-icon-forward"></span></button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- End .span6 -->  
                    </div><!-- End .row-fluid -->
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>
    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>
    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>
    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->
    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript">
    $(function(){
    })
    </script>
    </body>
</html>
