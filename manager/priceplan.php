<?php
include("access.php");


include 'includes/top.php';
include("main_includes/admin.class.php");
?>
    <div id="wrapper">

<?php
include 'includes/left.php';
?>
        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3>Prices Plan</h3>                    

                   

                </div><!-- End .heading-->

                <!-- Build page from here: -->
 <?php 
if(isset($_GET['msg_type'])){
?>        
        <div class="alert marginT10 alert-<?php echo $_GET['msg_type'];?>">
            <button class="close" data-dismiss="alert" type="button">&times;</button>
            <strong><?php echo strtoupper($_GET['msg_type']);?>!</strong>
            <?php echo $_GET['message'];?>
        </div>
<?php
}
?>
								<div class="row-fluid">

                        <div class="span12">

                            <div class="box gradient">

                                <div class="title">
                                    <h4>
                                        <span>Prices Plan</span>
                                        <?php 
                                            // $eq = mysql_query("select room_id from bsi_room where status=1 and hotel_id=".$_SESSION['hotel_id']);
                                            // $pq = mysql_query("select room_id from bsi_priceplan where status=1 and hotel_id=".$_SESSION['hotel_id']);
                                            // $roomsq = array(); $roomsp = array();
                                            // while($er = mysql_fetch_array($eq)){ $roomsq[] = $er['room_id']; }
                                            // while($pr = mysql_fetch_array($pq)){ $roomsp[] = $pr['room_id']; }
                                            // $c = array_diff($roomsq, $roomsp); //var_dump($roomsq);var_dump($roomsp);var_dump($c);
                                            // if(count($c)>0){
                                        ?>
                                        <form class="box-form right">
                                          <button class="btn btn-info btn-mini" onClick="window.location.href='editpriceplan.php?edit=0'" type="button">Add New Price Plan</button>
                                        </form>
                                        <?php //} ?>
                                    </h4>
                                </div>
                                <div class="content noPad clearfix">
                                    <table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table" width="100%">
                                        <thead>
                                          <tr>
                                            <th>Room</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Sunday</th>
                                            <th>Monday</th>
                                            <th>Tuesday</th>
                                            <th>Wednesday</th>
                                            <th>Thursday</th>
                                            <th>Friday</th>
                                            <th>Saturday</th>
                                            <th>&nbsp;</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            $q = mysql_query("select * from bsi_priceplan where status=1 and hotel_id=".$_SESSION['hotel_id']." order by plan_id desc");
                                            while($r = mysql_fetch_array($q)){
                                                $room = mysql_fetch_array(mysql_query("select roomtype_id,capacity_id,room_rate from bsi_room where room_id=".$r['room_id']));
                                                $cap = mysql_fetch_array(mysql_query("select title from bsi_capacity where id=".$room['capacity_id']));
                                                $type = mysql_fetch_array(mysql_query("select type_name from bsi_roomtype where roomtype_id=".$room['roomtype_id']));
                                                if($r['start_date']!="0000-00-00"){$start=date("d M Y", strtotime($r['start_date']));}else{$start="-";}
                                                if($r['end_date']!="0000-00-00"){$end=date("d M Y", strtotime($r['end_date']));}else{$end="-";}
                                                echo "<tr><td>".$cap['title']." ".$type['type_name']." Room (".$room['room_rate'].")</td><td>".$start."</td><td>".$end."</td><td>".$r['sun']."</td><td>".$r['mon']."</td><td>".$r['tue']."</td><td>".$r['wed']."</td><td>".$r['thu']."</td><td>".$r['fri']."</td><td>".$r['sat']."</td><td><a href='editpriceplan.php?edit=".$r['plan_id']."'>Edit</a> | <a href='editpriceplan.php?delplan=".$r['plan_id']."'>Disable</a></td></tr>";
                                            }
                                        ?>
                                        </tbody>
                                    </table>

                                </div><br>
                                <ul class="unstyled">
                                    <li><b>Prices Priority:</b></li>
                                    <li>Date-wise Plan {else} With-out Date Plan {else} Normal Room Rate</li>
                                </ul>
                            </div><!-- End .box -->

                        </div><!-- End .span12 -->

                    </div><!-- End .row-fluid -->
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- THIS IS DOWNLOADED FROM WWW.SXRIPTGATES.COM - SO THIS IS YOUR NEW SITE FOR DOWNLOAD SCRIPT ;) -->
    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>

    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="js/statistic.js"></script><!-- Control graphs ( chart, pies and etc) -->

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="plugins/touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>


    </body>
</html>
