<?php require_once("includes/initialize.php");
$datetime = '';
$room_id = '1';
$room = get_room($room_id);
$hotel = hotel_details($room->hotel_id);
if(!validateDate($datetime) or strtotime($datetime)<time()){ $datetime=date("m/d/Y"); }

?>
<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Booking Venture | Book your Trip today </title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Booking Venture | Book your Trip today ">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">
    
    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />
    
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css">
    
    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">
    
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper">
       <section id="content" style="padding-top: 0px !important;">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-md-12">
                        
                        <div id="hotel-features" style="margin-top: -0px;" class="tab-container">
                            <div class="tab-content">
                                
                                <form action="http://localhost/custom/hotel/search/1-your_hotel_and_resorts/" method="post" id="reservation_form_top">
                                <div class="tab-pane active" id="hotel-availability" style="padding: 0px !important;">
                                    <div class="update-search clearfix">
                                        <div class="col-md-5">
                                            <h4 class="title"><?php echo $_LANGUAGE['when']; ?></h4>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label><?php echo $_LANGUAGE['check_in']; ?></label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="mm/dd/yy" class="input-text full-width" id="check_in_date" name="check_in_date" onChange="do_action()" value="<?php echo $datetime; ?>" min="<?php echo date('m/d/Y');?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <label><?php echo $_LANGUAGE['check_out']; ?></label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="mm/dd/yy" class="input-text full-width" id="check_out_date" name="check_out_date" value="<?php $next=strtotime($datetime)+86400; echo date("m/d/Y", $next); ?>" onChange="do_action()" min="<?php echo date('m/d/Y');?>" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <h4 class="title"><?php echo $_LANGUAGE['who']; ?></h4>
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <label><?php echo $_LANGUAGE['rooms']; ?></label>
                                                    <div class="selector">
                                                        <select class="full-width" id="num_rooms" onChange="do_action()" name="num_rooms">
                                                            <option value="1" selected="selected">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                            <option value="5">05</option>
                                                            <option value="6">06</option>
                                                            <option value="7">07</option>
                                                            <option value="8">08</option>
                                                            <option value="9">09</option>
                                                            <option value="10">10</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label><?php echo $_LANGUAGE['adults']; ?></label>
                                                    <div class="selector">
                                                        <select class="full-width" name="dewasa">
                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label><?php echo $_LANGUAGE['kids']; ?></label>
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <h4 class="visible-md visible-lg">&nbsp;</h4>
                                            <label class="visible-md visible-lg">&nbsp;</label>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <button data-animation-duration="1" data-animation-type="bounce" class="full-width icon-check animated" type="submit"><?php echo $_LANGUAGE['search_now']; ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </section>
       
        
    </div>
   <?php require("footer.php"); ?>
   
<script type="text/javascript">
      
      function embed_availablity_celander(datetime,room_id, to){
        jQuery.post("ajax-celander.php", {on: datetime, room: room_id, upto: to}, function(data){
          jQuery('#availablity-celander').html(data);});
      }
    function do_action(){
      var num_rooms = tjq('#num_rooms').val();
      var check_in_date = tjq('#check_in_date').val();
      var check_out_date = tjq('#check_out_date').val();
      if(check_in_date!=""){
        tjq('#check_out_date').attr('min', check_in_date);
        if(Date.parse(check_in_date)>=Date.parse(check_out_date)){
          var t = new Date(new Date(check_in_date).getTime() + 86400000);
          var ne = ("0" + (t.getMonth()+1)).slice(-2)+"/"+("0" + t.getDate()).slice(-2)+"/"+t.getFullYear();
          tjq('#check_out_date').val(ne);
          check_out_date = ne;
        }
      }
      if(check_out_date && check_in_date){
        tjq.post("jquery_imbed_rooms.php", {room: <?php echo $room_id;?>,num_rooms : num_rooms,check_in_date : check_in_date,check_out_date : check_out_date}, function(data){
          tjq('#imbed_rooms').html(data);});
		   jQuery.post("ajax-celander.php", {on: check_in_date, room: '<?php echo $room_id;?>', upto: check_out_date}, function(data){
          jQuery('#availablity-celander').html(data);});
      }
    }
    tjq(function(){
      embed_availablity_celander('<?php echo $datetime;?>','<?php echo $room_id;?>','<?php echo date("m/d/Y",strtotime($datetime)+86400);?>');
      tjq.post("jquery_imbed_rooms.php", {room: <?php echo $room_id;?>,num_rooms:1,check_in_date:'<?php echo $datetime;?>',check_out_date:'<?php $next=strtotime($datetime)+86400; echo date("m/d/Y", $next); ?>'}, function(data){
          tjq('#imbed_rooms').html(data);});
      
      function amount(text){
        return parseInt(text.replace("<?php echo user_currency($_COOKIE['currency']); ?>",""));
      }
      // working for extras calculation
      tjq(document).on("change", ".extras_checkbox", function(){
        if(this.checked){
          var price = amount(tjq(this).data("price"));
          var id = tjq(this).attr("id");
          tjq("#"+id+"_quantity").val(1);
          tjq(this).parents("tr").children(".extra_amount").text('<?php echo user_currency($_COOKIE['currency']); ?>'+price);
        }else{
          var price = amount(tjq(this).data("price"));
          var id = tjq(this).attr("id");
          tjq("#"+id+"_quantity").val("");
          var already = amount(tjq(this).parents("tr").children(".extra_amount").text());
          tjq(this).parents("tr").children(".extra_amount").text('<?php echo user_currency($_COOKIE['currency']); ?>0');
        }
        updateCalc();
      })
      tjq(document).on("change", ".qty", function(){
        var qty = parseInt(tjq(this).val());
        var id = tjq(this).attr("id").replace("_quantity","");
        var price = amount(tjq("#"+id).data("price"));
        if(qty==0 || qty=="0"){ 
          tjq("#"+id).attr('checked', false);
          var already = amount(tjq(this).parents("tr").children(".extra_amount").text());
          tjq(this).parents("tr").children(".extra_amount").text('<?php echo user_currency($_COOKIE['currency']); ?>0');
        } else {
          tjq("#"+id).attr('checked', true);
          var already = amount(tjq(this).parents("tr").children(".extra_amount").text());
          var total = qty*price;
          tjq(this).parents("tr").children(".extra_amount").text('<?php echo user_currency($_COOKIE['currency']); ?>'+total);
        }
        updateCalc();
      })
      
      tjq(document).on("change", ".checkmembers", function(){
        var room = tjq(this).parents("ol").data("room");
        var already = amount(tjq(".extra_child_"+room+"_0").text());
        var adults = parseInt(tjq("#number_adults_"+room).val());
        var children = parseInt(tjq("#number_children_"+room).val());
        var allowed = tjq(this).parents("ol").data("allowed");
        var diff = (children+adults)-allowed;
        var price = diff*amount(tjq(this).parents("ol").data("extra-charge")); //2*50
        var pric_diff = price-already;
        var days = tjq(this).parents("ol").data("days");
        var grand_amount = price*days;
        if(diff>0){
          var c;
          for(c=0; c<days; c++){
            tjq(".extra_child_"+room+"_"+c).text('<?php echo user_currency($_COOKIE['currency']); ?>'+price);
            var alreadysub = amount(tjq(".rrt_date_total_"+room+"_"+c).text());
            tjq(".rrt_date_total_"+room+"_"+c).text('<?php echo user_currency($_COOKIE['currency']); ?>'+(alreadysub+pric_diff));
          }
        } else {
          for(c=0; c<days; c++){
            tjq(".extra_child_"+room+"_"+c).text('<?php echo user_currency($_COOKIE['currency']); ?>0');
            //var alreadysub = amount(tjq(".rrt_date_total_"+room+"_"+c).text());
            tjq(".rrt_date_total_"+room+"_"+c).text(tjq(".rrt_rate_date_"+room+"_"+c).text());
          }
        }
        updateCalc();
      })
      
      function updateCalc(){
        var total=0;
        var extras=0;
        var rooms=0;
        tjq("td[class^='extra_']").each(function(){
          extras += amount(tjq(this).text());
        })
        tjq("td[class^='rrt_rate_date']").each(function(){
          rooms += amount(tjq(this).text());
        })
        tjq("td.extra_total").text('<?php echo user_currency($_COOKIE['currency']); ?>'+extras);
        tjq("td.room_total").text('<?php echo user_currency($_COOKIE['currency']); ?>'+rooms);
        tjq("td#total_payment").text('<?php echo user_currency($_COOKIE['currency']); ?>'+(extras+rooms));
        tjq("#reservation_total").val(amount(tjq("#total_payment").text()));
      }
      // tjq(".card-information").hide();
      // tjq(".paypal-information").hide();
      // tjq(document).on("change", "#payment_type_co,#payment_type_auth,#payment_type_stripe,#payment_type_cc", function(e){
      //   tjq("select[name^='card_'], input[name^='card_']").attr("required", "required");
      //   tjq(".card-information").show('slow');
      //   tjq(".paypal-information").hide();
      // })
      // tjq(document).on("change", "#payment_type_pp,#payment_type_manual", function(e){
      //   tjq("select[name^='card_'], input[name^='card_']").removeAttr("required");
      //   tjq(".card-information").hide('slow');
      //   tjq(".paypal-information").hide();
      //   if(tjq(this).attr("id")=="payment_type_pp"){ 
      //     tjq(".paypal-information").show('slow');
      //     tjq(".card-information").hide();
      //     tjq("input[name=payment_method_detail]").attr("required", "required");
      //   }
      // })
      tjq(document).on("submit", "#reservation_form_required", function(e){
        e.preventDefault();
        tjq("#btn_confirm_booking").prop('disabled', true);
        var first = tjq("#reservation_form_top").serializeArray();
        var second = tjq("#reservation_form_required").serializeArray();
        var first = tjq.merge(first, second);
        first[first.length] = {name:"reserveIt",value:"1"};
        tjq.ajax({
          type:'POST',
          data: first,
          url:'includes/ajax_func.php',
          success:function(data) { 
            //alert(data);
            data = data.split("||");
            if(data[0]==1){
              if(tjq("input[name=payment_type]:checked").val()=="pp"){ // testpp999@gmail.com
                var usd = data[1]; //https://www.sandbox.paypal.com/cgi-bin/webscr https://www.paypal.com/cgi-bin/webscr
                var pp = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick&business="+data[2]+"&lc=US&item_name=Booking%20ID:%20"+data[3]+"&amount="+usd+"%2e00&currency_code=USD&button_subtype=services&cancel_return=http://localhost/custom/hotel/hotel_page/new/hotel-detailed.php&return=http://localhost/custom/hotel/hotel_page/new/thank-you.php?booking="+data[3]+"&notify_url=http://localhost/custom/hotel/hotel_page/new/pp_process.php&no_note=0&bn=PP%2dBuyNowBF%3abtn_buynowCC_LG%2egif%3aNonHostedGuest";
                  window.location.href=pp;
              }
              else if(tjq("input[name=payment_type]:checked").val()=="co"){
                var co = "https://www.2checkout.com/checkout/purchase?sid="+data[2]+"&mode=2CO&li_0_type=product&li_0_price="+data[1]+"%2e00&li_0_quantity=1&li_0_name=Booking%20ID:%20"+data[3]+"&demo=Y&"+data[4]; //demo=Y
                  window.location.href=co;
              }
              else if(tjq("input[name=payment_type]:checked").val()=="manual"){
                window.location.href="thank-you.php?booking="+data[1];
              }
              //alert("Your reservation has been received, we will review and contact you soon.");
            }
            else{alert("There is some problem in reservation. Please try again later!");window.location.href="hotel-detailed.php";} 
         }
        });
      })
    })
    </script>
</body>
</html>
