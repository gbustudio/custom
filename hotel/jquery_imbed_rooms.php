<?php require_once("includes/initialize.php");

$room_id = $_POST['room'];
$num_rooms = $_POST['num_rooms'];
$check_in_date = $_POST['check_in_date'];
$check_out_date = $_POST['check_out_date'];

if(!validateDate($check_in_date) or strtotime($check_in_date)<time()){ $check_in_date=date("m/d/Y"); }
if(!validateDate($check_out_date) or strtotime($check_out_date)>time()+(86400*90)){$next=time()+86400; $check_out_date=date("m/d/Y", $next);}

$room = get_room($room_id);
if(!check_room_availability($room_id, strtotime($check_in_date), strtotime($check_out_date), $num_rooms)){$class="disabled";}else{$class="";}

$date_diff = (strtotime($check_out_date) - strtotime($check_in_date)) / 86400;
?>
<div style="background-color: #fefefe; padding: 6px; border-radius: 5px" class="<?php echo $class; ?>">
    <table cellspacing="0" class="boocking-check-celander" style="width: 100%" >
    <thead>
    <tr>
    <th style="width:20%" class="roomcol mobile-hide"><?php echo $_LANGUAGE['occupancy']; ?></th>
    <th style="width:10%" class="date mobile-hide"><?php echo $_LANGUAGE['date']; ?></th>
    <th style="width:40%" class="inclusions mobile-hide"><?php echo $_LANGUAGE['daily_inclusions']; ?></th>
    <th style="width:15%" class="numeric"><?php echo $_LANGUAGE['room_rate']; ?></th>
    <th style="width:15%" class="numeric"><?php echo $_LANGUAGE['total']; ?></th>
    </tr>
    </thead>
    <tbody class="rooms">

<?php
$room_no = 0;
$total_price = 0;
for($i=1; $i<=$num_rooms; $i++){
  $flag = FALSE;
  $dd = strtotime($check_in_date);
          for($j=0; $j<$date_diff; $j++){
            $date = $dd + (86400*$j);
            // $sql_query = $DB->query('select ' . strtolower(date('D', $date)) .' AS price from bsi_priceplan where room_id='.$room_id);
            // $price_result = mysqli_fetch_array($sql_query);

            $sql_query = $DB->query('select ' . strtolower(date('D', $date)) .' AS price from bsi_priceplan where status=1 and "'.date('Y-m-d',$date).'" between start_date and end_date and room_id ='.$room_id." order by plan_id desc limit 1");
            if($DB->numRows($sql_query)<1){
              $sql_query2 = $DB->query('select ' . strtolower(date('D', $date)) .' AS price from bsi_priceplan where status=1 and start_date="0000-00-00" and room_id ='.$room_id." order by plan_id desc limit 1");
              if($DB->numRows($sql_query2)<1){
                $price = $room->room_rate;
              } else { $price_result = mysqli_fetch_array($sql_query2); $price=$price_result['price']; }
            } else { $price_result = mysqli_fetch_array($sql_query); $price=$price_result['price']; }

            $total_price += ceil($price);
        ?>  
          
      <tr class="control" data-room-index="0">
      <td style="width:20%" class="roomcol">

        <?php if(!$flag){ ?>  

          <div class="guest_name_rrt" style="position: absolute; margin-left: 112px;display:none;">
            <input style="margin-left: -70px;width: 100px !important;" class="input-text" id="reservation_guest_name_<?php echo $i; ?>" name="reservation_guest_name_<?php echo $i; ?>" placeholder="Guest Name" type="text" value="">
          </div>
          <ol style="margin-bottom: 20px">
          <li>
            <strong>Room <?php echo $i;?></strong>
          </li>

        <?php } ?>  

        </td>
          <td style="width:10%" class="date mobile-hide"> <?php echo date('D d M', $date);?></td>
          <td style="width:40%" class="inclusions mobile-hide"><?php echo amenities($room->room_basic_amenities,"room");?></td>
          <td style="width:15%" class="rrt_rate_date_<?php echo $i.'_'.$j; ?> numeric"><?php echo amount($price, $room_id);?></td>
          <td style="width:15%" class="rrt_date_total_<?php echo $i.'_'.$j; ?> numeric"><?php echo amount($price, $room_id);?></td>
        </tr>

        <?php
          $flag = TRUE;
          }
}
?>  
  
</tbody>
</table>

<?php if(hotel_booking_extras_chan($room->hotel_id)){ ?>
	<table cellspacing="0" class="boocking-check-celander" style="width: 100%">

		<tbody class="extras">
		  <tr class="subheader">
			  <td colspan="5">
			  <h4 style="margin-top: 30px;"><?php echo $_LANGUAGE['extras_available_for_this_room']; ?></h4>
			  </td>
		</tr>
		<?php echo hotel_booking_extras_chan($room->hotel_id); ?>
		<!-- <a href="#" class="load-more button full-width btn-large fourty-space">LOAD MORE ROOMS</a> -->
		</tbody>
	</table>
<?php } else { ?>
	<table cellspacing="0" class="boocking-check-celander" style="width: 100%">

		<tbody class="extras">
		  <tr class="subheader">
			  <td colspan="5">
			  <h4 style="margin-top: 30px;">No extra option for this room</h4>
			  </td>
		</tr>
		</tbody>
	</table>
<?php } ?>






<table cellspacing="0" class="boocking-check-celander" style="width: 100%" >
    <tbody class="totals ">
      <tr class="subheader">
      <td colspan="3">
      <h4 style="margin-top: 20px; border-top: solid 1px #e8e8e8;"><?php echo $_LANGUAGE['totals']; ?></h4>
      </td>
      </tr>
      <tr class="subtotal">
      <td class="total" style="width:80%"><?php echo $_LANGUAGE['room_charges']; ?></td>
      <td class="numeric room_total"><?php echo amount($total_price, $room_id); ?></td>
      </tr>
      <tr class="subtotal">
      <td class="total"><?php echo $_LANGUAGE['extras_total']; ?></td>
      <td class="numeric extra_total"><?php echo amount(0); ?></td>
      </tr>
      <tr class="grand_total" style="border-top: 2px solid rgb(67, 74, 80); font-size: 13px; font-weight: bold; color: rgb(102, 102, 102);">
      <td class="total" style="padding: 5px 0;"><?php echo $_LANGUAGE['grand_total']; ?></td>
      <td class="numeric"><span id="total_payment" style="white-space: nowrap;"><?php echo amount($total_price, $room_id); ?></span><span style="margin-left: 20px; font-size: 17px; color: rgb(125, 185, 33);">&#10004;</span></td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
      <input id="reservation_total" name="reservation_total" type="hidden" value="<?php echo str_replace(user_currency($_COOKIE['currency']),"",amount($total_price, $room_id)); ?>">
      <!-- <th class="bottom_left_corner bottom_right_corner" colspan="8">
      <em><?php echo $_LANGUAGE['note_infants_not_included']; ?></em>
      </th> -->
      </tr>
    </tfoot>
  </table>  
</div> 
<script type="text/javascript">
  tjq(function(){
    tjq("#reservation_required_info").<?php if($class=="disabled"){echo "addClass";}else{echo "removeClass";} ?>("disabled");
  })
</script>