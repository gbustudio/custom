<?php require_once("includes/initialize.php");
$booking = get_booking(base64_decode($_GET['booking'])-123);
if($booking->payment_success==1){$status="Completed";}elseif($booking->payment_success==-1){$status="Failed";}else{$status="Pending";}
if(!$booking){ header("Location: http://localhost/custom/hotel/"); exit();}
require("header.php");
?>
       <section id="content">
            <div class="container">
                <div class="row">
                  
                  <div id="main" class="col-sm-8 col-md-9">
                        <div class="booking-information travelo-box">
                            <h2>Booking Confirmation</h2>
                            <hr>
                            <div class="booking-confirmation clearfix">
                                <i class="soap-icon-recommend icon circle"></i>
                                <div class="message">
                                    <h4 class="main-message">Thank You. Your Booking Order is Received.</h4>
                                    <p>We will review your booking details and will send you booking confirmation email shortly!</p>
                                </div>
                                <a href="#" id="printit" class="button btn-small print-button uppercase">print Details</a>
                            </div>
                            <hr>
                            <h2>Traveler Information</h2>
                            <dl class="term-description">
                                <dt>Booking number:</dt><dd><?=$booking->booking_id?></dd>
                                <dt>Check-in:</dt><dd><?=date("M d, Y", strtotime($booking->start_date))?></dd>
                                <dt>Check-out:</dt><dd><?=date("M d, Y", strtotime($booking->end_date))?></dd>
                                <dt>Rooms:</dt><dd><?=count(json_decode($booking->booking_details_json, true))?></dd>
                            </dl>
                            <hr>
                            <h2>Payment <?=$status?></h2>
                            <?php if($booking->payment_success==1 and $booking->payment_type=="pp"){ ?>
                            <p class="red-color">Payment is made Via Paypal Transaction ID: <?=$booking->payment_txnid?>.</p>
                            <?php } ?>
                        </div>
                    </div>
                 <?php require("sidebar.php"); ?>
                </div>
            </div>
        </section>
        
    </div>
   <?php require("footer.php"); ?>
   <script type="text/javascript">
   tjq(function(){
      tjq(document).on("click", "#printit", function(e){
          e.preventDefault();
          window.print();
          return false;
      });
    });
   </script>
   
</body>
</html>