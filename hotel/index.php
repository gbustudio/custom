<?php require_once("includes/initialize.php");
        require("header.php");
?>
        <section id="content">
            <div class="container">
                <div class="row">
                   <div id="main" class="col-md-9">
                   <?php
                   		$q = $DB->query("select permalink,hotel_name from bsi_hotels where status=1"); // and paid_upto>NOW()
                   		while($r=$DB->fetchObject($q)){
                   			echo "<h3><a href='".$r->permalink."/'>".$r->hotel_name."</a></h3>";
                   		}
                   ?>
                   </div>
                </div>
            </div>
        </section>
    </div>
<?php if(!logged()){ ?>
<!-- registration html start -->
<div id="au-signup" style="background:#fefefe;padding:20px;display:none;" class="col-sm-offset-2 col-md-offset-2 col-sm-8 col-md-8">
    <form id="au_signup_form" style="background:#fefefe;padding:20px;">
      <div class="person-information">
          <h2><?php echo $_LANGUAGE['register']; ?></h2>
          <div class="form-group row">
              <div class="col-sm-12 col-md-12">
                  <label><?php echo $_LANGUAGE['title']; ?></label>
                  <div class="selector">
                      <select name="title" class="full-width" id="title">
                        <option value="Mr.">Mr.</option>
                        <option value="Ms.">Ms.</option>
                        <option value="Mrs.">Mrs.</option>
                        <option value="Miss.">Miss.</option>
                        <option value="Dr.">Dr.</option>
                        <option value="Prof.">Prof.</option>
                      </select>
                  </div>
              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['first_name']; ?></label>
                  <input type="text" name="fname" required id="fname" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['first_name']; ?>" />
              </div>
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['last_name']; ?></label>
                  <input type="text" name="lname" required id="lname" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['last_name']; ?>" />
              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-12">
                  <label><?php echo $_LANGUAGE['street_address']; ?></label>
                  <input type="text" name="str_addr" required id="str_addr" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['street_address']; ?>" />
              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-6">
                <label><?php echo $_LANGUAGE['city']; ?> &AMP; <?php echo $_LANGUAGE['zipcode']; ?></label>
                  <div class="constant-column-2">
                      <div class="selector">
                          <input type="text" required name="city"  id="city" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['city']; ?>" />
                      </div>
                      <div class="selector">
                          <input type="text" name="zipcode"  id="zipcode" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['zipcode']; ?>" />
                      </div>
                  </div>
              </div>
              <div class="col-sm-6 col-md-6">
                <label><?php echo $_LANGUAGE['state']; ?> &AMP; <?php echo $_LANGUAGE['country']; ?></label>
                  <div class="constant-column-2">
                      <div class="selector">
                          <input type="text" required name="state"  id="state" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['country']; ?>" />
                      </div>
                      <div class="selector">
                          <select required name="country" class="full-width">
                            <option value=""><?php echo $_LANGUAGE['select_country']; ?></option>
                            <?php echo countries_list(); ?>
                          </select>
                      </div>
                  </div>
              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['phone']; ?></label>
                  <input type="text" required name="phone" id="phone" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['phone']; ?>" />
              </div>
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['fax']; ?></label>
                  <input type="text" name="fax" id="fax" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['fax']; ?>" />
              </div>
          </div>
      </div>
     <!--  <hr /> -->
      <div class="extra-information">
          <!-- <h2>Extra Details</h2> -->
          <div class="form-group row">
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['identity_type']; ?></label>
                  <input name="id_type" id="id_type" required type="text" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['identity_type']; ?>" />
              </div>
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['identity_number']; ?></label>
                  <input type="text" name="id_number" required id="id_number" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['identity_number']; ?>" />
              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['email_address']; ?></label>
                  <input type="email" name="email" required id="email" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['email_address']; ?>" />
              </div>
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['password']; ?></label>
                  <input type="password" required name="password" id="password" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['password']; ?>" />
              </div>
          </div>
      </div>
      <div class="form-group row">
          <div class="col-sm-10 col-md-12">
              <button id="btn_exisitng_cust" type="submit" class="full-width btn-large"><?php echo $_LANGUAGE['signup']; ?></button>
          </div>
      </div>
      <p><?php echo $_LANGUAGE['alread_member']; ?>? &nbsp; &nbsp; <a href="#au-login" class="goto-login soap-popupbox"><?php echo $_LANGUAGE['login']; ?></a></p>
    </form>
</div> <!-- register end  -->
<div id="au-login" class="travelo-login-box travelo-box">
    <form id="au_login_form"  style="background:#fefefe;padding:20px;">
        <div class="form-group">
            <input type="email" required name="email" class="input-text full-width" placeholder="<?php echo $_LANGUAGE['email_address']; ?>">
        </div>
        <div class="form-group">
            <input type="password" required name="pass" class="input-text full-width" placeholder="<?php echo $_LANGUAGE['password']; ?>">
        </div>
        <div class="form-group">
            <!-- <a href="#" class="forgot-password pull-right">Forgot password?</a> -->
            <div class="checkbox checkbox-inline">
                <label>
                    <input name="remember" type="checkbox"> <?php echo $_LANGUAGE['remember_me']; ?>
                </label>
            </div>
        </div>
        <button type="submit" class="full-width btn-medium"><?php echo $_LANGUAGE['login']; ?></button>
    </form>
    <div class="seperator"></div>
    <p><?php echo $_LANGUAGE['dont_have_account']; ?>? <a href="#au-signup" class="goto-signup soap-popupbox"><?php echo $_LANGUAGE['signup']; ?></a></p>
</div>
<!-- login html end -->
<?php } ?>

    <?php require("footer.php"); ?>
    
</body>
</html>
