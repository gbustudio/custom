<?php require_once("includes/initialize.php");
if(!logged()){header("Location: hotel-detailed.php");}
$user=get_user($_SESSION['user_id']);
require("header.php");
?>
<section id="content">
    <div class="container">
		<div id="main">
		    <div class="tab-container full-width-style arrow-left dashboard">
		        <ul class="tabs">
		            <li class="active"><a data-toggle="tab" href="#profile"><i class="soap-icon-user circle"></i>Profile</a></li>
		            <li class=""><a data-toggle="tab" href="#booking"><i class="soap-icon-businessbag circle"></i>Booking</a></li>
		            <li class=""><a data-toggle="tab" href="#settings"><i class="soap-icon-settings circle"></i>Settings</a></li>
		        </ul>
		        <div class="tab-content">
		            <div id="profile" class="tab-pane fade active in">
		                <div class="view-profile">
		                    <article class="image-box style2 box innerstyle personal-details">
		                        <figure>
		                            <a title="" href="#"><img width="270" height="263" alt="" src="<?=imageCheck($user->profile_pic,"src","users")?>"></a>
		                        </figure>
		                        <div class="details">
		                            <a href="#" class="button btn-mini pull-right edit-profile-btn">EDIT PROFILE</a>
		                            <h2 class="box-title fullname"><?=$user->first_name." ".$user->last_name?></h2>
		                            <dl class="term-description">
		                                <dt>first name:</dt><dd><?=$user->first_name?></dd>
		                                <dt>last name:</dt><dd><?=$user->last_name?></dd>
		                                <dt>user email:</dt><dd><?=$user->email?></dd>
		                                <dt>phone number:</dt><dd><?=$user->phone?></dd>
		                                <dt>Street Address and number:</dt><dd><?=$user->street_address?></dd>
		                                <dt>Town / City:</dt><dd><?=$user->city.", ".$user->state?></dd>
		                                <dt>ZIP code:</dt><dd><?=$user->zip?></dd>
		                                <dt>Country:</dt><dd><?=country($user->country_id)?></dd>
		                                <dt>Identity Type:</dt><dd><?=$user->identity_type?></dd>
		                                <dt>Identity Number:</dt><dd><?=$user->identity_number?></dd>
		                            </dl>
		                        </div>
		                    </article>
		                    <hr>
		                    <h2>About You</h2>
		                    <div class="intro">
		                        <?=$user->about?>
		                    </div>
		                    <hr>
		                </div>
		                <div class="edit-profile" style="display: none;">
		                    <form class="edit-profile-form" action="" method="post" enctype="multipart/form-data">
		                        <h2>Personal Details</h2>
		                        <div class="col-sm-9 no-padding no-float">
		                            <div class="row form-group">
		                                <div class="col-sms-6 col-sm-6">
		                                    <label>First Name</label>
		                                    <input type="text" name="fname" value="<?=$user->first_name?>" required class="input-text full-width" placeholder="">
		                                </div>
		                                <div class="col-sms-6 col-sm-6">
		                                    <label>Last Name</label>
		                                    <input type="text" name="lname" value="<?=$user->last_name?>" required class="input-text full-width" placeholder="">
		                                </div>
		                            </div>
		                            <div class="row form-group">
		                                <div class="col-sms-6 col-sm-6">
		                                    <label>Email Address</label>
		                                    <input type="email" value="<?=$user->email?>" name="email" required class="input-text full-width" placeholder="">
		                                </div>
		                                <div class="col-sms-3 col-sm-3">
		                                    <label>Phone Number</label>
		                                    <input type="text" name="phone" value="<?=$user->phone?>" required class="input-text full-width" placeholder="">
		                                </div>
		                                <div class="col-sms-3 col-sm-3">
		                                    <label>Fax</label>
		                                    <input type="text" value="<?=$user->fax?>" name="fax" class="input-text full-width" placeholder="">
		                                </div>
		                            </div>
		                            
		                            <hr>
		                            <h2>Identity Details</h2>
		                        	<div class="col-sm-9 no-padding no-float">
			                            <div class="row form-group">
			                                <div class="col-sms-6 col-sm-6">
			                                    <label>Identity Type</label>
			                                    <input type="text" value="<?=$user->identity_type?>" name="idtype" required class="input-text full-width" placeholder="">
			                                </div>
			                                <div class="col-sms-6 col-sm-6">
			                                    <label>Identity Number</label>
			                                    <input type="text" name="idno" value="<?=$user->identity_number?>" required class="input-text full-width" placeholder="">
			                                </div>
			                            </div>
		                            </div>
		                            <hr>
		                            <h2>Contact Details</h2>
		                            <div class="row form-group">
		                                <div class="col-sms-6 col-sm-6">
		                                    <label>Address</label>
		                                    <input type="text" value="<?=$user->street_address?>" name="address" required class="input-text full-width">
		                                </div>
		                               <div class="col-sms-6 col-sm-6">
		                                    <label>City</label>
		                                    <input type="text" name="city" value="<?=$user->city?>" required class="input-text full-width">
		                                </div>
		                            </div>
		                            <div class="row form-group">
		                                <div class="col-sms-3 col-sm-3">
		                                    <label>Region State</label>
		                                    <input type="text" name="state" value="<?=$user->state?>" required class="input-text full-width">
		                                </div>
		                                <div class="col-sms-3 col-sm-3">
		                                    <label>Zip</label>
		                                    <input type="text" name="zip" value="<?=$user->zip?>" required class="input-text full-width">
		                                </div>
		                                <div class="col-sms-6 col-sm-6">
		                                    <label>Country</label>
		                                    <div class="selector">
		                                        <select name="country" required class="full-width">
		                                            <option value="">Select Country</option>
		                                            <?php
		                                            	$qc = $DB->query("SELECT * FROM countries WHERE status=1 ORDER BY country_name ASC");
														while($country=$DB->fetchObject($qc)){
															if($country->country_id==$user->country_id){$sel="selected='selected'";}else{$sel="";}
															echo '<option '.$sel.' value="'.$country->country_id.'">'.$country->country_name.'</option>';
														}
		                                            ?>
		                                        </select>
		                                    </div>
		                                </div>
		                            </div>

		                            <hr>
		                            <h2>Upload Profile Photo</h2>
		                            <div class="row form-group">
		                                <div class="col-sms-12 col-sm-6 no-float">
		                                    <div class="fileinput full-width" style="line-height: 34px;">
		                                        <input name="pic" type="file" class="input-text" data-placeholder="select image/s">
		                                        <input type="hidden" name="file" value="<?=$user->profile_pic?>">
		                                    </div>
		                                </div>
		                            </div>
		                            <hr>
		                            <h2>Describe Yourself</h2>
		                            <div class="form-group">
		                                <textarea rows="5" name="about" class="input-text full-width" placeholder="please tell us about you"><?=strip_tags($user->about)?></textarea>
		                            </div>
		                            <div class="from-group">
		                                <button name="editprofile" type="submit" class="btn-medium col-sms-6 col-sm-4">UPDATE SETTINGS</button>
		                            </div>

		                        </div>
		                    </form>
		                </div>
		            </div>
		            <div id="booking" class="tab-pane fade">
		                <h2>Trips You have Booked!</h2>
		               <!--  <div class="filter-section gray-area clearfix">
		                    <form>
		                        <label class="radio radio-inline checked">
		                            <input type="radio" name="filter" checked="checked">
		                            All Types
		                        </label>
		                        <label class="radio radio-inline">
		                            <input type="radio" name="filter">
		                            Hotels
		                        </label>
		                        <label class="radio radio-inline">
		                            <input type="radio" name="filter">
		                            Flights
		                        </label>
		                        <label class="radio radio-inline">
		                            <input type="radio" name="filter">
		                            Cars
		                        </label>
		                        <label class="radio radio-inline">
		                            <input type="radio" name="filter">
		                            Cruises
		                        </label>
		                        <div class="pull-right col-md-6 action">
		                            <h5 class="pull-left no-margin col-md-4">Sort results by:</h5>
		                            <button id="sortbydate" class="btn-small white gray-color">DATE</button>
		                            <button id="sortbystatus" class="btn-small white gray-color">STATUS</button>
		                        </div>
		                    </form>
		                </div> -->
		                <div class="booking-history">
		                    <?=booking_history()?>
		                </div>

		            </div>
		            <div id="settings" class="tab-pane fade">
		                <h2>Account Settings</h2>
		                <h5 class="skin-color">Change Your Password</h5>
		                <form action="" method="post">
		                    <div class="row form-group">
		                        <div class="col-xs-12 col-sm-6 col-md-4">
		                            <label>Old Password</label>
		                            <input required name="oldpass" type="text" class="input-text full-width">
		                        </div>
		                    </div>
		                    <div class="row form-group">
		                        <div class="col-xs-12 col-sm-6 col-md-4">
		                            <label>Enter New Password</label>
		                            <input required name="pass" type="text" class="input-text full-width">
		                        </div>
		                    </div>
		                    <div class="row form-group">
		                        <div class="col-xs-12 col-sm-6 col-md-4">
		                            <label>Confirm New password</label>
		                            <input required name="pass2" type="text" class="input-text full-width">
		                        </div>
		                    </div>

		                <hr>

		                <h5 class="skin-color">Send Me Emails</h5>
		                
		                    <div class="checkbox">
		                        <label>
		                            <input name="newsletters" value="1" type="checkbox"> BookingVenture has periodic offers and deals on really cool destinations.
		                        </label>
		                    </div>
		                    <!-- <div class="checkbox">
		                        <label>
		                            <input type="checkbox"> Travelo has fun company news, as well as periodic emails.
		                        </label>
		                    </div>
		                    <div class="checkbox">
		                        <label>
		                            <input type="checkbox"> I have an upcoming reservation.
		                        </label>
		                    </div> -->
		                    <button name="updatepass" type="submit" class="btn-medium uppercase">Update Settings</button>
		                </form>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</section>

<?php require("footer.php"); ?>

<script type="text/javascript">
    tjq(document).ready(function() {
        tjq("#profile .edit-profile-btn").click(function(e) {
            e.preventDefault();
            tjq(".view-profile").fadeOut();
            tjq(".edit-profile").fadeIn();
        });

        setTimeout(function() {
            tjq(".notification-area").append('<div class="info-box block"><span class="close"></span><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus ab quis a dolorem, placeat eos doloribus esse repellendus quasi libero illum dolore. Esse minima voluptas magni impedit, iusto, obcaecati dignissimos.</p></div>');
        }, 10000);
    });
    tjq('a[href="#profile"]').on('shown.bs.tab', function (e) {
        tjq(".view-profile").show();
        tjq(".edit-profile").hide();
    });
</script>

</body>
</html>