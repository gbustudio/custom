<style>
a.back-to-top {
	display: none;
	width: 48px;
	height: 48px;
	text-indent: -9999px;
	position: fixed;
	z-index: 999;
	right: 20px;
	bottom: 20px;
	background: rgba(0, 0, 0, 0) url("images/icon/up.png") no-repeat center;
}
</style>
<a href="#" class="back-to-top">Back to Top</a>


 <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>
    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>
    <!-- Google Map Api -->
    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    <script type="text/javascript" src="js/calendar.js"></script>
    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>
    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>
    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>

    <script type="text/javascript">
	
	
	
      tjq(function(){
		  
		  
var amountScrolled = 300;
tjq(window).scroll(function() {
	if ( tjq(window).scrollTop() > amountScrolled ) {
		tjq('a.back-to-top').fadeIn('slow');
	} else {
		tjq('a.back-to-top').fadeOut('slow');
	}
});
tjq('a.back-to-top').click(function() {
	tjq('html, body').animate({
		scrollTop: 0
	}, 700);
	return false;
});
		  
		  
		  
       
       //var tooltips_vals =  {"11416652182":{"hot":"1","min_stay":"2 NIGHTS","incl_capacity":"2 People","max_capacity":"4 People","availability":"2 Rooms","promo":"- wifi<br>-breakfast everyday"}};

        tjq(document).on("mouseenter", ".boocking-check-celander td.rate:not(:has(.unavailable))", function(event){
            var dd = tjq(this).data('id');
            tjq("#au_tooltips .min-stay-tooltip").text(tooltips_vals[dd]['min_stay']);
            tjq("#au_tooltips .availability-tooltip").text(tooltips_vals[dd]['availability']);
            tjq("#au_tooltips .inc-occu-tooltip").text(tooltips_vals[dd]['incl_capacity']);
            tjq("#au_tooltips .max-occu-tooltip").text(tooltips_vals[dd]['max_capacity']);
            tjq("#au_tooltips .details-tooltip").html(tooltips_vals[dd]['promo']);
            if(tooltips_vals[dd]['hot']=="0"){ tjq("#au_tooltips .hot-deal-tooltip").hide();}
            tjq("#au_tooltips").css({
                 top: event.pageY + 5 + "px",
                left: event.pageX - 300 + "px"
            }).show();
       })
       tjq(document).on("mouseleave", ".boocking-check-celander td.rate", function(){
            tjq("#au_tooltips").hide();
       })


        tjq(document).on("click", ".currency ul a", function(e){
        	e.preventDefault();
        	var cur = tjq(this).text();
        	tjq.ajax({
	            type:'POST',
	            data: "currency_change="+cur,
	            url:'includes/ajax_func.php',
	            success:function(data) { if(data==1){ location.reload();} }
	          });
        })

        tjq(document).on("click", ".language ul a", function(e){
        	e.preventDefault();
        	var lang = tjq(this).data("code");
        	tjq.ajax({
	            type:'POST',
	            data: "language_change="+lang,
	            url:'includes/ajax_func.php',
	            success:function(data) { if(data==1){ location.reload();} }
	          });
        })

        tjq(document).on("click", "#au_logout", function(e){
        	e.preventDefault();
        	tjq.ajax({
	            type:'POST',
	            data: "logoutMe=1",
	            url:'includes/ajax_func.php',
	            success:function(data) { if(data==1){ location.reload(); } }
	          });
        })
        
        // tjq(".card-information").hide();
        // tjq(document).on("click", "#payment_type_1", function(){
        //   tjq(".card-information").hide();
        //   window.open("http://www.paypal.com");
        // });
        // tjq(document).on("click", "#payment_type_2", function(){
        //   tjq(".card-information").hide();
        //   window.open("http://www.2checkout.com");
        // });
        // tjq(document).on("click", "#payment_type_3", function(){
        //   tjq(".card-information").hide();
        //   window.open("http://www.authorize.net");
        // });
        // tjq(document).on("click", "#payment_type_4", function(){
        //   tjq(".card-information").hide();
        //   window.open("http://www.stripe.com");
        // });
        // tjq(document).on("click", "#payment_type_6", function(){
        //   tjq(".card-information").toggle("slow");
        // });
      })
    </script>

    <script type="text/javascript">
        tjq(document).ready(function() {  
            tjq(document).on("submit", "#au_signup_form", function(e){
                e.preventDefault();
                var dataString = tjq(this).serializeArray();
                dataString[dataString.length] = {name:"signupme",value:"1"};
                tjq.ajax({
                    type:'POST',
                    data: dataString,
                    url:'includes/ajax_func.php',
                    success:function(data) { doForHotel(data); }
                  });
            })

            tjq(document).on("submit", "#au_login_form", function(e){
                e.preventDefault();
                var dataString = tjq(this).serializeArray();
                var v;
                if(tjq(this).parent().hasClass("popup-content")){v=1;}else{v=2;}
                dataString[dataString.length] = {name:"signinme",value:v};
                tjq.ajax({
                    type:'POST',
                    data: dataString,
                    url:'includes/ajax_func.php',
                    success:function(data) { doForHotel(data); }
                  });
            })

            tjq(".goto-writereview-pane").click(function(e) {
                e.preventDefault();
                tjq('#hotel-features .tabs a[href="#hotel-write-review"]').tab('show')
            });

            tjq(document).on("click", ".service_rate, .value_rate, .sleep_rate, .clean_rate, .location_rate, .rooms_rate, .pool_rate, .fitness_rate", function(){
                var stars = parseInt(tjq(this).children("a.ui-slider-handle").attr("style").replace("left: ","").replace("%;",""));
                var classof = tjq(this).attr("class").split(' ');
                tjq("input[name="+classof[2]+"]").val(stars);
                var all=0;
                tjq(".upvote").each(function() {
                    all = parseInt(all) + parseInt(tjq(this).val());
                })
                var allrate = Math.ceil(all/8);
                tjq(".overall-rating .five-stars-container .five-stars").width(allrate+"%");
                tjq("input[name=rate_all]").val(Math.floor(allrate));
            })

            tjq(document).on("click", ".trip_type", function(){
                var t = tjq(this).attr("id").split("_").pop();
                tjq("input[name=trip_type]").val(t);
                //alert(t);   
            })
            
            // editable rating
            tjq(".editable-rating.five-stars-container").each(function() {
                var oringnal_value = tjq(this).data("original-stars");
                if (typeof oringnal_value == "undefined") {
                    oringnal_value = 0;
                } else {
                    //oringnal_value = 10 * parseInt(oringnal_value);
                }

                tjq(this).slider({
                    range: "min",
                    value: oringnal_value,
                    min: 0,
                    max: 5,
                    slide: function( event, ui ) {
                        
                    }
                });
            });
        });
        
        tjq('a[href="#map-tab"]').on('shown.bs.tab', function (e) {
            var center = panorama.getPosition();
            google.maps.event.trigger(map, "resize");
            map.setCenter(center);
        });
        tjq('a[href="#steet-view-tab"]').on('shown.bs.tab', function (e) {
            fenway = panorama.getPosition();
            panoramaOptions.position = fenway;
            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
            map.setStreetView(panorama);
        });
        var map = null;
        var panorama = null;
        var fenway = new google.maps.LatLng(<?php echo $hotel->hotel_latitude_longitude; ?>);
		//var_dump("chanif ".$hotel->hotel_latitude_longitude);
        var mapOptions = {
            center: fenway,
            zoom: 12
        };
        var panoramaOptions = {
            position: fenway,
            pov: {
                heading: 34,
                pitch: 10
            }
        };
        function initialize() {
            tjq("#map-tab").height(tjq("#hotel-main-content").width() * 0.6);
            map = new google.maps.Map(document.getElementById('map-tab'), mapOptions);
            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
            map.setStreetView(panorama);
        }
        google.maps.event.addDomListener(window, 'load', initialize);

        function doForHotel(data){
            // console.log(data);
            if(data==1){alert("You are successfully registered. Please login now!");tjq("#au_signup_form").trigger('reset');}
            else if(data==2){alert("User already exists. Please login!");tjq("#au_signup_form").trigger('reset');}
            else if(data==3){alert("Please fill form correctly!");}
            else if(data==4){alert("User Does not exist, Please signup for a new account!");}
            else if(data==5){location.reload();}
            else if(data.substring(0,1)==6){ var u = data.split("_").pop(); tjq("ul.tabs, #login-tab, #register-tab").hide(); tjq("input[name=user]").val(u); tjq("#reservation_required_info").show(); }
            else { alert("There are some issues on our side, Please try again later!");location.reload();}
        }
		
    </script>