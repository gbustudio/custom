<?php require_once("includes/initialize.php");
$room_id = $_POST['room'];
$room = get_room($room_id);
$hotel = hotel_details($room->hotel_id);
$dated = strtotime($_POST['on']);
$upto = ceil((strtotime($_POST['upto'])-strtotime($_POST['on']))/86400);
?>

<table cellspacing="0" class="table boocking-check-celander">
<thead class="controls">
<tr>

<th colspan="16" class="date_navigation">
<table class="table" style="margin-bottom: 0px">
<tbody><tr>
<th class="date_nav top_left_corner">

    <a class="button yellow uppercase btn-small mobile-hide" style="float: left; margin-right: 1px;" href="javascript:embed_availablity_celander('<?php echo date("m/d/Y", $dated - (86400 * 7));?>','<?php echo $room_id;?>');">&leftleftarrows; <?php echo $_LANGUAGE['back_1_week']; ?></a>
    <a class="button yellow uppercase btn-small mobile-hide" style="float: left" href="javascript:embed_availablity_celander('<?php echo date("m/d/Y", $dated - 86400);?>','<?php echo $room_id;?>');">&leftarrow; <?php echo $_LANGUAGE['back_1_day']; ?></a>

    <a class="button yellow uppercase btn-small mobile-hide" style="float: right; margin-left: 1px" href="javascript:embed_availablity_celander('<?php echo date("m/d/Y", $dated + (86400 * 7));?>','<?php echo $room_id;?>');"><?php echo $_LANGUAGE['forward_1_week']; ?> &rightrightarrows;</a>
    <a class="button yellow uppercase btn-small mobile-hide" style="float: right" href="javascript:embed_availablity_celander('<?php echo date("m/d/Y", $dated + 86400);?>','<?php echo $room_id;?>');"><?php echo $_LANGUAGE['forward_1_day']; ?> &rightarrow;</a>

    <a class="mobile-show" style="float: left; margin-right: 1px; display: none; background-color: #434a50; padding: 5px; color: #fff;" href="javascript:embed_availablity_celander('<?php echo date("m/d/Y", $dated - (86400 * 7));?>','<?php echo $room_id;?>');">&leftleftarrows; 1 week &nbsp;</a>
    <a class="mobile-show" style="float: left; color: #fff; background-color: #434a50; display: none; padding: 5px;" href="javascript:embed_availablity_celander('<?php echo date("m/d/Y", $dated - 86400);?>','<?php echo $room_id;?>');">&leftarrowtail; 1 day</a>
    
    <a class="mobile-show" style="float: right; color: #fff; background-color: #434a50; display: none; margin-left: 1px; padding: 5px;" href="javascript:embed_availablity_celander('<?php echo date("m/d/Y", $dated + (86400 * 7));?>','<?php echo $room_id;?>');">1 Week &rightrightarrows;</a>
    <a class="mobile-show" style="float: right; color: #fff; background-color: #434a50; display: none; padding: 5px;" href="javascript:embed_availablity_celander('<?php echo date("m/d/Y", $dated + 86400);?>','<?php echo $room_id;?>');">1 Day &rightarrowtail; &nbsp;</a>
</th>
</tr>
</tbody></table>
</th>
</tr>

</thead>
<thead>
<tr>
<th><?php echo $_LANGUAGE['room_selection']; ?></th>
<th class="rack_rate mobile-hide"><?php echo $_LANGUAGE['full_rate']; ?></th>

<?php
//$crunt_date = $dated;
$tooltips = array();
for($i=0; $i<8; $i++){
	$crunt_date = $dated + (86400*$i);
	$day_name = date('D', $crunt_date);
	$day = date('d', $crunt_date);
	$month = date('M', $crunt_date);
	$year = date('Y', $crunt_date);
	?>
	<th class="date <?php echo ($i < 6) ? '' : 'mobile-hide';?> <?php echo ($day_name == 'Sat' || $day_name == 'Sun') ? 'weekend' : '';?>">
	<span class="day_name"><?php echo $day_name;?></span>
	<span class="day"><?php echo $day;?></span>
	<span class="month"><?php echo $month;?></span>
	</th>
	<?php
}
?>
</tr>
</thead>
<tbody>
<tr>
<td class="room_name"><?php echo room_type_title($room->roomtype_id).' '.room_capacity($room->capacity_id);?><?php //echo $room_type['type_name']?></td>
<td class="rack_rate mobile-hide" style="white-space: nowrap"><?php echo amount($room->room_rate);?></td>

<?php
//$crunt_date = $dated;
for($i=0; $i<8; $i++){
  $crunt_date = $dated + (86400*$i);
  $day_name = date('D', $crunt_date);
if($crunt_date>=strtotime("today")){
  if(!room_available($room_id, $crunt_date)){
?>
<td class="rate <?php echo ($i < 6) ? '' : 'mobile-hide';?> <?php echo ($day_name == 'Sat' || $day_name == 'Sun') ? 'weekend' : '';?>">
<span class="unavailable"><?php echo $_LANGUAGE['sold']; ?></span>
</td>
<?php
  }
  else{

     $sql_query = $DB->query('select ' . strtolower($day_name) .' AS price from bsi_priceplan where status=1 and "'.date('Y-m-d',$crunt_date).'" between start_date and end_date and room_id ='.$room_id." order by plan_id desc limit 1");
	  if($DB->numRows($sql_query)<1){
		$sql_query2 = $DB->query('select ' . strtolower($day_name) .' AS price from bsi_priceplan where status=1 and start_date="0000-00-00" and room_id ='.$room_id." order by plan_id desc limit 1");
		if($DB->numRows($sql_query2)<1){
		  $price = $room->room_rate;
		} else { $price_result = mysqli_fetch_array($sql_query2); $price=$price_result['price']; }
	  } else { $price_result = mysqli_fetch_array($sql_query); $price=$price_result['price']; }

?>
<td data-id="<?php echo $room_id.$crunt_date; ?>" class="rate special <?php echo ($i < 6) ? '' : 'mobile-hide';?> <?php echo ($day_name == 'Sat' || $day_name == 'Sun') ? 'weakend' : '';?> <?php echo ($i < $upto) ? 'selected' : '';?>">
<div class="hot"><?php echo $_LANGUAGE['hot']; ?>!</div>
<span style="font-size: 9px; white-space: nowrap;"><?php echo amount($price, $room_id);?></span>
</td>
<?php
      $tooltips[$room_id.$crunt_date] = calendarJsonTooltip($room_id, $crunt_date);
  }
  } 
  else {  //date passed ?>
      <td class="rate <?php echo ($i < 6) ? '' : 'mobile-hide';?> <?php echo ($day_name == 'Sat' || $day_name == 'Sun') ? 'weekend' : '';?>">
<span class="unavailable"><?php echo $_LANGUAGE['sold']; ?></span>
</td>
<?php }
}
?>

</tr>
</tbody>

</table>                                  

<script type="text/javascript">
  var tooltips_vals = <?php echo json_encode($tooltips); ?>
</script>                      