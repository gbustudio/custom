<?php require_once("includes/initialize.php");



$hotel_id = $_POST['hotel_id'];



$sql_query = $DB->query('SELECT * FROM  bsi_hotels where hotel_id = ' . $hotel_id);

$hotel_info = mysqli_fetch_array($sql_query);

?>



<table cellspacing="0" class="table boocking-check-celander multi-booking-check-availability">

<thead class="controls"> 

<tr>



<th colspan="16" class="date_navigation">

<table class="table" style="margin-bottom: 0px">

<tbody><tr>

<th class="date_nav top_left_corner">

    <a class="button yellow uppercase btn-small mobile-hide" style="float: left; margin-right: 1px;" href="javascript:embed_availablity_celander('<?php echo $_POST['datetime'] - (86400 * 7);?>','<?php echo $hotel_id;?>');">&leftleftarrows; <?php echo $_LANGUAGE['back_1_week']; ?></a>

    <a class="button yellow uppercase btn-small mobile-hide" style="float: left" href="javascript:embed_availablity_celander('<?php echo $_POST['datetime'] - 86400;?>','<?php echo $hotel_id;?>');">&leftarrow; <?php echo $_LANGUAGE['back_1_day']; ?></a>



    <a class="button yellow uppercase btn-small mobile-hide" style="float: right; margin-left: 1px" href="javascript:embed_availablity_celander('<?php echo $_POST['datetime'] + (86400 * 7);?>','<?php echo $hotel_id;?>');"><?php echo $_LANGUAGE['forward_1_week']; ?> &rightrightarrows;</a>

    <a class="button yellow uppercase btn-small mobile-hide" style="float: right" href="javascript:embed_availablity_celander('<?php echo $_POST['datetime'] + 86400;?>','<?php echo $hotel_id;?>');"><?php echo $_LANGUAGE['forward_1_day']; ?> &rightarrow;</a>



    <a class="mobile-show" style="float: left; margin-right: 1px; display: none; background-color: #fdb714; padding: 5px; color: #fff;" href="javascript:embed_availablity_celander('<?php echo $_POST['datetime'] - (86400 * 7);?>','<?php echo $hotel_id;?>');">&leftleftarrows; 1 week &nbsp;</a>

    <a class="mobile-show" style="float: left; color: #fff; background-color: #fdb714; display: none; padding: 5px;" href="javascript:embed_availablity_celander('<?php echo $_POST['datetime'] - 86400;?>','<?php echo $hotel_id;?>');">&leftarrowtail; 1 day</a>

    

    <a class="mobile-show" style="float: right; color: #fff; background-color: #fdb714; display: none; margin-left: 1px; padding: 5px;" href="javascript:embed_availablity_celander('<?php echo $_POST['datetime'] + (86400 * 7);?>','<?php echo $hotel_id;?>');">1 Week &rightrightarrows;</a>

    <a class="mobile-show" style="float: right; color: #fff; background-color: #fdb714; display: none; padding: 5px;" href="javascript:embed_availablity_celander('<?php echo $_POST['datetime'] + 86400;?>','<?php echo $hotel_id;?>');">1 Day &rightarrowtail; &nbsp;</a>

</th>

</tr>

</tbody></table>

</th>

</tr>



</thead>

<thead>

<tr>

<th><?php echo $_LANGUAGE['room_selection']; ?></th>

<th class="rack_rate  mobile-hide"><?php echo $_LANGUAGE['full_rate']; ?></th>



<?php

$d = $_POST['datetime'];

for($i=0; $i<7; $i++){

  $crunt_date = $d + (86400*$i);

  $day_name = date('D', $crunt_date);

  $day = date('d', $crunt_date);

  $month = date('M', $crunt_date);

  $year = date('Y', $crunt_date);

?>

<th class="date <?php echo ($i < 5) ? '' : 'mobile-hide';?> <?php echo ($day_name == 'Sat' || $day_name == 'Sun') ? 'weakend' : '';?>">

<span class="day_name"><?php echo $day_name;?></span>

<span class="day"><?php echo $day;?></span>

<span class="month"><?php echo $month;?></span>

</th>

<?php

}

?>

</tr>

</thead>

<tbody>

<?php 

	$sql_rooms =  "SELECT * FROM bsi_room WHERE hotel_id=".$hotel_id." AND status=1 ORDER BY room_id DESC";

	$query_rooms= $DB->query($sql_rooms);

  $tooltips = array();

	while($sq_rooms=mysqli_fetch_assoc($query_rooms)){

    $dd = $_POST['datetime'];

?>

<tr>

<td class="room_name"><a href="#accomo"><?php echo room_type_title($sq_rooms[roomtype_id]).' '.room_capacity($sq_rooms[capacity_id]);?></a><br><a class="button btn-mini sky-blue1" style="margin-right:5px;" href="#accomo"><?php echo $_LANGUAGE['details']; ?></a><a class="button btn-mini yellow" href="booking/<?php echo $sq_rooms['room_id']."/".date("m/d/Y",$dd); ?>/"><?php echo $_LANGUAGE['book']; ?></a></td>

<td class="rack_rate mobile-hide"><br /><?php echo amount($sq_rooms[room_rate]);?></td>



<?php

for($i=0; $i<7; $i++){

  $crunt_date = $dd + (86400*$i);

  $day_name = date('D', $crunt_date);



if($crunt_date>=strtotime("today")){

  if(!room_available($sq_rooms['room_id'], $crunt_date)){

?>

<td class="rate <?php echo ($i < 5) ? '' : 'mobile-hide';?> <?php echo ($day_name == 'Sat' || $day_name == 'Sun') ? 'weekend' : '';?>">

<span class="unavailable"><?php echo $_LANGUAGE['sold']; ?></span>

</td>

<?php

  } else {



    $sql_query = $DB->query('select ' . strtolower($day_name) .' AS price from bsi_priceplan where status=1 and ("'.date("Y-m-d",$crunt_date).'" between start_date and end_date) and room_id ='.$sq_rooms['room_id']." order by plan_id desc limit 1");

    $c = $DB->numRows($sql_query);

    if($c==0){

    $sql_query2 = $DB->query('select ' . strtolower($day_name) .' AS price from bsi_priceplan where status=1 and start_date="0000-00-00" and room_id ='.$sq_rooms['room_id']." order by plan_id desc limit 1");

    $d = $DB->numRows($sql_query2);

    if($d==0){

      $price = $sq_rooms[room_rate];

    } else { $price_result = mysqli_fetch_array($sql_query2); $price=$price_result['price']; }

    } else { $price_result = mysqli_fetch_array($sql_query); $price=$price_result['price']; }



?>

<td data-id="<?php echo $sq_rooms['room_id'].$crunt_date; ?>" class="rate special <?php echo ($i < 5) ? '' : 'mobile-hide';?> <?php echo ($day_name == 'Sat' || $day_name == 'Sun') ? 'weakend' : '';?> <?php //echo ($i == 0) ? 'selected' : '';?>">

<div class="hot"><?php echo $_LANGUAGE['hot']; ?>!</div>

<span style="font-size: 9px;white-space: nowrap;"><a href="booking/<?php echo $sq_rooms['room_id']."/".date("m/d/Y",$crunt_date); ?>/"><?php echo amount($price, $sq_rooms['room_id']);?></a></span>

</td>

<?php

      $tooltips[$sq_rooms['room_id'].$crunt_date] = calendarJsonTooltip($sq_rooms['room_id'], $crunt_date);

  } } 

  else {  //date passed ?>

      <td class="rate <?php echo ($i < 5) ? '' : 'mobile-hide';?> <?php echo ($day_name == 'Sat' || $day_name == 'Sun') ? 'weekend' : '';?>">

<span class="unavailable"><?php echo $_LANGUAGE['sold']; ?></span>

</td>

<?php }

}

?>

</tr>

<?php } ?>

</tbody>

</table>                                  

<script type="text/javascript">

  var tooltips_vals = <?php echo json_encode($tooltips); ?>

</script>                     