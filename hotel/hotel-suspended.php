<?php require_once("includes/initialize.php");
require("header.php");
?>
       <section id="content">
            <div class="container">
                <div class="row">
                  
                  <div id="main" class="col-sm-12 col-md-12">
                        <div class="booking-information travelo-box">
                            <h2>Hotel Suspended</h2>
                            <hr>
                            <div class="booking-confirmation clearfix">
                                <i class="soap-icon-playplace icon circle"></i>
                                <div class="message">
                                    <h2 class="main-message">Selected Hotel has been suspended.<br><small>You can't book room from this hotel.</small></h2>
                                </div>
                               
                            </div>
                            <hr>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </div>
   <?php require("footer.php"); ?>
   
</body>
</html>