<?php
	require_once("includes/initialize.php");
	require_once("includes/ipay88.php");

	$id = base64_decode($_GET['bi']) - 678;

	// Booking
	$bq = $DB->query("SELECT * FROM bsi_bookings WHERE booking_id = '{$id}' LIMIT 1");
	$bo = $DB->fetchObject($bq);
	// var_dump($bo);

	if (is_null($bo)) {
		// Redirect
	}

	switch ($bo->payment_type) {
		case 'mc':
			$pid = 4;
			break;
		case 'xt':
			$pid = 7;
			break;
		default:
			$pid = 1;
			break;
	}

	// Hotel
	$hq = $DB->query("SELECT * FROM bsi_hotels WHERE hotel_id = '{$bo->hotel_id}' LIMIT 1");
	$ho = $DB->fetchObject($hq);

	// Room
	$rq = $DB->query("SELECT RT.type_name, C.title FROM bsi_room R, bsi_roomtype RT, bsi_capacity C WHERE R.hotel_id = '{$ho->hotel_id}' AND R.roomtype_id = RT.roomtype_id AND R.capacity_id = C.id LIMIT 1");
	$ro = $DB->fetchObject($rq);

	$bt = floor((strtotime($bo->end_date) - strtotime($bo->start_date))/(60*60*24));
	$bts = $bt . ' day' . ($bt > 1 ? 's' : '');
	$prodDesc = $ho->hotel_name . ' : ' . $ro->title . ' ' . $ro->type_name . ' (' . $bts . ')';
	// var_dump($prodDesc);

	// User
	$uq = $DB->query("SELECT * FROM users WHERE user_id = '{$bo->user_id}' LIMIT 1");
	$uo = $DB->fetchObject($uq);

	// var_dump($uo);

	if (is_null($uo)) {
		// Redirect
	}

	// Gateway
	$pgq = $DB->query("SELECT account FROM bsi_payment_gateway WHERE hotel_id = '{$ho->hotel_id}' LIMIT 1");
	$pgo = $DB->fetchObject($pgq);

	if (is_null($pgo)) {
		// Redirect
	}

	$ipo = explode('||', $pgo->account);
	// var_dump($ipo);

	$merchantKey = $ipo[1];
	$merchantCode = $ipo[0];
	$refNo = $bo->booking_id;
	$amount = str_replace('.', '', $bo->total_cost) . '00';
	$currency = 'IDR';

	$src = $merchantKey.$merchantCode.$refNo.$amount.$currency;
	$signature = $iPay->iPay88_signature($src);
	$username = $uo->title . ' ' . $uo->first_name . ' ' . $uo->last_name;

	// var_dump($signature);

?>

<html>
	<body>
		<form method="post" name="ePayment" action="https://sandbox.ipay88.co.id/epayment/entry.asp">
			<input type="hidden" name="MerchantCode" value="<?php echo $merchantCode; ?>">
	        <input type="hidden" name="PaymentId" value="<?php echo $pid; ?>">
	        <input type="hidden" name="RefNo" value="<?php echo $refNo; ?>">
	        <input type="hidden" name="Amount" value="<?php echo $amount; ?>">
	        <input type="hidden" name="Currency" value="<?php echo $currency; ?>">
	        <input type="hidden" name="ProdDesc" value="<?php echo $prodDesc; ?>">
	        <input type="hidden" name="UserName" value="<?php echo $username; ?>">
	        <input type="hidden" name="UserEmail" value="<?php echo $uo->email; ?>">
	        <input type="hidden" name="UserContact" value="<?php echo $uo->phone; ?>">
	        <input type="hidden" name="Remark" value="">
	        <input type="hidden" name="Lang" value="UTF-8">
	        <input type="hidden" name="Signature" value="<?php echo $signature; ?>">
	        <input type="hidden" name="ResponseUrl" value="<?php echo BASEURL . 'payment_response.php'; ?>">
	        <input type="submit" value="Proceed with Payment" name="Submit">
		</form>
	</body>

	<script language="JavaScript">
		document.ePayment.submit();
	</script>
</html>