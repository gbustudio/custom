<?php require_once("includes/initialize.php");
$datetime = '';
$room_id = '1';
$room = get_room($room_id);
$hotel = hotel_details($room->hotel_id);
if(!validateDate($datetime) or strtotime($datetime)<time()){ $datetime=date("m/d/Y"); }

?>
<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Booking Venture | Book your Trip today </title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Booking Venture | Book your Trip today ">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Theme Styles -->
    <!-- Main Style -->

</head>
<body style="">
	<link href="http://code.jquery.com/ui/1.10.4/themes/ui-darkness/jquery-ui.css" rel="stylesheet">
	<style>
		.ui-datepicker-trigger
		{
			padding:0px;
			padding-left:5px;
			vertical-align:baseline;

			position:relative;
			top:4px;
			height:18px;
		}
	</style>
                                <form target="_blank" action="http://localhost/custom/hotel/search/1-your_hotel_and_resorts/" method="post" id="reservation_form_top">
                                <div class="tab-pane active" id="hotel-availability" style="padding: 0px !important;">
                                    <div class="update-search clearfix" style="background-color: #dedede !important;border: 15px solid #dedede !important;">
                                        
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Hotel</label>
													<div class="">
														<select name="sizes" class="full-width" id="select_hotel">
														   <?php
																$q = $DB->query("select permalink,hotel_name from bsi_hotels where status=1"); // and paid_upto>NOW()
																while($r=$DB->fetchObject($q)){
																	echo "<option value='".$r->permalink."'>".$r->hotel_name."</option>";
																}
														   ?>
														</select>
													</div>
												</div>
                                            </div>
                                        </div>
										<div class="col-md-12">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label><?php echo $_LANGUAGE['check_in']; ?></label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="mm/dd/yy" class="input-text full-width" id="check_in_date" name="check_in_date" onChange="do_action()" value="<?php echo $datetime; ?>" min="<?php echo date('m/d/Y');?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <label><?php echo $_LANGUAGE['check_out']; ?></label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="mm/dd/yy" class="input-text full-width" id="check_out_date" name="check_out_date" value="<?php $next=strtotime($datetime)+86400; echo date("m/d/Y", $next); ?>" onChange="do_action()" min="<?php echo date('m/d/Y');?>" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
	
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>Person</label>
                                                    <div class="">
                                                        <select class="full-width" id="person" name="person">
                                                            <option value="1" selected="selected">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                            <option value="5">05</option>
                                                            <option value="6">06</option>
                                                            <option value="7">07</option>
                                                            <option value="8">08</option>
                                                            <option value="9">09</option>
                                                            <option value="10">10</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <h4 class="visible-md visible-lg">&nbsp;</h4>
                                            <label class="visible-md visible-lg">&nbsp;</label>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <button class="full-width" type="submit"><?php echo $_LANGUAGE['search_now']; ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
								
								
	
	<script type="text/javascript">
		document.getElementById('select_hotel').onchange = function(){
			document.getElementById('reservation_form_top').action = 'http://localhost/custom/hotel/search/'+this.value+'/';
		}
	</script>

			
 <!-- Javascript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://localhost/custom/hotel/js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="http://localhost/custom/hotel/js/jquery-ui.1.10.4.min.js"></script>



	<script type="text/javascript">
		function changeTraveloElementUI() {

			// datepicker
			tjq('.datepicker-wrap input').each(function() {
				var minDate = tjq(this).data("min-date");
				if (typeof minDate == "undefined") {
					minDate = 0;
				}
				tjq(this).datepicker({
					showOn: 'both',
					buttonText: '',
					buttonImageOnly: true,
					changeYear: false,
					/*showOtherMonths: true,*/
					minDate: minDate,
					dateFormat: "mm/dd/yy",
					dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
					beforeShow: function(input, inst) {
						var themeClass = tjq(input).parent().attr("class").replace("datepicker-wrap", "");
						tjq('#ui-datepicker-div').attr("class", "");
						tjq('#ui-datepicker-div').addClass("ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all");
						tjq('#ui-datepicker-div').addClass(themeClass);
					}
				});
			});
			
			// placeholder for ie8, 9
			try {
				tjq('input, textarea').placeholder();
			} catch (e) {}
		}

		tjq(document).ready(function() {
			changeTraveloElementUI();
		});



	</script>
	<script type="text/javascript">
		function do_action(){
		  var num_rooms = tjq('#num_rooms').val();
		  var check_in_date = tjq('#check_in_date').val();
		  var check_out_date = tjq('#check_out_date').val();
		  if(check_in_date!=""){
			tjq('#check_out_date').attr('min', check_in_date);
			if(Date.parse(check_in_date)>=Date.parse(check_out_date)){
			  var t = new Date(new Date(check_in_date).getTime() + 86400000);
			  var ne = ("0" + (t.getMonth()+1)).slice(-2)+"/"+("0" + t.getDate()).slice(-2)+"/"+t.getFullYear();
			  //var ne = ("0" + t.getDate()).slice(-2)+"/"+("0" + (t.getMonth()+1)).slice(-2)+"/"+t.getFullYear();
			  tjq('#check_out_date').val(ne);
			  check_out_date = ne;
			}
		  }
		}
	</script>
</body>
</html>
