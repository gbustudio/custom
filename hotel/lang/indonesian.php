<?php
$_LANGUAGE['login']  = 'Login';
$_LANGUAGE['signup']  = 'Daftar';
$_LANGUAGE['logout']  = 'Keluar';
$_LANGUAGE['my_account']  = 'Akun Saya';
$_LANGUAGE['enter_promotional_code']  = 'Masukkan kode promosi';
$_LANGUAGE['apply']  = 'Terapkan';
$_LANGUAGE['back_1_week']  = 'Kembali 1 Minggu';
$_LANGUAGE['back_1_day']  = 'Kembali 1 Hari';
$_LANGUAGE['forward_1_day']  = 'Teruskan 1 Hari';
$_LANGUAGE['forward_1_week']  = 'Maju 1 Minggu';
$_LANGUAGE['room_selection']  = 'Ruang yang dipilih';
$_LANGUAGE['full_rate']  = 'Harga Penuh';
$_LANGUAGE['details']  = 'Rincian';
$_LANGUAGE['book']  = 'buku';
$_LANGUAGE['hot']  = 'Hot';
$_LANGUAGE['photos']  = 'Foto-foto';
$_LANGUAGE['map']  = 'Peta';
$_LANGUAGE['street_view']  = 'Street View';
$_LANGUAGE['description']  = 'Deskripsi';
$_LANGUAGE['amenities']  = 'Fasilitas';
$_LANGUAGE['terms_conditions']  = 'Kebijakan';
$_LANGUAGE['write_review']  = 'Tulis Ulasan';
$_LANGUAGE['hotel_type']  = 'Tipe Hotel';
$_LANGUAGE['extra_people']  = 'Tambahan Orang';
$_LANGUAGE['minimum_stay']  = 'Minimal menginap';
$_LANGUAGE['security_deposit']  = 'Security Deposit';
$_LANGUAGE['country']  = 'Negara';
$_LANGUAGE['city']  = 'Kota';
$_LANGUAGE['neighborhood']  = 'Lingkungan';
$_LANGUAGE['cancellation']  = 'pembatalan';
$_LANGUAGE['about']  = 'tentang';
$_LANGUAGE['accomodation_details']  = 'Detail Akomodasi';
$_LANGUAGE['room']  = 'Ruang';
$_LANGUAGE['per']  = 'per';
$_LANGUAGE['book_now']  = 'Pesan Sekarang';
$_LANGUAGE['wi_fi']  = 'Wi-Fi';
$_LANGUAGE['coffee']  = 'Kopi';
$_LANGUAGE['fridge']  = 'Kulkas';
$_LANGUAGE['entertainment']  = 'Hiburan';
$_LANGUAGE['room_service']  = 'Layanan Kamar';
$_LANGUAGE['complimentary_breakfast']  = 'Sarapan gratis';
$_LANGUAGE['fire_place']  = 'Fire Place';
$_LANGUAGE['hot_tub']  = 'Hot Tub';
$_LANGUAGE['swimming_pool']  = 'Kolam Renang';
$_LANGUAGE['air_conditioning']  = 'AC';
$_LANGUAGE['wine_bar']  = 'Wine Bar';
$_LANGUAGE['secure_vault']  = 'Brangkas';
$_LANGUAGE['pets_allowed']  = 'Hewan Peliharaan';
$_LANGUAGE['free_parking']  = 'Parkir Gratis';
$_LANGUAGE['handicap_accessible']  = 'Dapat diakses Difabel';
$_LANGUAGE['elevator_in_building']  = 'Lift di Gedung';
$_LANGUAGE['television']  = 'Televisi';
$_LANGUAGE['fitness_facility']  = 'Fasilitas Kebugaran';
$_LANGUAGE['smoking_allowed']  = 'Merokok Diizinkan';
$_LANGUAGE['no-smoking_allowed']  = 'No Smoking Diizinkan';
$_LANGUAGE['pick_and_drop']  = 'Antar Jemput';
$_LANGUAGE['play_place']  = 'Tempat Bermain';
$_LANGUAGE['conference_room']  = 'Ruang Konferensi';
$_LANGUAGE['doorman']  = 'Penjaga Pintu';
$_LANGUAGE['suitable_for_events']  = 'Cocok untuk Acara';
$_LANGUAGE['night']  = 'malam';
$_LANGUAGE['visit_main_website']  = 'Kunjungi Website Utama';
$_LANGUAGE['need']  = 'Butuh';
$_LANGUAGE['help']  = 'Bantuan';
$_LANGUAGE['why_book_with_us']  = 'Mengapa kami?';
$_LANGUAGE['hotels']  = 'hotel';
$_LANGUAGE['register']  = 'Daftar';
$_LANGUAGE['title']  = 'judul';
$_LANGUAGE['first_name']  = 'nama kecil';
$_LANGUAGE['last_name']  = 'nama keluarga';
$_LANGUAGE['street_address']  = 'Street Address';
$_LANGUAGE['zipcode']  = 'Kode Pos';
$_LANGUAGE['state']  = 'negara';
$_LANGUAGE['select_country']  = 'Pilih Negara';
$_LANGUAGE['phone']  = 'telepon';
$_LANGUAGE['fax']  = 'fax';
$_LANGUAGE['identity_type']  = 'Jenis identitas';
$_LANGUAGE['identity_number']  = 'Nomor identitas';
$_LANGUAGE['email_address']  = 'Alamat email';
$_LANGUAGE['password']  = 'kata sandi';
$_LANGUAGE['alread_member']  = 'Sudah Anggota';
$_LANGUAGE['remember_me']  = 'Remember Me';
$_LANGUAGE['dont_have_account']  = 'Jangan memiliki account';
$_LANGUAGE['your_overall_rating']  = 'Anda atas semua Peringkat properti ini';
$_LANGUAGE['very_good']  = 'sangat bagus';
$_LANGUAGE['service']  = 'layanan';
$_LANGUAGE['value']  = 'nilai';
$_LANGUAGE['sleep_quality']  = 'Kualitas tidur';
$_LANGUAGE['cleanliness']  = 'kebersihan';
$_LANGUAGE['location']  = 'tempat';
$_LANGUAGE['rooms']  = 'kamar';
$_LANGUAGE['reviews']  = 'ulasan';
$_LANGUAGE['title_of_review']  = 'Judul review Anda';
$_LANGUAGE['enter_review_title']  = 'Masukkan judul reviw';
$_LANGUAGE['your_review']  = 'tinjauan Anda';
$_LANGUAGE['enter_your_review']  = 'Masukkan ulasan Anda (minimal 100 karakter)';
$_LANGUAGE['what_sort_of_trip']  = 'Macam apa perjalanan adalah ini';
$_LANGUAGE['business']  = 'bisnis';
$_LANGUAGE['couples']  = 'Pasangan';
$_LANGUAGE['family']  = 'keluarga';
$_LANGUAGE['friends']  = 'teman';
$_LANGUAGE['solo']  = 'solo';
$_LANGUAGE['when_did_you_travel']  = 'Wen apakah Anda bepergian';
$_LANGUAGE['select_month']  = 'Pilih Bulan';
$_LANGUAGE['add_tip_for_travelers']  = 'Menambahkan tip untuk membantu wisatawan memilih ruang yang baik';
$_LANGUAGE['write_something_here']  = 'Menulis sesuatu di sini';
$_LANGUAGE['do_you_have_photo_to_share']  = 'Apakah Anda memiliki foto untuk berbagi';
$_LANGUAGE['optional']  = 'opsional';
$_LANGUAGE['select_image']  = 'Pilih Foto';
$_LANGUAGE['browse']  = 'melihat-lihat';
$_LANGUAGE['share_with_friends']  = 'Berbagi dengan teman';
$_LANGUAGE['share_your_review_with_friends']  = 'Berbagi review Anda dengan teman Anda di berbagai social jaringan media';
$_LANGUAGE['submit_review']  = 'Kirim Review';
$_LANGUAGE['back_to_reservation_page']  = 'Kembali ke Halaman Reservasi';
$_LANGUAGE['rooms_available']  = 'Kamar yang tersedia';
$_LANGUAGE['when']  = 'Kapan';
$_LANGUAGE['who']  = 'Siapa';
$_LANGUAGE['check_in']  = 'Check-In';
$_LANGUAGE['check_out']  = 'Check Out';
$_LANGUAGE['adults']  = 'Dewasa';
$_LANGUAGE['kids']  = 'Anak-anak';
$_LANGUAGE['search_now']  = 'Cari Sekarang';
$_LANGUAGE['choose_room_occupants']  = 'Rincian Pembayaran';
$_LANGUAGE['occupancy']  = 'Penyewa';
$_LANGUAGE['date']  = 'Tanggal';
$_LANGUAGE['daily_inclusions']  = 'Fasilitas';
$_LANGUAGE['room_rate']  = 'Tingkat Room';
$_LANGUAGE['extra_adult']  = 'Ekstra Dewasa';
$_LANGUAGE['extra_child']  = 'Anak-anak';
$_LANGUAGE['extra_infant']  = 'Bayi';
$_LANGUAGE['total']  = 'Total';
$_LANGUAGE['children_infants_upto']  = 'Anak-anak sampai 12 tahun, bayi sampai 5 tahun';
$_LANGUAGE['extras_available_for_this_room']  = 'Ekstra tersedia untuk kamar ini';
$_LANGUAGE['smoking_room']  = 'Rokok';
$_LANGUAGE['non_smoking_room']  = 'Ruangan Bebas Rokok';
$_LANGUAGE['per_booking']  = 'per pemesanan';
$_LANGUAGE['one_way_airport_transfer_service_pickup']  = 'Salah satu cara layanan transfer bandara (mengambil) per mobil';
$_LANGUAGE['available_starting_from']  = 'Tersedia mulai dari';
$_LANGUAGE['until']  = 'sampai';
$_LANGUAGE['airport_pickup_service']  = 'Layanan Airport Pick Up';
$_LANGUAGE['airport_drop_off_service'] = 'Layanan Drop Off Bandara';
$_LANGUAGE['quantity']  = 'Jumlah';
$_LANGUAGE['please_provide_flight_details']  = 'Harap memberikan rincian penerbangan Anda.';
$_LANGUAGE['one_way_airport_transfer_service_dropoff']  = 'One way airport transfer service (drop off) per car ';
$_LANGUAGE['only']  = 'Hanya';
$_LANGUAGE['return_airport_transfer']  = 'Transfer kembali Bandara';
$_LANGUAGE['totals']  = 'Total';
$_LANGUAGE['room_charges']  = 'Biaya tambahan';
$_LANGUAGE['room_taxes']  = 'Harga Kamar';
$_LANGUAGE['service_charges']  = 'Biaya layanan';
$_LANGUAGE['extras_total']  = 'Jumlah Ekstra';
$_LANGUAGE['grand_total']  = 'Total Semua';
$_LANGUAGE['note_infants_not_included']  = 'Catatan: Bayi tidak termasuk dalam hunian maksimum atau per orang ekstra';
$_LANGUAGE['payment_by']  = 'PEMBAYARAN OLEH';
$_LANGUAGE['message']  = 'Pesan';
$_LANGUAGE['by_continuing_you_agree_terms']  = 'Dengan melanjutkan, Anda setuju dengan';
$_LANGUAGE['your_card_information']  = 'Informasi Kartu Anda';
$_LANGUAGE['credit_card_type']  = 'Type kartu kredit';
$_LANGUAGE['select_a_card']  = 'Pilih Kartu';
$_LANGUAGE['card_holder_name']  = 'Nama Pemegang Kartu';
$_LANGUAGE['card_number']  = 'Nomor Kartu';
$_LANGUAGE['card_identification_number']  = 'Kartu Identification Number';
$_LANGUAGE['expiration_date']  = 'Tanggal Kedaluwarsa';
$_LANGUAGE['month']  = 'Bulan';
$_LANGUAGE['year']  = 'Tahun';
$_LANGUAGE['billing_zipcode']  = 'Kode Pos Tagihan';
$_LANGUAGE['sold'] = 'T+erjual';
?>