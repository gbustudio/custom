<?php require_once("includes/initialize.php");
        require("header.php");
		//var_dump("chanif ".$hotel->hotel_latitude_longitude);
?>
        <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-md-9">
                        <div class="tab-container style1" id="hotel-main-content">
                            <div class="tab-content"> <!-- added from here -->
                                <div class="tab-pane active">
                                <style type="text/css">
                                    table.boocking-check-celander {
                                        border-bottom: solid 1px #e8e8e8;
                                    }
                                    table.boocking-check-celander th.date_navigation,th.date_nav,table.boocking-check-celander table th {
                                        background-color: inherit !important;
                                    }
                                    
                                    table.boocking-check-celander .hot {
                                        color: #f60;
                                    }
                                    table.boocking-check-celander th.weakend {
                                        background-color: #2a2f34;
                                    }
                                    table.boocking-check-celander th {
                                        background-color: #434a50;
                                        height: 60px;
                                        padding: 0 5px;
                                        text-align: left;
                                        vertical-align: middle;
                                        color: #fff;
                                    }
                                    table.boocking-check-celander th.date {
                                        width: 40px;
                                        
                                    }
                                    table.boocking-check-celander th .day_name {
                                        color: #fff;
                                        font-size: 11px;
                                        font-weight: bold;
                                        white-space: nowrap;
                                    }
                                    table.boocking-check-celander th .day {
                                        color: #fff;
                                        font-size: 15px;
                                        margin: -5px 0;
                                    }
                                    table.boocking-check-celander th .month {
                                        color: #a8a8a8;
                                        font-size: 11px;
                                        font-weight: normal;
                                    }
                                </style>
                                <div style="position:relative;">
                                <h2 style="padding:20px 20px 0;"><?php echo $hotel->hotel_name; ?></h2>
                                
                                <hr>
                                </div>
                                <div style="width:100%;min-height:40px;overflow:hidden;">
                                <form class="navbar-form navbar-right" style="margin-right: 0px !important;" action="" method="post">
                                  <div class="form-group">
                                  <span><?php echo $_LANGUAGE['enter_promotional_code']; ?></span>
                                    <input type="text" name="coupon" class="form-control" value="<?=isset($_SESSION['coupon_added'])? $_SESSION['coupon_added'] : ''?>" placeholder="EarlyBirdPromo" />
                                  </div>
                                  <button type="submit" name="add_coupon" class="btn btn-default"><?php echo $_LANGUAGE['apply']; ?></button>
                                </form>
                                </div>
                                <div id="availablity-celander"></div>
                                </div>
                            </div>
                            <ul class="tabs">
                                <li class="active"><a data-toggle="tab" href="#photos-tab"><?php echo $_LANGUAGE['photos']; ?></a></li>
                                <?php if($hotel->hotel_latitude_longitude){ ?>
                                <li><a data-toggle="tab" href="#map-tab"><?php echo $_LANGUAGE['map']; ?></a></li>
                                <li><a data-toggle="tab" href="#steet-view-tab"><?php echo $_LANGUAGE['street_view']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <div id="photos-tab" class="tab-pane fade in active">
                                    <?php echo hotel_gallery($hotel->hotel_id); ?>
                                </div>
                                <div id="map-tab" class="tab-pane fade">
                                    
                                </div>
                                <div id="steet-view-tab" class="tab-pane fade" style="height: 500px;">
                                    
                                </div>
                                
                            </div>
                        </div>
                        
                        <div id="hotel-features" class="tab-container">
                            <ul class="tabs">
                                <li class="active"><a href="#hotel-description" data-toggle="tab"><?php echo $_LANGUAGE['description']; ?></a></li>
                                <li><a href="#hotel-amenities" data-toggle="tab"><?php echo $_LANGUAGE['amenities']; ?></a></li>
                                <li><a href="#hotel-terms" data-toggle="tab"><?php echo $_LANGUAGE['terms_conditions']; ?></a></li>
                                
                                <?php if(logged()){ ?>
                                <li><a href="#hotel-write-review" data-toggle="tab"><?php echo $_LANGUAGE['write_review']; ?></a></li>
                                <?php } else { ?>
                                <li><a href="#au-signup" class="soap-popupbox"><?php echo $_LANGUAGE['write_review']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="hotel-description">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="col-sm-5 col-lg-4 features table-cell">
                                            <ul>
                                                <li><label><?php echo $_LANGUAGE['hotel_type']; ?>:</label><?php echo $hotel->hotel_info_type; ?></li>
                                                <li><label><?php echo $_LANGUAGE['extra_people']; ?>:</label><?php echo $hotel->hotel_info_extra_people; ?></li>
                                                <li><label><?php echo $_LANGUAGE['minimum_stay']; ?>:</label><?php echo $hotel->hotel_info_min_stay; ?></li>
                                                <li><label><?php echo $_LANGUAGE['security_deposit']; ?>:</label><?php echo amount($hotel->hotel_info_security_deposit); ?></li>
                                                <li><label><?php echo $_LANGUAGE['country']; ?>:</label><?php echo country($hotel->hotel_info_country_id); ?></li>
                                                <li><label><?php echo $_LANGUAGE['city']; ?>:</label><?php echo $hotel->hotel_info_city; ?></li>
                                                <li><label><?php echo $_LANGUAGE['cancellation']; ?>:</label><?php echo $hotel->hotel_info_cancellation; ?></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-7 col-lg-8 table-cell testimonials">
                                            <div class="testimonial style1">
                                                <ul class="slides ">
                                                    <?php echo last_reviews($hotel->hotel_id); ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($hotel->hotel_full_description){ ?>
                                    <div class="long-description">
                                        <h2><?php echo $_LANGUAGE['about']; ?> <?php echo $hotel->hotel_name; ?></h2>
                                        <p><?php echo $hotel->hotel_full_description; ?></p>
                                    </div>
                                    <?php } ?>
                                </div>
                               
                                <div class="tab-pane fade" id="hotel-amenities">
                                    <h2><?php echo $_LANGUAGE['amenities']; ?></h2>
                                    
                                    <p><?php echo $hotel->hotel_amenities_description; ?></p>
                                    <ul class="amenities clearfix style1">
                                        <?php echo amenities($hotel->hotel_amenities); ?>
                                    </ul>
                                    
                                </div>
                                <div class="tab-pane fade" id="hotel-terms">
                                    <?php echo $hotel->hotel_terms; ?>
                                </div>
                                
                                
                                <div class="tab-pane fade" id="hotel-write-review">
                                    <div class="main-rating table-wrapper full-width hidden-table-sms intro">
                                        <article class="image-box box hotel listing-style1 photo table-cell col-sm-4">
                                            <figure>
                                                <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="<?php echo imageCheck($hotel->hotel_logo, "src", "logo"); ?>"></a>
                                            </figure>
                                            <div class="details">
                                                <h4 class="box-title"><?php echo $hotel->hotel_name; ?><small><i class="soap-icon-departure"></i> <?php echo $hotel->hotel_info_city.", ".country($hotel->hotel_info_country_id); ?></small></h4>
                                                <div class="feedback">
                                                    <div title="<?php echo hotel_reviews($hotel->hotel_id); ?>%" class="five-stars-container"><span class="five-stars" style="width: <?php echo hotel_reviews($hotel->hotel_id); ?>%;"></span></div>
                                                    <span class="review"><?php echo hotel_reviews($hotel->hotel_id,"count"); ?> <?php echo $_LANGUAGE['reviews']; ?></span>
                                                </div>
                                            </div>
                                        </article>
                                    
                                        <div class="table-cell col-sm-8">
                                            <div class="overall-rating">
                                                <h4><?php echo $_LANGUAGE['your_overall_rating']; ?></h4>
                                                <div class="star-rating clearfix">
                                                    <div class="five-stars-container"><div class="five-stars" style="width: 80%;"></div></div>
                                                    <span class="status"><?php echo $_LANGUAGE['very_good']; ?></span>
                                                </div>
                                                <div class="detailed-rating">
                                                    <ul class="clearfix">
                                                        <li class="col-md-6"><div class="each-rating"><label><?php echo $_LANGUAGE['service']; ?></label><div class="five-stars-container editable-rating service_rate" data-original-stars="4"></div></div></li>
                                                        <li class="col-md-6"><div class="each-rating"><label><?php echo $_LANGUAGE['value']; ?></label><div class="five-stars-container editable-rating value_rate" data-original-stars="4"></div></div></li>
                                                        <li class="col-md-6"><div class="each-rating"><label><?php echo $_LANGUAGE['sleep_quality']; ?></label><div class="five-stars-container editable-rating sleep_rate" data-original-stars="4"></div></div></li>
                                                        <li class="col-md-6"><div class="each-rating"><label><?php echo $_LANGUAGE['cleanliness']; ?></label><div class="five-stars-container editable-rating clean_rate" data-original-stars="4"></div></div></li>
                                                        <li class="col-md-6"><div class="each-rating"><label><?php echo $_LANGUAGE['location']; ?></label><div class="five-stars-container editable-rating location_rate" data-original-stars="4"></div></div></li>
                                                        <li class="col-md-6"><div class="each-rating"><label><?php echo $_LANGUAGE['rooms']; ?></label><div class="five-stars-container editable-rating rooms_rate" data-original-stars="4"></div></div></li>
                                                        <li class="col-md-6"><div class="each-rating"><label><?php echo $_LANGUAGE['swimming_pool']; ?></label><div class="five-stars-container editable-rating pool_rate" data-original-stars="4"></div></div></li>
                                                        <li class="col-md-6"><div class="each-rating"><label><?php echo $_LANGUAGE['fitness_facility']; ?></label><div class="five-stars-container editable-rating fitness_rate" data-original-stars="4"></div></div></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <form class="review-form" action="#hotel-write-review" method="post">
                                        <input type="hidden" name="service_rate" class="upvote" value="80" />
                                        <input type="hidden" name="value_rate" class="upvote" value="80" />
                                        <input type="hidden" name="sleep_rate" class="upvote" value="80" />
                                        <input type="hidden" name="clean_rate" class="upvote" value="80" />
                                        <input type="hidden" name="location_rate" class="upvote" value="80" />
                                        <input type="hidden" name="rooms_rate" class="upvote" value="80" />
                                        <input type="hidden" name="pool_rate" class="upvote" value="80" />
                                        <input type="hidden" name="fitness_rate" class="upvote" value="80" />
                                        <input type="hidden" name="hotelid" value="<?php echo $hotel->hotel_id; ?>" />
                                        <input type="hidden" name="rate_all" value="80" />
                                        <input type="hidden" name="trip_type" value="" />
                                        <div class="form-group col-md-5 no-float no-padding">
                                            <h4 class="title"><?php echo $_LANGUAGE['title_of_review']; ?></h4>
                                            <input type="text" required pattern=".{3,200}" name="review_title" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['enter_review_title']; ?>" />
                                        </div>
                                        <div class="form-group">
                                            <h4 class="title"><?php echo $_LANGUAGE['your_review']; ?></h4>
                                            <textarea class="input-text full-width" required name="review_text" pattern=".{100,1000}" placeholder="<?php echo $_LANGUAGE['enter_your_review']; ?>" rows="5"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <h4 class="title"><?php echo $_LANGUAGE['what_sort_of_trip']; ?>?</h4>
                                            <ul class="sort-trip clearfix">
                                                <li id="trip_business" class="trip_type"><a href="#"><i class="soap-icon-businessbag circle"></i></a><span><?php echo $_LANGUAGE['business']; ?></span></li>
                                                <li id="trip_couples" class="trip_type"><a href="#"><i class="soap-icon-couples circle"></i></a><span><?php echo $_LANGUAGE['couples']; ?></span></li>
                                                <li id="trip_family" class="trip_type"><a href="#"><i class="soap-icon-family circle"></i></a><span><?php echo $_LANGUAGE['family']; ?></span></li>
                                                <li id="trip_friends" class="trip_type"><a href="#"><i class="soap-icon-friends circle"></i></a><span><?php echo $_LANGUAGE['friends']; ?></span></li>
                                                <li id="trip_solo" class="trip_type"><a href="#"><i class="soap-icon-user circle"></i></a><span><?php echo $_LANGUAGE['solo']; ?></span></li>
                                            </ul>
                                        </div>
                                        <div class="form-group col-md-5 no-float no-padding">
                                            <h4 class="title"><?php echo $_LANGUAGE['when_did_you_travel']; ?>?</h4>
                                            <div class="selector">
                                                <select name="travel_month" class="full-width">
                                                    <option value=""><?php echo $_LANGUAGE['select_month']; ?></option>
                                                <?php 
                                                    for ($i = 6; $i >= 1; $i--) {
                                                        echo '<option value="'.date('Y-m', strtotime("-$i month", strtotime('first day this month'))).'">'.date('F Y', strtotime("-$i month", strtotime('first day this month'))).'</option>';
                                                    }
                                                 ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <h4 class="title"><?php echo $_LANGUAGE['add_tip_for_travelers']; ?></h4>
                                            <textarea class="input-text full-width" name="tip" pattern=".{,500}" rows="3" placeholder="<?php echo $_LANGUAGE['write_something_here']; ?>"></textarea>
                                        </div>
                                        <div class="form-group col-md-5 no-float no-padding">
                                            <h4 class="title"><?php echo $_LANGUAGE['do_you_have_photo_to_share']; ?>? <small>(<?php echo $_LANGUAGE['optional']; ?>)</small> </h4>
                                            <div class="fileinput full-width">
                                                <input type="file" name="review_pics[]" multiple class="input-text" data-placeholder="<?php echo $_LANGUAGE['select_image']; ?>" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <h4 class="title"><?php echo $_LANGUAGE['share_with_friends']; ?> <small>(<?php echo $_LANGUAGE['optional']; ?>)</small></h4>
                                            <p><?php echo $_LANGUAGE['share_your_review_with_friends']; ?></p>
                                            <ul class="social-icons icon-circle clearfix">
                                                <li class="twitter"><a title="Twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                                <li class="facebook"><a title="Facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                                <li class="googleplus"><a title="GooglePlus" href="#" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                                <li class="pinterest"><a title="Pinterest" href="#" data-toggle="tooltip"><i class="soap-icon-pinterest"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="form-group col-md-5 no-float no-padding no-margin">
                                            <button type="submit" name="submit_review" class="btn-large full-width"><?php echo $_LANGUAGE['submit_review']; ?></button>
                                        </div>
                                    </form>
                                    
                                </div>
                            </div>
                        <?php if(hotel_rooms_details($hotel->hotel_id)){ ?>
                            <br><br>
                            <h2 id="accomo"><?php echo $_LANGUAGE['accomodation_details']; ?></h2>
                            <hr style="background:#ccc;height:1px;" />
                            <div class="room-list listing-style3 hotel">  
                            <?php echo hotel_rooms_details($hotel->hotel_id); ?>
                                <!-- <a href="#" class="load-more button full-width btn-large fourty-space">LOAD MORE ROOMS</a> -->
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                    <?php require("sidebar.php"); ?>
            </div>
        </section>
        
        
    </div>
<?php if(!logged()){ ?>
<!-- registration html start -->
<div id="au-signup" style="background:#fefefe;padding:20px;display:none;" class="col-sm-offset-2 col-md-offset-2 col-sm-8 col-md-8">
    <form id="au_signup_form" style="background:#fefefe;padding:20px;">
      <div class="person-information">
          <h2><?php echo $_LANGUAGE['register']; ?></h2>
          <div class="form-group row">
              <div class="col-sm-12 col-md-12">
                  <label><?php echo $_LANGUAGE['title']; ?></label>
                  <div class="selector">
                      <select name="title" class="full-width" id="title">
                        <option value="Mr.">Mr.</option>
                        <option value="Ms.">Ms.</option>
                        <option value="Mrs.">Mrs.</option>
                        <option value="Miss.">Miss.</option>
                        <option value="Dr.">Dr.</option>
                        <option value="Prof.">Prof.</option>
                      </select>
                  </div>
              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['first_name']; ?></label>
                  <input type="text" name="fname" required id="fname" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['first_name']; ?>" />
              </div>
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['last_name']; ?></label>
                  <input type="text" name="lname" required id="lname" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['last_name']; ?>" />
              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-12">
                  <label><?php echo $_LANGUAGE['street_address']; ?></label>
                  <input type="text" name="str_addr" required id="str_addr" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['street_address']; ?>" />
              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-6">
                <label><?php echo $_LANGUAGE['city']; ?> &AMP; <?php echo $_LANGUAGE['zipcode']; ?></label>
                  <div class="constant-column-2">
                      <div class="selector">
                          <input type="text" required name="city"  id="city" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['city']; ?>" />
                      </div>
                      <div class="selector">
                          <input type="text" name="zipcode"  id="zipcode" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['zipcode']; ?>" />
                      </div>
                  </div>
              </div>
              <div class="col-sm-6 col-md-6">
                <label><?php echo $_LANGUAGE['state']; ?> &AMP; <?php echo $_LANGUAGE['country']; ?></label>
                  <div class="constant-column-2">
                      <div class="selector">
                          <input type="text" required name="state"  id="state" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['country']; ?>" />
                      </div>
                      <div class="selector">
                          <select required name="country" class="full-width">
                            <option value=""><?php echo $_LANGUAGE['select_country']; ?></option>
                            <?php echo countries_list(); ?>
                          </select>
                      </div>
                  </div>
              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['phone']; ?></label>
                  <input type="text" required name="phone" id="phone" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['phone']; ?>" />
              </div>
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['fax']; ?></label>
                  <input type="text" name="fax" id="fax" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['fax']; ?>" />
              </div>
          </div>
      </div>
     <!--  <hr /> -->
      <div class="extra-information">
          <!-- <h2>Extra Details</h2> -->
          <div class="form-group row">
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['identity_type']; ?></label>
                  <input name="id_type" id="id_type" required type="text" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['identity_type']; ?>" />
              </div>
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['identity_number']; ?></label>
                  <input type="text" name="id_number" required id="id_number" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['identity_number']; ?>" />
              </div>
          </div>
          <div class="form-group row">
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['email_address']; ?></label>
                  <input type="email" name="email" required id="email" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['email_address']; ?>" />
              </div>
              <div class="col-sm-6 col-md-6">
                  <label><?php echo $_LANGUAGE['password']; ?></label>
                  <input type="password" required name="password" id="password" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['password']; ?>" />
              </div>
          </div>
      </div>
      <div class="form-group row">
          <div class="col-sm-10 col-md-12">
              <button id="btn_exisitng_cust" type="submit" class="full-width btn-large"><?php echo $_LANGUAGE['signup']; ?></button>
          </div>
      </div>
      <p><?php echo $_LANGUAGE['alread_member']; ?>? &nbsp; &nbsp; <a href="#au-login" class="goto-login soap-popupbox"><?php echo $_LANGUAGE['login']; ?></a></p>
    </form>
</div> <!-- register end  -->
<div id="au-login" class="travelo-login-box travelo-box">
    <form id="au_login_form"  style="background:#fefefe;padding:20px;">
        <div class="form-group">
            <input type="email" required name="email" class="input-text full-width" placeholder="<?php echo $_LANGUAGE['email_address']; ?>">
        </div>
        <div class="form-group">
            <input type="password" required name="pass" class="input-text full-width" placeholder="<?php echo $_LANGUAGE['password']; ?>">
        </div>
        <div class="form-group">
            <!-- <a href="#" class="forgot-password pull-right">Forgot password?</a> -->
            <div class="checkbox checkbox-inline">
                <label>
                    <input name="remember" type="checkbox"> <?php echo $_LANGUAGE['remember_me']; ?>
                </label>
            </div>
        </div>
        <button type="submit" class="full-width btn-medium"><?php echo $_LANGUAGE['login']; ?></button>
    </form>
    <div class="seperator"></div>
    <p><?php echo $_LANGUAGE['dont_have_account']; ?>? <a href="#au-signup" class="goto-signup soap-popupbox"><?php echo $_LANGUAGE['signup']; ?></a></p>
</div>
<!-- login html end -->
<?php } ?>
<div id="au_tooltips" style="position:absolute;border-radius:4px;border:2px solid red;width:330px;background:#000;color:white;display:none;">
 <p class="col-md-12 hot-deal-tooltip" style="background:#ff6106;"><em><b>Hot Deal!</b></em></p>
 <table class="table"><tr><td style="border-top:0px;padding:0px;padding-left:8px;">Minimum Stay</td>
 <td style="border-top:0px;padding:0px;padding-left:8px;" class="min-stay-tooltip"></td></tr>
<tr><td style="border-top:0px;padding:0px;padding-left:8px;">Availability</td>
   <td style="border-top:0px;padding:0px;padding-left:8px;" class="availability-tooltip"></td></tr>
<tr><td style="border-top:0px;padding:0px;padding-left:8px;">Included Occupancy</td>
   <td style="border-top:0px;padding:0px;padding-left:8px;" class="inc-occu-tooltip"></td></tr>
<tr><td style="border-top:0px;padding:0px;padding-left:8px;">Maximum Occupancy</td>
   <td style="border-top:0px;padding:0px;padding-left:8px;" class="max-occu-tooltip"></td></tr>
</table>
 <div class="col-md-offset-1" >
 <p class="details-tooltip"></p>
  <p style="font-size:small;">SUBJECT TO SERVICE CHARGE & GOVT TAX</p>
 </div>
</div>
    <?php require("footer.php"); ?>
    <script type="text/javascript">
      embed_availablity_celander('<?php echo time();?>','<?php echo $hotel->hotel_id;?>');
      function embed_availablity_celander(datetime,hotel_id){
        jQuery.post("ajax-celander-hotel.php", {datetime: datetime,hotel_id: hotel_id}, function(data){
          jQuery('#availablity-celander').html(data);});
      }
    </script>
    
</body>
</html>
