<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Booking Venture | Book your Trip today </title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Booking Venture | Book your Trip today ">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="http://localhost/custom/hotel/" />
    <!-- Theme Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css">
    
    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="components/flexslider/flexslider.css" media="screen" />
    
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="css/style.css">
    
    <!-- Custom Styles -->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Updated Styles -->
    <link rel="stylesheet" href="css/updates.css">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="css/responsive.css">
    
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body>
    
    <div id="page-wrapper">
      <header id="header" class="navbar-static-top">
        
        <div class="topnav hidden-xs">
                <div class="container">
                    <ul class="quick-menu pull-left">
                        <?php if(logged()){ ?>
                        <!-- <li><a href="my_bookings.php"><?php //echo $_LANGUAGE['my_account']; ?></a></li> -->
                        <?php } ?>
                        <!-- <li class="ribbon language">
                            <a href="#"><?php //echo user_language($_COOKIE['language'], "name"); ?></a>
                            <ul class="menu mini">
                                <?php //echo languages_list(); ?>
                            </ul>
                        </li> -->
                        <li >
                            <a href="#">HOME</a>
                        </li>
                    </ul>
                    <ul class="quick-menu pull-right">
                        <?php if(!logged()){ ?>
                        <li><a href="#au_login_form" class="soap-popupbox"><?php echo $_LANGUAGE['login']; ?></a></li>
                        <li><a href="#au_signup_form" class="soap-popupbox"><?php echo $_LANGUAGE['signup']; ?></a></li>
                        <?php } else { ?>
                        <li class="ribbon">
                            <a href="#"><?=$_SESSION['user_name']?></a>
                            <ul class="menu mini uppercase left">
                                <li><a href="profile.php" class="location-reload">Profile</a></li>
                                <li><a href="profile.php#booking" class="location-reload">My Bookings</a></li>
                                <li><a href="profile.php#settings" class="location-reload">settings</a></li>
                                <li><a href="#" id="au_logout"><?=$_LANGUAGE['logout']?></a></li>
                            </ul>
                        </li>
                        <?php } ?>
                        <li class="ribbon language">
                            <a href="#"><?php echo user_language($_COOKIE['language'], "name"); ?></a>
                            <ul class="menu mini">
                                <?php echo languages_list(); ?>
                            </ul>
                        </li>
                        <li class="ribbon currency">
                            <a href="#" title=""><?php echo $_COOKIE['currency']; ?></a>
                            <ul class="menu mini left">
                                <?php echo currencies_list(); ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        <style>
          .main-header {
              padding: 0;
              position: relative;
              overflow: hidden;
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover; 
          }
          .main-header img {
              height: auto;
              width: 100%;
              margin: 0 auto; 
              display: block;
          }
          .main-header::before {
              box-shadow: 0 -208px 80px -114px <?=$hotel->theme_color?> inset;
              position: absolute;
              top: 0;
              left: 0;
              width: 100%;
              height: 100%;
              content: "";
          }
          section#content{background:<?=$hotel->theme_color?> !important;}
        </style>
        <div class="main-header">
          <img src="images/hotels/cover/<?=($hotel->hotel_cover_image)?$hotel->hotel_cover_image:"1.jpg"?>" />
        </div>
      </header>