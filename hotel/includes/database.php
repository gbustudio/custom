<?php

class MySQLDatabase {
	private $connection;
	function __construct() {
		$this->connection = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
		$this->connection->query("SET CHARACTER SET 'utf8'");
		$this->connection->query("SET NAMES utf8");
		if (mysqli_connect_errno()){
		  echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}
	public function escapeValue( $value ) {
		return mysqli_real_escape_string($this->connection, $value);
	}
	public function query($sql) {
		return mysqli_query($this->connection, $sql);
	}
	public function fetchObject($resultSet) {
		return mysqli_fetch_object($resultSet);
	}
	public function numRows($resultSet) {
	   return mysqli_num_rows($resultSet);
	}		
}
	$DB = new MySQLDatabase();
?>