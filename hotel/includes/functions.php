<?php session_start();
function logged(){
	if(isset($_SESSION['user_id']) and $_SESSION['user_id']!="" and is_numeric($_SESSION['user_id'])){
		return true;
	} else { return false; }
}
function logout(){
	$_SESSION['user_id']="";
	$_COOKIE['user']="";
	unset($_SESSION['user_id']);
	unset($_COOKIE['user']);
	setcookie("user", "", time() - 3600, "/");
	session_destroy();
	//header("Location: index.html");
}
function hotel_details($hotel_id){
	global $DB;
	return $DB->fetchObject($DB->query("SELECT * FROM bsi_hotels WHERE hotel_id=".$hotel_id." and status=1"));
}
function get_payment_gateway($gateway_code, $hotel_id){
	global $DB;
	return $DB->fetchObject($DB->query("SELECT * FROM bsi_payment_gateway WHERE hotel_id=".$hotel_id." and gateway_code='{$gateway_code}'"));
}
function hotel_exists($hotel_id){
	global $DB;
	$results = $DB->numRows($DB->query("SELECT hotel_id FROM bsi_hotels WHERE hotel_id=".$hotel_id." and status=1"));
	if($results>0){
		return true;
	} else { return false; }
}
function room_exists($room_id){
	global $DB;
	$results = $DB->numRows($DB->query("SELECT room_id FROM bsi_room WHERE room_id=".$room_id." and status=1"));
	if($results>0){
		return true;
	} else { return false; }
}
function country($country_id){
	global $DB;
	$q = $DB->query("SELECT country_name FROM countries WHERE country_id=".$country_id." and status=1");
	$results = $DB->numRows($q);
	if($results>0){
		$country = $DB->fetchObject($q);
		return $country->country_name;
	} else { return ""; }
}
function currency($return="symbol"){
	global $DB;
	$c = $DB->fetchObject($DB->query("SELECT currency_symbl, exchange_rate, currency_code, format FROM bsi_currency WHERE default_c=1 and status=1"));
	if($return=="symbol"){ return $c->currency_symbl; }
	if($return=="rate"){ return $c->exchange_rate; }
	if($return=="code"){ return $c->currency_code; }
	if($return=="format"){ return $c->format; }
}
function hotel_currency($hotel_id){
	global $DB;
	$c = $DB->fetchObject($DB->query("SELECT default_currency FROM bsi_hotels WHERE hotel_id=$hotel_id"));
	return $c->default_currency;
}
function user_currency($code, $return="symbol"){
	global $DB;
	$c = $DB->fetchObject($DB->query("SELECT currency_symbl, exchange_rate, format FROM bsi_currency WHERE currency_code='{$code}' and status=1"));
	if($return=="symbol"){ return $c->currency_symbl; }
	if($return=="rate"){ return $c->exchange_rate; }
	if($return=="format"){ return $c->format; }
}
function format_currency($amount, $spacer){
	$rupiah  = "";
	
	$uang = strval($amount);
	$panjang = Strlen($uang);

	while ($panjang > 3)
	{
		$rupiah = $spacer . substr($uang, -3) . $rupiah;
		$lebar  = strlen($uang)-3;
		$uang   = substr($uang,0,$lebar);
		$panjang= strlen($uang);
	}

	$rupiah = $uang.$rupiah;
	return $rupiah;
}
function currency_exists($code){
	global $DB;
	$c = $DB->numRows($DB->query("SELECT exchange_rate FROM bsi_currency WHERE currency_code='{$code}' and status=1"));
	return ($c>0) ? true : false;
}
function currencies_list(){
	global $DB;
	$list = "";
	$q = $DB->query("SELECT currency_code FROM bsi_currency where status=1");
	while($currency=$DB->fetchObject($q)){
		if($_COOKIE['currency']==$currency->currency_code){$a=" class='active'";}else{$a="";}
		$list .= '<li'.$a.'><a href="#" title="'.$currency->currency_code.'">'.$currency->currency_code.'</a></li>';
	}
	return $list;
}
function language($return="code"){
	global $DB;
	$c = $DB->fetchObject($DB->query("SELECT * FROM bsi_language WHERE lang_default=1 and status=1"));
	if($return=="name"){ return $c->lang_title; }
	if($return=="file"){ return $c->lang_file; }
	if($return=="code"){ return $c->lang_code; }
}
function user_language($code, $return="file"){
	global $DB;
	$c = $DB->fetchObject($DB->query("SELECT * FROM bsi_language WHERE lang_code='{$code}' and status=1"));
	if($return=="name"){ return $c->lang_title; }
	if($return=="file"){ return $c->lang_file; }
}
function language_exists($code){
	global $DB;
	$c = $DB->numRows($DB->query("SELECT lang_code FROM bsi_language WHERE lang_code='{$code}' and status=1"));
	return ($c>0) ? true : false;
}
function languages_list(){
	global $DB;
	$list = "";
	$q = $DB->query("SELECT lang_title, lang_code FROM bsi_language where status=1");
	while($language=$DB->fetchObject($q)){
		if($_COOKIE['language']==$language->lang_code){$a=" class='active'";}else{$a="";}
		$list .= '<li'.$a.'><a href="#" data-code="'.$language->lang_code.'">'.$language->lang_title.'</a></li>';
	}
	return $list;
}
function countries_list(){
	global $DB;
	$list = "";
	$q = $DB->query("SELECT * FROM countries WHERE status=1 ORDER BY country_name ASC");
	while($country=$DB->fetchObject($q)){
		$list .= '<option value="'.$country->country_id.'">'.$country->country_name.'</option>';
	}
	return $list;
}
function getCoupon($coupon_code){
	global $DB;
	$q = $DB->query("SELECT * FROM promotions WHERE coupon_code='".$coupon_code."' and status=1 and CURDATE() between start_date AND end_date");
	$results = $DB->numRows($q);
	if($results>0){
		return $DB->fetchObject($q);;
	} else { return ""; }
}
function couponExists($coupon_code){
	global $DB;
	$q = $DB->query("SELECT promo_id FROM promotions WHERE coupon_code='".$coupon_code."' and status=1 and CURDATE() between start_date AND end_date");
	$results = $DB->numRows($q);
	if($results>0){
		return true;
	} else { return false; }
}
function discount_amount($coupon_code, $dollars){
	$coupon = getCoupon($_SESSION['coupon_added']);
	if($coupon->discount_type==1){	// percentage discount
		return $dollars*($coupon->discount/100);
	} else {
		return $coupon->discount;
	}
}
/*
function amount($dollars, $room_id="au"){	//room_id is for coupon, bcoz coupon would be for specific room
	if(isset($_SESSION['coupon_added']) and $_SESSION['coupon_added']!="" and couponExists($_SESSION['coupon_added'])){
		if($room_id=="au"){
			return user_currency($_COOKIE['currency']).ceil(user_currency($_COOKIE['currency'], "rate")*$dollars);
		} else {
			$coupon = getCoupon($_SESSION['coupon_added']);
			$couponFor = $coupon->room_id;
			if($couponFor==0){
				return user_currency($_COOKIE['currency']).ceil(user_currency($_COOKIE['currency'], "rate")*($dollars-discount_amount($_SESSION['coupon_added'], $dollars)));
			} elseif($couponFor==$room_id){
				return user_currency($_COOKIE['currency']).ceil(user_currency($_COOKIE['currency'], "rate")*($dollars-discount_amount($_SESSION['coupon_added'], $dollars)));
			} else {
				return user_currency($_COOKIE['currency']).ceil(user_currency($_COOKIE['currency'], "rate")*$dollars);
			}
		}
	} else {
		return user_currency($_COOKIE['currency']).ceil(user_currency($_COOKIE['currency'], "rate")*$dollars);
	}
}
*/
function amount($dollars, $room_id="au"){	//room_id is for coupon, bcoz coupon would be for specific room
	if(isset($_SESSION['coupon_added']) and $_SESSION['coupon_added']!="" and couponExists($_SESSION['coupon_added'])){
		if($room_id=="au"){
			return user_currency($_COOKIE['currency']).format_currency(ceil(user_currency($_COOKIE['currency'], "rate")*$dollars), user_currency($_COOKIE['currency'], "format"));
		} else {
			$coupon = getCoupon($_SESSION['coupon_added']);
			$couponFor = $coupon->room_id;
			if($couponFor==0){
				return user_currency($_COOKIE['currency']).format_currency(ceil(user_currency($_COOKIE['currency'], "rate")*($dollars-discount_amount($_SESSION['coupon_added'], $dollars))), user_currency($_COOKIE['currency'], "format"));
			} elseif($couponFor==$room_id){
				return user_currency($_COOKIE['currency']).format_currency(ceil(user_currency($_COOKIE['currency'], "rate")*($dollars-discount_amount($_SESSION['coupon_added'], $dollars))), user_currency($_COOKIE['currency'], "format"));
			} else {
				return user_currency($_COOKIE['currency']).format_currency(ceil(user_currency($_COOKIE['currency'], "rate")*$dollars), user_currency($_COOKIE['currency'], "format"));
			}
		}
	} else {
		return user_currency($_COOKIE['currency']).format_currency(ceil(user_currency($_COOKIE['currency'], "rate")*$dollars), user_currency($_COOKIE['currency'], "format"));
	}
}

function amount_usd($amount){
	return ceil($amount/user_currency($_COOKIE['currency'], "rate"));
}
function avg_hotel_rate($hotel_id){
	global $DB;
	$q = $DB->fetchObject($DB->query("SELECT sum(room_rate) as Total, count(room_rate) as Num FROM bsi_room WHERE hotel_id=".$hotel_id." and status=1"));
	if($q->Num>0){
		return ceil($q->Total/$q->Num);
	} else { return ""; }
}
function room_type_title($roomtype_id){
	global $DB;
	$q = $DB->query("SELECT type_name FROM bsi_roomtype WHERE roomtype_id=".$roomtype_id);
	$results = $DB->numRows($q);
	if($results>0){
		$roomtype = $DB->fetchObject($q);
		return $roomtype->type_name;
	} else { return ""; }
}
function room_capacity($capacity_id, $return="title"){
	global $DB;
	$q = $DB->query("SELECT title,capacity FROM bsi_capacity WHERE id=".$capacity_id);
	$results = $DB->numRows($q);
	if($results>0){
		$capacity = $DB->fetchObject($q);
		if($return=="capacity"){ return $capacity->capacity; }
		else { return $capacity->title; }
	} else { return ""; }
}
function hotel_rooms_details($hotel_id, $start=0, $show=25){
	global $DB;
	$list = "";
	$q = $DB->query("SELECT * FROM bsi_room WHERE hotel_id=".$hotel_id." AND status=1 ORDER BY room_id DESC LIMIT {$start}, {$show}");
	$results = $DB->numRows($q);
	if($results>0){
		while($room = $DB->fetchObject($q)){
			$list .= room_details_box($room);
		}
		return $list;
	} else { return ""; }
}
function getSingleValue($tableName, $columnName, $kondisi)
{
  global $DB;
  $q = $DB->query("SELECT `$columnName` FROM `$tableName` WHERE `$kondisi`");
  $f = $q->fetch();
  $result = $f[$columnName];
  return $result;
}
function hotel_booking_extras_chan($hotel_id, $start=0, $show=25){
	global $DB;

	$list = "";
	$q = $DB->query("SELECT * FROM booking_extras WHERE hotel_id=".$hotel_id." AND status=1 ORDER BY extras_id DESC LIMIT {$start}, {$show}");
	$results = $DB->numRows($q);
	if($results>0){
		while($extra = $DB->fetchObject($q)){
			$list .= room_booking_extras_box_chan($extra);
		}
		return $list;
	} else { return ""; }
}
function hotel_rooms_details_chan($hotel_id, $check_in, $check_out, $start=0, $show=25){
	global $DB;
	
	//chanif
	//cari jumlah kamar tersisa
	//cari jumlah kamar
	//bandingkan
	//
	
	$list = "";
	$q = $DB->query("SELECT * FROM bsi_room WHERE hotel_id=".$hotel_id." AND status=1 ORDER BY room_id DESC LIMIT {$start}, {$show}");
	$results = $DB->numRows($q);
	if($results>0){
		while($room = $DB->fetchObject($q)){
			$list .= room_details_box_chan($room, $check_in, $check_out);
		}
		return $list;
	} else { return ""; }
}
function hotel_room_detail($room_id, $page="booking"){
	global $DB;
	//$q = $DB->query("SELECT * FROM bsi_room WHERE room_id=".$room_id." AND status=1");
	$q = $DB->query("SELECT * FROM bsi_room WHERE room_id=".$room_id." AND status=1");
	$results = $DB->numRows($q);
	if($results>0){
		return room_details_box($DB->fetchObject($q), $page);
	} else { return ""; }
}
function hotel_gallery($hotel_id, $start=0, $show=25){
	global $DB;
	$q = $DB->query("SELECT img_path FROM bsi_gallery WHERE hotel_id=".$hotel_id." AND status=1 ORDER BY pic_id DESC LIMIT {$start}, {$show}");
	$results = $DB->numRows($q);
	if($results>0){
		$list = array();
		while($pic = $DB->fetchObject($q)){
			$images_dir = ABSPATH."images";
			$path = $images_dir."/hotels/galleries/".$hotel_id."/".$pic->img_path;
			if($pic->img_path!="" and is_file($path) and file_exists($path)){
				$list[] = "images/hotels/galleries/".$hotel_id."/".$pic->img_path;
			}
		}
		return hotel_gallery_box($list);
	}
}
function room_gallery($room_id, $start=0, $show=25){
	global $DB;
	$q = $DB->query("SELECT img_path, hotel_id FROM bsi_gallery WHERE room_id=".$room_id." AND status=1 ORDER BY pic_id DESC LIMIT {$start}, {$show}");
	$results = $DB->numRows($q);
	if($results>0){
		$list = array();
		while($pic = $DB->fetchObject($q)){
			$images_dir = ABSPATH."images";
			$path = $images_dir."/hotels/galleries/".$pic->hotel_id."/".$pic->img_path;
			if($pic->img_path!="" and is_file($path) and file_exists($path)){
				$list[] = "images/hotels/galleries/".$pic->hotel_id."/".$pic->img_path;
			}
		}
		return room_gallery_box($list);
	} else { return room_gallery_box(""); }
}
function hotel_reviews($hotel_id, $return="rate"){
	global $DB;
	$q = $DB->query("SELECT sum(review_rate_overall) as Rate, count(review_rate_overall) as Num FROM hotel_reviews WHERE hotel_id=".$hotel_id." AND status=1");
	$results = $DB->numRows($q);
	if($results>0){
		$reviews = $DB->fetchObject($q);
		if($reviews->Num==0 or $reviews->Num==""){ return 0; }
		else {
			if($return=="rate"){
				return ceil($reviews->Rate/$reviews->Num);
			} else { return $reviews->Num; }
		}
	} else { return 0; }
}
function last_reviews($hotel_id){
	global $DB;
	$q = $DB->query("SELECT review_detailed, user_id FROM hotel_reviews WHERE hotel_id=".$hotel_id." AND status=1 ORDER BY review_id DESC LIMIT 0, 7");
	$results = $DB->numRows($q);
	if($results>0){
		$list = "";
		while($review = $DB->fetchObject($q)){
			$list .= review_box($review);
		}
		return $list;
	} else{ return ""; }
}
function get_booking($booking_id){
	global $DB;
	$q = $DB->query("SELECT * FROM bsi_bookings WHERE booking_id=".$booking_id);
	$results = $DB->numRows($q);
	if($results>0){
		return $DB->fetchObject($q);
	} else { return false; }
}
function get_user($user_id){
	global $DB;
	$q = $DB->query("SELECT * FROM users WHERE user_id=".$user_id." and status=1");
	$results = $DB->numRows($q);
	if($results>0){
		return $DB->fetchObject($q);
	} else { return ""; }
}
function get_room($room_id){
	global $DB;
	$q = $DB->query("SELECT * FROM bsi_room WHERE room_id=".$room_id." and status=1");
	$results = $DB->numRows($q);
	if($results>0){
		return $DB->fetchObject($q);
	} else { return ""; }
}
function get_promo_features($room_id){
	global $DB;
	$q = $DB->query("SELECT features FROM promotions WHERE room_id=$room_id and status=1 and CURDATE() between start_date AND end_date");
	$results = $DB->numRows($q);
	if($results>0){
		return $DB->fetchObject($q);
	} else { return ""; }
}
function room_reservations($room_id, $date){
	global $DB; 
	$q = $DB->query("SELECT booking_details_json FROM bsi_bookings WHERE room_id=".$room_id." and (status=1 or status=2 or status=-3) and '".date("Y-m-d",$date)."' between start_date and end_date");
	$results = $DB->numRows($q);
	if($results>0){
		$total = 0;
		while($booking = $DB->fetchObject($q)){
			$total += count(json_decode($booking->booking_details_json, true));
		}
		return $total;
	} else { return 0; }
}
function room_available($room_id, $date, $num_rooms=1){
	$room = get_room($room_id);
	$total_rooms = $room->no_of_rooms;
	$reserved = room_reservations($room_id, $date);
	$bal = $total_rooms-$reserved;
	if($bal>=$num_rooms){
		return true;
	} else { return false; }
}
function check_room_availability($room_id, $checkin, $checkout, $num_rooms=1){
	$diff =  ($checkout - $checkin)/ 86400;
	$not = 0;
	if($diff==0){$diff=1;}
	for($i=0; $i<$diff; $i++){
		if(!room_available($room_id, $checkin+(86400*$i), $num_rooms)){
			$not=1;
		}
	}
	if($not==1){return false;}else{return true;}
}
function calendarJsonTooltip($room_id, $date){
	$data = array();
	$room = get_room($room_id);
	$hotel = hotel_details($room->hotel_id);
	$data['hot'] = $room->hot;
	$data['min_stay'] = $hotel->hotel_info_min_stay;
	$data['incl_capacity'] = room_capacity($room->capacity_id, "capacity")." People";
	$data['max_capacity'] = ($data['incl_capacity']+$room->room_guests_capacity)." People";
	$data['availability'] = $room->no_of_rooms-room_reservations($room_id, $date)." Rooms";
	$data['promo'] = get_promo_features($room_id);
	return $data;
}
function imageCheck($filename, $return="src", $type="room", $imageOf='hotels'){
	//$filename=UserID or FanPageID, $imageOf= main folder under images, $type= room/logo (Inner folder)
	$images_dir = ABSPATH."images";
	if($type=="users"){
		$file = $images_dir."/users/".$filename;
		$public_path = "images/users/".$filename;
	} else {
		$file = $images_dir."/".$imageOf."/".$type."/".$filename;
		$public_path = "images/".$imageOf."/".$type."/".$filename;
	}
	if($return=="src"){
		if (is_file($file) and file_exists($file)) { return $public_path; }
		else { return defaultImage($type); }
	} else {
		if (is_file($file) and file_exists($file)) { return true; }
		else { return false; }
	}
}
function defaultImage($imageOf){
	if($imageOf=="room"){ return "images/defaults/room.gif"; }
	if($imageOf=="logo"){ return "images/defaults/hotel_logo.jpg"; }
	if($imageOf=="users"){ return "images/defaults/user.jpg"; }
}
function validateDate($date){
    $d = DateTime::createFromFormat('m/d/Y', $date);
    return $d && $d->format('m/d/Y') == $date;
}
function booking_history($user=NULL){
	global $DB;
	if($user==NULL){$user = $_SESSION['user_id'];}
	$list = "";
	$q = $DB->query("select * from bsi_bookings where user_id=".$user);
	$results = $DB->numRows($q);
	if($results>0){
		while($booking=$DB->fetchObject($q)){
			$list .= booking_history_box($booking);
		}
		return $list;
	} else { return "No Bookings listed."; }
}
function hotel_paid($hotel_id){
	global $DB;
	$q = $DB->query("select hotel_id from bsi_hotels where hotel_id={$hotel_id} and status=1 and paid_upto>=NOW()");
	if($DB->numRows($q)>0){ return true; }
	else{ return false; }
}
/* initialize functions into variables */
if(!logged() and isset($_COOKIE['user']) and $_COOKIE['user']!=""){
	$_SESSION['user_id'] = base64_decode($_COOKIE['user'])-123; //123 is added in cookie on login function at ajax page
	header("Location: ".$_SERVER["REQUEST_URI"]);
}
if(!isset($_COOKIE['currency']) or $_COOKIE['currency']==""){
	setcookie("currency", currency("code"), time() + (86400 * 30 * 365 * 10), "/");
}
if(!isset($_COOKIE['language']) or $_COOKIE['language']==""){
	setcookie("language", language(), time() + (86400 * 30 * 365 * 10), "/");
	include(ABSPATH."lang/".language("file"));
	header("Location: ".$_SERVER["REQUEST_URI"]); //redirect to current page 
} else {
	include(ABSPATH."lang/".user_language($_COOKIE['language']));
}
if(isset($_GET['hid'])){
	$perma = explode("-",$_GET['hid']);
	if($_GET['hid']!="" and $perma[0]!="" and is_numeric($perma[0]) and hotel_exists($perma[0])){ 
		if(hotel_paid($perma[0])){ $hotel = hotel_details($perma[0]); }
		else{ header("Location: /hotel/hotel-suspended.php?hotel=".$perma[0]); }
	}
	else { header("Location: /hotel/"); }
}
if(isset($_GET['on']) and isset($_GET['room'])){
	if($_GET['room']!="" and is_numeric($_GET['room']) and room_exists($_GET['room'])){
		$room = get_room($_GET['room']);
		if(!hotel_paid($room->hotel_id)){ header("Location: /hotel/hotel-suspended.php?hotel=".$room->hotel_id); }
	} else { header("Location: /hotel/"); }
}
if(isset($_POST['add_coupon']) and $_POST['coupon']!=""){
	if(couponExists($_POST['coupon'])){
		$_SESSION['coupon_added'] = $_POST['coupon'];
		header("Location: ".$_SERVER["REQUEST_URI"]); //redirect to current page 
	}
}
if(isset($_POST['submit_review']) and logged()){
	// echo "<pre>";
	// var_dump($_FILES);
	// echo "</pre>";
	$service_rate = $_POST['service_rate'];
	$value_rate = $_POST['value_rate'];
	$sleep_rate = $_POST['sleep_rate'];
	$clean_rate = $_POST['clean_rate'];
	$location_rate = $_POST['location_rate'];
	$rooms_rate = $_POST['rooms_rate'];
	$pool_rate = $_POST['pool_rate'];
	$fitness_rate = $_POST['fitness_rate'];
	$hotelid = $_POST['hotelid'];
	$rate_all = $_POST['rate_all'];
	$trip_type = $_POST['trip_type'];
	$review_title = trim(strip_tags($_POST['review_title']));
	$review_text = trim(strip_tags($_POST['review_text']));
	$travel_month = $_POST['travel_month'];
	$tip = trim(strip_tags($_POST['tip']));
	if(isset($_FILES['review_pics']) and is_array($_FILES['review_pics']['name']) and count($_FILES['review_pics']['name'])>0){
		$pics = array();
		for($i=0; $i<count($_FILES['review_pics']['name']); $i++){
			$ext = explode(".", $_FILES['review_pics']['name'][$i]);
			$filename = rand(11,99).time().rand(11,99).end($ext);
			$path = ABSPATH."images/hotels/reviews/".$filename;
			if(move_uploaded_file($_FILES['review_pics']['tmp_name'][$i], $path)){
				$pics[] = $filename;
			}
		}
		$pic = implode("|", $pics);
	} else { $pic=""; }
	
	$q = $DB->query("INSERT INTO hotel_reviews (user_id, review_rate_overall, review_rate_service, review_rate_sleep_quality, review_rate_location, review_rate_swimming_pool, review_rate_value, review_rate_cleanliness, review_rate_rooms, review_rate_fitness_facility, hotel_id, review_title, review_detailed, trip_with, travel_month, tip_for_travellers, review_pics, status, review_dated) VALUES ('".$_SESSION['user_id']."', '{$rate_all}', '{$service_rate}', '{$sleep_rate}', '{$location_rate}', '{$pool_rate}', '{$value_rate}', '{$clean_rate}', '{$rooms_rate}', '{$fitness_rate}', '{$hotelid}', '".$DB->escapeValue($review_title)."', '".$DB->escapeValue($review_text)."', '{$trip_type}', '".date('Y-m-d', strtotime($travel_month))."', '".$DB->escapeValue($tip)."', '{$pic}', '0', '".date('Y-m-d H:i:s')."')");
}
/* theme functions */
function amenities($amenities, $place="hotel"){
	global $_LANGUAGE;
	$list = "";
	$all = explode("|", $amenities);
	foreach($all as $amenity){
		switch ($amenity) {
			case 'WI_FI':
				$class = 'wifi';
			break;
			case 'SWIMMING POOL':
				$class = 'swimming';
			break;
			case 'AIR CONDITIONING':
				$class = 'aircon';
			break;
			case 'WINE BAR':
				$class = 'winebar';
			break;
			case 'SECURE VAULT':
				$class = 'securevault';
			break;
			case 'PETS ALLOWED':
				$class = 'pets';
			break;
			case 'FREE PARKING':
				$class = 'parking';
			break;
			case 'HANDICAP ACCESSIBLE':
				$class = 'handicapaccessiable';
			break;
			case 'ELEVATOR IN BUILDING':
				$class = 'elevator';
			break;
			case 'TELEVISION':
				$class = 'television';
			break;
			case 'FITNESS FACILITY':
				$class = 'fitnessfacility';
			break;
			case 'SMOKING ALLOWED':
				$class = 'smoking';
			break;
			case 'PICK AND DROP':
				$class = 'pickanddrop';
			break;
			case 'PLAY PLACE':
				$class = 'playplace';
			break;
			case 'CONFERENCE ROOM':
				$class = 'conference';
			break;
			case 'DOORMAN':
				$class = 'doorman';
			break;
			case 'SUITABLE FOR EVENTS':
				$class = 'star';
			break;
			case 'COFFEE':
				$class = 'coffee';
			break;
			case 'FRIDGE':
				$class = 'fridge';
			break;
			case 'ENTERTAINMENT':
				$class = 'entertainment';
			break;
			case 'ROOM SERVICE':
				$class = 'phone';
			break;
			case 'COMPLIMENTARY BREAKFAST':
				$class = 'breakfast';
			break;
			case 'FIRE PLACE':
				$class = 'fireplace';
			break;
			case 'HOT TUB':
				$class = 'tub';
			break;
			default:
				$class = 'star';
			break;
		}
		$amenity = str_replace(" ","_",strtolower($amenity));
		if($place=="hotel"){
			$list .= '<li class="col-md-4 col-sm-6"><div class="icon-box style1"><i class="soap-icon-'.$class.'"></i>'.$_LANGUAGE[$amenity].'</div></li>';
		} else {
			$list .= '<i class="soap-icon-'.$class.' circle" title="'.$_LANGUAGE[$amenity].'"></i>';
		}
	}
	return $list;
}
function room_details_box($room, $page="hotel"){
	global $_LANGUAGE;
	if($page=="booking"){ $btn=""; }
	else{$btn= '<a href="booking/'.$room->room_id.'/'.date("m/d/Y").'/" title="" class="button btn-small full-width text-center">'.$_LANGUAGE['book_now'].'</a>';}
	return '<article class="box">
            <figure class="col-sm-4 col-md-3">
                <a class="hover-effect popup-gallery" href="ajax/slideshow-popup.php?room='.$room->room_id.'" title=""><img width="230" height="160" src="'.imageCheck($room->room_basic_pic).'" alt=""></a>
            </figure>
            <div class="details col-xs-12 col-sm-8 col-md-9">
                <div>
                    <div>
                        <div class="box-title">
                            <h4 class="title">'.room_capacity($room->capacity_id).' '.room_type_title($room->roomtype_id).'</h4>
                        </div>
                        <div class="amenities">
                            '.amenities($room->room_basic_amenities, "room").'
                        </div>
                    </div>
                    <div class="price-section">
                        <span class="price" style="white-space: nowrap"><small>'.$_LANGUAGE['per'].'/'.$_LANGUAGE['night'].'</small>'.amount($room->room_rate, $room->room_id).'</span>
                    </div>
                </div>
                <div>
                    <p>'.$room->room_description.'</p>
                    <div class="action-section">
                        '.$btn.'
                    </div>
                </div>
            </div>
        </article>';
}

function room_details_box_chan($room, $check_in, $check_out, $page="hotel"){
	global $_LANGUAGE;
	global $DB;
	
	if(!validateDate($check_in) or strtotime($check_in)<time()){ $check_in=date("m/d/Y"); }
	if(!validateDate($check_out) or strtotime($check_out)>time()+(86400*90)){$next=time()+86400; $check_out=date("m/d/Y", $next);}
		
	if(check_room_availability($room->room_id, strtotime($check_in), strtotime($check_out), room_capacity($room->capacity_id, "capacity"))){$class="";}else{$class="<sup style='font-size: 70%;color: #ff0404;'> (not available for today)</sup>";}
	
	$diff =  "";//$checkout - $checkin;
	

	$harga = $room->room_rate;
	
	/*
	$sql_query = $DB->query('select ' . strtolower(date('D')) .' AS price from bsi_priceplan where status=1 and "'.date('Y-m-d').'" between start_date and end_date and room_id ='.$room->room_id." order by plan_id desc limit 1");

	if($DB->numRows($sql_query)<1){
	  $sql_query2 = $DB->query('select ' . strtolower(date('D')) .' AS price from bsi_priceplan where status=1 and start_date="0000-00-00" and room_id ='.$room->room_id." order by plan_id desc limit 1");
	  if($DB->numRows($sql_query2)<1){
		$harga = $room->room_rate;
	  } else { $price_result = mysqli_fetch_array($sql_query2); $harga=$price_result['price']; }
	} else { $price_result = mysqli_fetch_array($sql_query); $harga=$price_result['price']; }
	*/
	
	if($page=="booking"){ $btn=""; }
	else{$btn= '<a href="booking/'.$room->room_id.'/'.date("m/d/Y").'/" title="" class="button btn-small full-width text-center">'.$_LANGUAGE['book_now'].'</a>';}
	return '<article class="box">
            <figure class="col-sm-4 col-md-3">
                <a class="hover-effect popup-gallery" href="ajax/slideshow-popup.php?room='.$room->room_id.'" title=""><img width="230" height="160" src="'.imageCheck($room->room_basic_pic).'" alt=""></a>
            </figure>
            <div class="details col-xs-12 col-sm-8 col-md-9">
                <div>
                    <div>
                        <div class="box-title">
                            <h4 class="title">'.room_type_title($room->roomtype_id).' '.room_capacity($room->capacity_id).' '.$_LANGUAGE['room'].' '.$diff.'</h4>
                        </div>
                        <div class="amenities">
                            '.amenities($room->room_basic_amenities, "room").'
                        </div>
                    </div>
                    <div class="price-section">
                        <span class="price" style="white-space: nowrap"><small>'.$_LANGUAGE['per'].'/'.$_LANGUAGE['night'].'</small>'.amount($harga, $room->room_id).'</span>
                    </div>
                </div>
                <div>
                    <p>'.$room->room_description.'</p>
                    <div class="action-section">
                        '.$btn.'
                    </div>
                </div>
            </div>
        </article>';
}
function room_booking_extras_box_chan($extra, $page="hotel"){
	global $_LANGUAGE;
	
	return ' <tr>
				  <td style="width:25%" >
					  <input id="'.$extra->slug.'" data-price="'.amount($extra->extras_rate).'" class="extras_checkbox" name="extra_'.$extra->extras_id.'" type="checkbox" value="1" style="margin-right: 6px;vertical-align: middle;position: relative;top: -4px;">
					  <label for="extra">
					  '.$extra->extras_title.' <br><span style="margin-left:22px;">'.$_LANGUAGE['quantity'].' <input id="'.$extra->slug.'_quantity" name="extra_'.$extra->slug.'_quantity" size="2" min="0" style="width:35px;" class="qty" type="number"></span>
					  </label>
				  </td>
				  <td style="width:45%" class="description mobile-hide">
					  '.$extra->extras_description.'
				  </td>
				  <td style="width:20%" >'.amount($extra->extras_rate).' per '.$extra->extras_per.'</td>
				  <td style="width:10%;white-space: nowrap;" class="extra_amount numeric">'.amount(0).'</td>
			  </tr>';
}
function review_box($review){
	$user = get_user($review->user_id);
	return '<li>
                <p class="description">'.$review->review_detailed.'</p>
                <div class="author clearfix">
                    <a href="#"><img src="'.imageCheck($user->profile_pic, "src", "users").'" alt="" width="74" height="74" /></a>
                    <h5 class="name">'.$user->first_name.' '.$user->last_name.'</h5>
                </div>
            </li>';
}
function hotel_gallery_box($photos){
	$list = '<div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
                <ul class="slides">';
    foreach($photos as $photo){
    	$list .= '<li><img src="'.$photo.'" alt="" /></li>';
    }
    $list .= '</ul>
            </div>
            <div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                <ul class="slides">';
    foreach($photos as $photo){
    	$ex = explode("/",$photo);
    	array_splice($ex, -1, 0, "thumbnail" );
    	$src = implode( "/", $ex);
    	$list .= '<li><img src="'.$src.'" alt="" /></li>';
    }
    $list .= '</ul>
            </div>';
	
	return $list;
}
function room_gallery_box($photos){
	if(is_array($photos) and count($photos)>0){
		$list = '<div class="photo-gallery style1" id="photo-gallery1" data-animation="slide" data-sync="#image-carousel1">
	    			<ul class="slides">';
	    foreach($photos as $photo){
	    	$list .= '<li><img src="'.$photo.'" alt="" /></li>';
	    }
	    $list .= '</ul>
				</div>
				<div class="image-carousel style1" id="image-carousel1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photo-gallery1">
				    <ul class="slides">';
	    foreach($photos as $photo){
	    	$ex = explode("/",$photo);
	    	array_splice($ex, -1, 0, "thumbnail" );
	    	$src = implode( "/", $ex);
	    	$list .= '<li><img src="'.$src.'" alt="" /></li>';
	    }
	    $list .= '</ul>
	            </div>';
		
		return $list;
	} else { echo "<img width='900px' height='500px' src='images/defaults/no.jpg' />"; }
}
function booking_history_box($booking){
	$hotel = hotel_details($booking->hotel_id);
	$days = ceil((strtotime($booking->end_date)-strtotime($booking->start_date))/86400);
	$rooms = count(json_decode($booking->booking_details_json, true));
	if($booking->status==0){$status="PENDING";}elseif($booking->status==1){$status="CONFIRMED";}elseif($booking->status==2){$status="CHECKED OUT";}elseif($booking->status==-1){$status="REJECTED";}elseif($booking->status==-2){$status="CANCELLED";}
	return '<div class="booking-info clearfix">
                <div class="date">
                    <label class="month">'.date("M",strtotime($booking->start_date)).'</label>
                    <label class="date">'.date("d",strtotime($booking->start_date)).'</label>
                    <label class="day">'.date("D",strtotime($booking->start_date)).'</label>
                </div>
                <h4 class="box-title"><i class="icon soap-icon-hotel blue-color circle"></i>'.$hotel->hotel_name.'<small>'.$days.' Days, '.$rooms.' Rooms</small></h4>
                <dl class="info">
                    <dt>booked on</dt>
                    <dd>'.date("D, M d, Y", strtotime($booking->booking_time)).'</dd>
                </dl>
                <button class="btn-mini status" title="Call us for Booking Status">'.$status.'</button>
            </div>';
}
if(logged() and isset($_POST['updatepass'])){
	foreach($_POST as $k=>$v){$$k=$v;}
	if($pass!=$pass2){echo "<script>alert('Passwords not matching, please try again!');</script>";}
	elseif($pass==$oldpass){echo "<script>alert('New password should be changed, please try again!');</script>";}
	else{ 
		$correct = $DB->numRows($DB->query("select user_id from users where password='{$oldpass}' and user_id=".$_SESSION['user_id']));
		if($correct>0){
			if(isset($_POST['newsletters'])){$newsletters=1;}else{$newsletters=0;}
			$DB->query("update users set password='{$pass}', newsletters={$newsletters} where user_id=".$_SESSION['user_id']);
			echo "<script>alert('Settings saved successfully!');</script>";
		} else { echo "<script>alert('Incorrect old passwrod, please try again!');</script>"; }
	}
}
if(logged() and isset($_POST['editprofile'])){
	foreach($_POST as $k=>$v){$$k=$v;}
	if(isset($_FILES['pic']['name']) and $_FILES['pic']['name']!=""){
		$ex = explode(".", $_FILES['pic']['name']);
		if(end($ex)=="jpg" or end($ex)=="JPG" or end($ex)=="JPEG" or end($ex)=="png" or end($ex)=="PNG" or end($ex)=="jpeg" or end($ex)=="gif" or end($ex)=="GIF"){
			$file = $_SESSION['user_id'].".".end($ex);
			move_uploaded_file($_FILES['pic']['tmp_name'], ABSPATH."images/users/".$file);
		}
	}
	$q = $DB->query("update users set first_name='{$fname}', last_name='{$lname}', profile_pic='{$file}', about='<p>".stripslashes(strip_tags($about))."</p>', street_address='{$address}', city='{$city}', zip='{$zip}', state='{$state}', country_id='{$country}', phone='{$phone}', fax='{$fax}', identity_type='{$idtype}', identity_number='{$idno}' where user_id=".$_SESSION['user_id']." limit 1");
	echo "<script>alert('wohooo! Profile updated successfully!');</script>";
}
?>