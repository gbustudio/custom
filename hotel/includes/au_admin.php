<?php session_start();
	$aufile = basename($_SERVER['PHP_SELF']);
	if(!isset($_SESSION['au_user'])){ if(!isset($_GET['login'])){ header('Location: '.$aufile.'?login'); } }
	elseif(!file_exists('config.txt')){ if(!isset($_GET['settings']) and !isset($_GET['editor'])){ header('Location: '.$aufile.'?settings'); } }
	if(isset($_GET) and count($_GET)>0){ $head = ucwords(str_replace("_"," ",array_keys($_GET)[0])); } else { $head="";}
	if(isset($_GET['editor']) and count($_GET)>1 and !isset($_GET['create_folder']) and !isset($_GET['create_file']) and !isset($_GET['upload_file'])){ $subhead=ucwords(base64_decode(array_keys($_GET)[1])); }
	elseif(isset($_GET) and count($_GET)>1){ $subhead=ucwords(array_keys($_GET)[1]); } else { $subhead="";}
	if(isset($_GET) and count($_GET)>2){ $subhead .= " => ".ucwords(array_keys($_GET)[2]); }
	if(isset($_GET) and (isset($_GET['settings']) or isset($_GET['login']))){ $subhead=ucwords(array_keys($_GET)[0]); }
	$msg="";
	$values = array();
	if(file_exists('config.txt')){ 
		$values = explode("_|_", file_get_contents('config.txt'));
		if(count($values)!=5){ unlink('config.txt'); header("Location: ".$aufile."?settings"); }
		else { $conn = mysqli_connect($values[1], $values[2], $values[3]); }
	} else { $values[0]="Test & Co."; $values[2]=$values[3]=""; $values[1]="localhost"; $values[4]="|admin|"; }
	
	if(isset($_POST['login'])){
		$user = $_POST['user'];
		$pass = $_POST['pass'];
		if($user=='admin' and $pass=="admin"){
			$_SESSION['au_user'] = 'admin';
			header("Location: ".$aufile."?database");
		} else { $msg = "<b style='color:red;'>Wrong username and Password combination. lala</b>"; }
	}
	
	if(isset($_GET['logout'])){
		unset($_SESSION['au_user']);
		$_SESSION['au_user'] = "";
		session_destroy();
		header("Location: ".$aufile."?login");
	}
	
	if(isset($_POST['saveConfig'])){
		$name = ucwords(str_replace("_|_","", $_POST['name']));
		$host = $_POST['host'];
		$user = $_POST['user'];
		if(isset($_POST['pass'])){ $pass = $_POST['pass']; } else { $pass=""; }
		$adminPassword = $_POST['adminPassword'];
		
		if(!empty($name) and strlen($name)>1 and !empty($host) and !empty($user) and !empty($adminPassword) and strlen($adminPassword)>3){
			$conn = mysqli_connect($host, $user, $pass);
			if(mysqli_connect_errno()){
				$msg = "<b style='color:red;'>Failed, MySQL Connect Error! ".mysqli_connect_error()."</b>";
			} else {
				$configFile = fopen('config.txt', 'w');
				$data = $name."_|_".$host."_|_".$user."_|_".$pass."_|_".$adminPassword;
				fwrite($configFile, $data);
				fclose($configFile);
				$msg = "<b style='color:green;'>Hi Five! Settings Saved Successfully.</b>";
			}
		} else { $msg = "<b style='color:red;'>Failed, Enter values correctly!</b>"; }
	}
	
	if(isset($_POST['update_file'])){
		$file_content = $_POST['file_content'];
		$file_path = $_POST['file_path'];
		$file_handle = fopen($file_path, 'w');
		fwrite($file_handle, $file_content);
		fclose($file_handle);
		$msg = "<b style='color:green;'>Hurray! File Updated Successfully.</b>";
	}
	
	if(isset($_GET['kick']) and count($_GET)>2){
		$kick = base64_decode(array_keys($_GET)[1]);
		unlink($kick);
		header("Location: ".$aufile."?editor&".base64_encode(substr($kick, 0, strrpos($kick, "/"))));
	}
	
	if(isset($_GET['rmdir']) and count($_GET)>2){
		$rmdir = base64_decode(array_keys($_GET)[1]);
		rmdir($rmdir);
		header("Location: ".$aufile."?editor&".base64_encode(substr($rmdir, 0, strrpos($rmdir, "/"))));
	}
	
	if(isset($_POST['create_db'])){
		$db = str_replace(" ","_",strip_tags($_POST['db_name']));
		mysqli_query($conn, 'CREATE DATABASE '.$db);
		$msg = "<b style='color:green;'>Yahoo! ".$db." Database Created Successfully.</b>";
	}
	
	if(isset($_GET['database']) and isset($_GET['drop']) and count($_GET)>2){
		$conn3 = mysqli_connect($values[1], $values[2], $values[3], array_keys($_GET)[1]);
		mysqli_query($conn3, "DROP TABLE ".array_keys($_GET)[2]);
		header("Location: ".$aufile."?database&".array_keys($_GET)[1]);
	}
	
	if(isset($_GET['database']) and isset($_GET['truncate']) and count($_GET)>2){
		$conn3 = mysqli_connect($values[1], $values[2], $values[3], array_keys($_GET)[1]);
		mysqli_query($conn3, "Truncate TABLE ".array_keys($_GET)[2]);
		header("Location: ".$aufile."?database&".array_keys($_GET)[1]);
	}

	if(isset($_GET['database']) and isset($_GET['del']) and count($_GET)>4){
		$dbd = explode("___", array_keys($_GET)[4]);
		$conn3 = mysqli_connect($values[1], $values[2], $values[3], array_keys($_GET)[1]);
		mysqli_query($conn3, "delete from ".array_keys($_GET)[2]." where ".$dbd[0]."=".$dbd[1]." limit 1");
		header("Location: ".$aufile."?database&".array_keys($_GET)[1]."&".array_keys($_GET)[2]);
	}

	if(isset($_POST['nvalue'])){
		$val = $_POST['nvalue'];
		$dbd = explode("___", $_POST['details']);
		$conn4 = mysqli_connect($values[1], $values[2], $values[3], array_keys($_GET)[1]);
		mysqli_query($conn4, "update ".array_keys($_GET)[2]." set ".$dbd[0]."='".$val."' where ".$dbd[1]."='".$dbd[2]."' limit 1");
		header("location: ".$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING']);
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="robots" content="noindex, nofollow" />
	<title>Admin Panel</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<style>
		body{background: #eee;margin: 0;}
		a, a:link, a:hover, a:active, a:visited{text-decoration:none;color:inherit;}
		#container{width:1000px;height:auto;position:relative;margin:10px auto;background:#fff;color:#000;font-size:12px;font-family:"Comic Sans MS", cursive, sans-serif;box-shadow:1px 1px 1px 1px rgba(0,0,0,0.6);}
		#header{position:relative; width:100%;height:50px;background:#777;}
		#header h1{height:50px;width:250px;position:relative;float:left;margin:0;font-size:24px;line-height:50px;padding: 0 20px;color:#e12;}
		#header ul{padding:0;position:relative;width:680px;line-height:50px;float:right;text-align:right;margin-right:10px;margin-bottom:0;margin-top:0;}
		#header ul li{list-style:none;display:inline;}
		#header ul li a{padding:9px 10px;color:#fff;font-size:18px;}
		#header ul li a:hover, .active{color:#6f6 !important;border-bottom:2px solid #6f6;}
		#content{width:1000px;position:relative;overflow:hidden;}
		#content #leftbar{width:200px;position:relative;background:#777;float:left;}
		#content h2{margin:0;font-size:20px;padding:10px;background:#aaa;color:blue;border-bottom:1px dashed #fff;width:90%;}
		#content h2 span{color:#6f6;}
		.full{width: 100% !important;}
		#content #leftbar ul{padding:0;margin-bottom:0;}
		#content #leftbar ul li{list-style:none;margin-bottom:1px;min-height:20px;background:#888;padding:10px;font-size:14px;color:#fff;width:170px;margin-left:10px;word-wrap:break-word;}
		#content #leftbar ul li:hover{margin-left:0;color:#6f6;}
		#content #leftbar ul li span{background:#fff;min-width:20px;border-radius:20px;float:right;text-align:center;color:#000;padding:0 3px;}
		#content #rightbar{width:795px;min-height:600px;position:relative;float:left;margin-left:5px;}
		#footer{width:100%;height:40px;position:relative;font-size:18px;color:#fff;background:#777;line-height:40px;text-align:center;}
		#footer a{color:pink;}
		<?php if(isset($_GET['settings']) or isset($_GET['login'])){ echo "#content #leftbar{display:none;}#content #rightbar{width:100%;margin-top:5px;margin-left:0;}"; } ?>
		.inner-content{position:relative;width:100%;height:100%;padding:20px;<?php if(isset($_GET['database']) and count($_GET)>2){echo "overflow-x:scroll;"; } ?>}
		.inner-content .login{margin:30px auto; text-align:center;border:1px solid green;width:320px;min-height:100px;position:relative;padding:20px;}
		.btn{min-width:70px;padding:5px 20px;}
		.inner-content .config{margin-bottom:30px;width:300px;}
		.inner-content .link{background:#333;color:#fff;padding:5px 20px;margin-right:20px;}
		.inner-content li{height:20px;line-height:20px;}
		.inner-content table{position:relative;padding-right: 40px;}
		.inner-content table td, .inner-content table th{border: 1px solid black;padding:5px 20px;min-width:30px;max-width:200px;word-wrap:break-word;}
		.inner-content table td span{font-size:20px;margin-right:5px;color:red;cursor:pointer;}
		.inner-content .pagination{position:relative;width:100%;height:40px;text-align:center;margin-top:40px;}
		.inner-content .pagination a{margin-right:20px;}
		.dir{color:blue !important;}
		input{padding:5px 20px;}
	</style>
</head>
<body>
	<div id="container">
		<div id="header">
			<h1><a href=""><?php echo $values[0]; ?></a></h1>
			<?php if(!isset($_GET['login'])){ ?>
			<ul>
				<li><a <?php if(isset($_GET['db'])){ echo 'class="active"'; } ?> href="<?=$aufile?>?database">Database</a></li>
				<li><a <?php if(isset($_GET['db'])){ echo 'class="active"'; } ?> href="<?=$aufile?>?editor">Editor</a></li>
				<li><a <?php if(isset($_GET['db'])){ echo 'class="active"'; } ?> href="<?=$aufile?>?settings">Settings</a></li>
				<li><a href="<?=$aufile?>?logout">Logout</a></li>
			</ul>
			<?php } ?>
		</div>  <!-- #header -->

		<div id="content">
			<div id="leftbar">
				<h2>Menu <span> { <?php echo $head; ?> }</span></h2>
				
					<?php if(isset($_GET['database'])){ ?>
						<ul>
							<a href="<?=$aufile?>?database&create_database"><li>Create Database</li></a>
							<a href="<?=$aufile?>?database&sql"><li>SQL Query</li></a>
						</ul>
						<h2>Select Database</h2>
						<ul>
						<?php $dbq = mysqli_query($conn, "SHOW DATABASES");
								while($dbr = mysqli_fetch_assoc($dbq)){
									if($dbr['Database']=='mysql' or $dbr['Database']=='information_schema' or $dbr['Database']=='performance_schema'){ continue; }
						?>
							<a href="<?=$aufile?>?database&<?php echo urlencode($dbr['Database']); ?>"><li><?php echo $dbr['Database']; ?></li></a>
						<?php } ?>
						</ul>
						
						<?php //if(count($_GET)==2 and !isset($_GET['create_database'])){ ?>
								
									<?php $tabq = mysqli_query($conn, "SHOW TABLES from ".array_keys($_GET)[1]); 
											if(mysqli_num_rows($tabq)>0){
											echo "<h2>Select Table</h2><ul>";
											while($tabr = mysqli_fetch_assoc($tabq)){
									?>
									<a href="<?=$aufile?>?database&<?php echo array_keys($_GET)[1].'&'.$tabr['Tables_in_'.array_keys($_GET)[1]].'&0'; ?>"><li><?php echo $tabr['Tables_in_'.array_keys($_GET)[1]]; ?></li></a>
									<?php } echo "</ul>"; } ?>
						
					<?php } elseif(isset($_GET['editor'])){ ?>
						
						<ul>
							<?php 
								$path = dirname(dirname(__FILE__))."/";
								$dir = scandir($path);
								// $block_ext = array('jpg', 'jpeg', 'png', 'ico', 'gif');
								 // or in_array($ext, $block_ext)
								 // $ext = end(explode('.', $file));
								foreach($dir as $file){
									if($file=="." or $file==".."){ continue; }
									if(is_dir($path.$file)){ 
										echo '<li><a class="dir" href="'.$aufile.'?editor&'.base64_encode($path.$file).'">'.$file.'</a></li>';
									} else { echo '<li><a href="'.$aufile.'?editor&'.base64_encode($path.$file).'">'.$file.'</a></li>'; }
								}
							?>							
						</ul>
					<?php } ?>
					
				
				
			</div> <!-- #leftbar -->
			<div id="rightbar">
				<h2 class="full">{ <?php echo $subhead; ?> }</h2>
				<div class="inner-content">
					<?php if(isset($_GET['login'])){ ?>
						<div class="login">
							<?php echo $msg; ?>
							<form action="" method="post">
								Username: <input type="text" name="user" placeholder="username" /><br><br>
								Password: <input type="password" name="pass" /><br><br>
								<input type="submit" name="login" value="Login" class="btn" />
							</form>
						</div>
					<?php } elseif(isset($_GET['settings'])){ ?>
						<div class="login">
							<?php echo $msg; ?>
							<form action="" method="post">
								<table class="config">
								<tr><td colspan=2><h2>Project Name</h2></td></tr>
								<tr><td>Change Project Name:</td><td><input type="text" name="name" value="<?php echo $values[0]; ?>" placeholder="Test & Co." /></td></tr>
								</table>
								
								<table class="config">
								<tr><td colspan=2><h2>MySQL Settings</h2></td></tr>
								<tr><td>MySQL Host:</td><td><input type="text" name="host" value="<?php echo $values[1]; ?>" /></td></tr>
								<tr><td>MySQL Username:</td><td><input type="text" name="user" value="<?php echo $values[2]; ?>" placeholder="MySQL User" /></td></tr>
								<tr><td>MySQL Password:</td><td><input type="password" value="<?php echo $values[3]; ?>" name="pass" /></td></tr>
								</table>
								
								<table class="config">
								<tr><td colspan=2><h2>Admin Password</h2></td></tr>
								<tr><td>Admin Password:</td><td><input type="password" name="adminPassword" title="minimum 4 characters" value="<?php echo $values[4]; ?>" /></td></tr>
								</table>
								
								<input type="submit" name="saveConfig" value="Save Settings" class="btn" />
							</form>
						</div>
					<?php } elseif(isset($_GET['editor']) and count($_GET)>1){ 
						echo "<a class='link' href='".$aufile."?editor&".base64_encode(dirname(base64_decode(array_keys($_GET)[1])))."'>Go Up Level</a><br>";
						if(is_dir(base64_decode(array_keys($_GET)[1]))){
							$path2 = base64_decode(array_keys($_GET)[1]).'/';
							$dir2 = scandir($path2);
							echo "<ul>";
							foreach($dir2 as $file2){
								if($file2=="." or $file2==".."){ continue; }
								if(is_dir($path2.$file2)){ 
									echo '<li><a class="dir" href="'.$aufile.'?editor&'.base64_encode($path2.$file2).'">'.$file2.'</a></li>';
								} else { echo '<li><a href="'.$aufile.'?editor&'.base64_encode($path2.$file2).'">'.$file2.'</a></li>'; }
							}
							echo "</ul>";
							if(count($dir2)<3){ echo "<a class='link' href='".$aufile."'?editor&".array_keys($_GET)[1]."&rmdir'>Delete Folder</a>"; }
						} else { 
							$ext = explode(".", base64_decode(array_keys($_GET)[1]));
							if(in_array($ext[count($ext)-1], array('php','js','css','txt','html','htm'))){
								echo $msg."<br><form action='' method='post'><textarea cols='90' rows='15' name='file_content'>".file_get_contents(base64_decode(array_keys($_GET)[1]))."</textarea><br><br><input type='hidden' name='file_path' value='".base64_decode(array_keys($_GET)[1])."' /><input class='btn' type='submit' name='update_file' value='Update File' /></form> 
								<br><a class='link' href='".base64_decode(array_keys($_GET)[1])."' download>Download File</a>
								<a class='link' href='".$aufile."?editor&".array_keys($_GET)[1]."&kick'>Delete File</a> ";
							} else { echo "<a class='link' href='".base64_decode(array_keys($_GET)[1])."' download>Download File</a> <a class='link' href='".$aufile."?editor&".array_keys($_GET)[1]."&kick'>Delete File</a>"; }
						}
					 } elseif(isset($_GET['database']) and count($_GET)>1){ 
							if(isset($_GET['create_database'])){
								echo $msg."<br><form action='' method='post'><input type='text' name='db_name' placeholder='New Database Name' /><br><br><input class='btn' type='submit' name='create_db' value='Create Database' /></form>";
							} elseif(isset($_GET['sql'])){
								echo $msg."<br><form action='' method='post'>Select Database:<br><select required name='sql_dbname'><option value=''>Select Database</option>";
								$dbq = mysqli_query($conn, "SHOW DATABASES");
								$tabs=array(); $fields=array();
								while($dbr = mysqli_fetch_assoc($dbq)){
									if($dbr['Database']=='mysql' or $dbr['Database']=='information_schema' or $dbr['Database']=='performance_schema'){ continue; }
									$connf = mysqli_connect($values[1], $values[2], $values[3], $dbr['Database']);
									$tabq = mysqli_query($conn, "SHOW TABLES from ".$dbr['Database']);
									$t=0;
									while($tabr=mysqli_fetch_assoc($tabq)){
										$tabs[$dbr['Database']][$t] = $tabr['Tables_in_'.$dbr['Database']];
										$fq = mysqli_query($connf, "SHOW COLUMNS FROM ".$tabr['Tables_in_'.$dbr['Database']]);
										$f=0;
										while($fr=mysqli_fetch_assoc($fq)){
											$fields[$dbr['Database']."__".$tabr['Tables_in_'.$dbr['Database']]][$f] = $fr['Field'];
											$f++;
										}
										$t++;
									}
									echo "<option value='".$dbr['Database']."'>".$dbr['Database']."</option>";
								}
								echo "</select><br><br>";
								foreach($tabs as $ki => $tab){ echo "<div class='tabs ".$ki."'>TABLES: <span>".implode("</span> | <span>",$tab)."</span></div>";}
								echo "<br>";
								foreach($fields as $kif => $field){ echo "<div class='fields ".$kif."'>FIELDS: <span>".implode("</span> | <span>",$field)."</span></div>";}
								//$fq = mysqli_query($conn, "SHOW COLUMNS FROM ".$tabr['Tables_in_'.$dbr['Database']]);
								//while($fr=mysqli_fetch_assoc($fq)){var_dump($fr);}
								echo "<br><br><textarea required name='sql_query' style='width:750px;height:60px;' placeholder='SQL Query here'></textarea><br><br><input class='btn' type='submit' name='submit_sql' value='Run Query' /></form><hr />";
								if(isset($_POST['submit_sql'])){
									$conn2 = mysqli_connect($values[1], $values[2], $values[3], $_POST['sql_dbname']);
									$qr = $_POST['sql_query'];
									if(strtolower(substr($qr, 0, 6))=="delete"){ echo "Can't run delete query."; }
									else{ $q = mysqli_query($conn2, $qr);
										if(strtolower(substr($qr, 0, 6))=="select"){
											$res = mysqli_num_rows($q);
											if($res>0){
												echo "<h3>".$res." Results</h3><table id='no_update'><tr>";
												while($rf=mysqli_fetch_field($q)){
													echo "<th>".$rf->name."</th>";
												}
												echo "</tr>";
												while($r=mysqli_fetch_assoc($q)){
													echo "<tr>";
													foreach($r as $ckey => $cell){
														echo "<td>".$cell."</td>";
													}
													echo "</tr>";
												}
												echo "</table>";
											} else { echo "No Results!"; }
										} else {
											echo "Rows affected: ".mysqli_affected_rows($conn2);
										}
									}
								}
							}
							if(count($_GET)==2 and !isset($_GET['create_database']) and !isset($_GET['sql'])){ ?>
							
								<?php $tabq = mysqli_query($conn, "SHOW TABLES from ".array_keys($_GET)[1]); 
											if(mysqli_num_rows($tabq)>0){
											echo "<h2>Select Table</h2><ul>";
											while($tabr = mysqli_fetch_assoc($tabq)){
									?>
									<a href="<?=$aufile?>?database&<?php echo array_keys($_GET)[1].'&'.$tabr['Tables_in_'.array_keys($_GET)[1]].'&0'; ?>"><li><?php echo $tabr['Tables_in_'.array_keys($_GET)[1]]; ?></li></a>
									<?php } echo "</ul>"; } else{ echo "No Tables in this Database."; } ?>
									
						<?php } if(count($_GET)>2){
								echo "<a class='link' href='".$aufile."?database&".array_keys($_GET)[1]."&".array_keys($_GET)[2]."&drop'>Delete Table</a>
								<a class='link' href='".$aufile."?database&".array_keys($_GET)[1]."&".array_keys($_GET)[2]."&truncate'>Empty Table</a><br><br>";
								
								$conn2 = mysqli_connect($values[1], $values[2], $values[3], array_keys($_GET)[1]);
								$records = 50;
								$tableq = mysqli_query($conn2, "select * from ".array_keys($_GET)[2]." limit ".($records*array_keys($_GET)[3]).', '.$records);
								$next = mysqli_num_rows(mysqli_query($conn2, "select * from ".array_keys($_GET)[2]." limit ".($records*(array_keys($_GET)[3]+1)).', '.$records));
								
								echo "<table><tr>";
								//SHOW KEYS FROM table WHERE Key_name = 'PRIMARY'
								$ff = 0;
								while($fieldr=mysqli_fetch_field($tableq)){
									if($ff==0){$ffield=$fieldr->name;}
									echo "<th>".$fieldr->name."</th>";
									$ff++;
								}
								echo "<th></th></tr>";
								
								while($row=mysqli_fetch_assoc($tableq)){
									echo "<tr id='".$ffield."___".$row[$ffield]."'>";
										foreach($row as $ckey => $cell){
											echo "<td class='".$ckey."'>".$cell."</td>";
										}
									echo "<td><span class='del'><a href='".$aufile."?database&".array_keys($_GET)[1]."&".array_keys($_GET)[2]."&del&".$ffield."___".$row[$ffield]."'>&#9760;</a></span></td></tr>";
								}
								echo "</table><div class='pagination'>";
								if(array_keys($_GET)[3]>0){ echo "<a class='link' href='".$aufile."?database&".array_keys($_GET)[1]."&".array_keys($_GET)[2]."&".(array_keys($_GET)[3]-1)."'>Previous Page</a>"; }
								if(!empty($next)){ echo "<a class='link' href='".$aufile."?database&".array_keys($_GET)[1]."&".array_keys($_GET)[2]."&".(array_keys($_GET)[3]+1)."'>Next Page</a>"; }
								echo "</div>";
						?>
	
					 <?php } } ?>
				</div> <!-- .inner-content -->
			</div> <!-- #rightbar -->
		</div> <!-- #content -->
		<div id="footer">
			Developed by: <a href="mailto:vuabid@hotmail.com">Abid Ali</a> and Disturbed by: <a href="mailto:sajjad@vistabit.com">M. Sajjad</a>
		</div>
	</div> <!-- #container -->
	<script type="text/javascript">
		$(function(){
			$(document).on("dblclick","table:not(#no_update) tbody tr td:not(:first-child,:last-child)",function(){
				var t = $(this).text();
				var c = $(this).attr("class");
				var f = $(this).parent("tr").attr("id");
				$(this).html("<form id='upform' action='' method='post'><input type='text' name='nvalue' class='edit' value='"+t+"' /><input type='hidden' value='"+t+"' class='prev' /><input type='hidden' value='"+c+"___"+f+"' name='details' /></form>");
				$(".edit").focus();
			})
			$(document).on("blur","table tbody tr td input",function(){
				var v = $(this).val();
				var p = $(".prev").val();
				var d = $(this).attr("id");
				if(v!=p){ $("#upform").submit(); }
				else{$(this).parent("form").parent("td").text(p);}
				//alert(v+p+d);
				//$(this).parent("td").text(v);
			})
			$(".tabs, .fields").hide();
			$(document).on("change","select[name=sql_dbname]", function(){
				var t = $(this).val();
				$(".tabs, .fields").hide();
				$("."+t).show('slow');
			})

            $(".tabs span").click(function () {
            	$(".tabs span").css("background-color","white");
              	$(this).css("background-color","yellow");
              	var str = $(this).text().trim();
              	var tab = $("select[name=sql_dbname]").val();
              	//alert( str );
              	$(".fields").hide();
              	$("."+tab+"__"+str).show("slow");
            });
		})
	</script>
</body>
</html>