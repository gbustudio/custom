<?php
    require_once("includes/initialize.php");
    $datetime = $_GET['on'];
    $room_id = $_GET['room'];
    $room = get_room($room_id);
    $hotel = hotel_details($room->hotel_id);
    if(!validateDate($datetime) or strtotime($datetime)<time()){ $datetime=date("m/d/Y"); }
    require("header.php");
?>
       <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-md-9">
                        
                        <div id="hotel-features" style="margin-top: -0px;" class="tab-container">
                            <div class="tab-content">
                                
                                <form action="" method="post" id="reservation_form_top">
                                <div class="tab-pane active" id="hotel-availability">
                                  <h2><?php echo $hotel->hotel_name;?> 
                                  <sup style="color: #090;font-weight: bold; font-size: 12px; text-decoration: overline"><?php echo $_LANGUAGE['rooms_available']; ?></sup>
                                  <a href="/hotel/<?php echo $hotel->permalink; ?>/" style="float:right;font-size:11px;color:#434a50 !important;"><?php echo $_LANGUAGE['back_to_reservation_page']; ?></a>
                                  </h2>
                                    <div class="room-list listing-style3 hotel">
                                        <?php echo hotel_room_detail($room_id); ?>
                                    </div>
                                    <div class="update-search clearfix" style="display:none;">
                                        <div class="col-md-5">
                                            <h4 class="title"><?php echo $_LANGUAGE['when']; ?></h4>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label><?php echo $_LANGUAGE['check_in']; ?></label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="mm/dd/yy" class="input-text full-width" id="check_in_date" name="check_in_date" onChange="do_action()" value="<?php echo $datetime; ?>" min="<?php echo date('m/d/Y');?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <label><?php echo $_LANGUAGE['check_out']; ?></label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" placeholder="mm/dd/yy" class="input-text full-width" id="check_out_date" name="check_out_date" value="<?php $next=strtotime($datetime)+(86400*2); echo date("m/d/Y", $next); ?>" onChange="do_action()" min="<?php echo date('m/d/Y');?>" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <h4 class="title"><?php echo $_LANGUAGE['who']; ?></h4>
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <label><?php echo $_LANGUAGE['rooms']; ?></label>
                                                    <div class="selector">
                                                        <select class="full-width" id="num_rooms" onChange="do_action()" name="num_rooms">
                                                            <option value="1" selected="selected">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                            <option value="5">05</option>
                                                            <option value="6">06</option>
                                                            <option value="7">07</option>
                                                            <option value="8">08</option>
                                                            <option value="9">09</option>
                                                            <option value="10">10</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label><?php echo $_LANGUAGE['adults']; ?></label>
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label><?php echo $_LANGUAGE['kids']; ?></label>
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="1">01</option>
                                                            <option value="2">02</option>
                                                            <option value="3">03</option>
                                                            <option value="4">04</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <h4 class="visible-md visible-lg">&nbsp;</h4>
                                            <label class="visible-md visible-lg">&nbsp;</label>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <button data-animation-duration="1" data-animation-type="bounce" class="full-width icon-check animated" type="button"><?php echo $_LANGUAGE['search_now']; ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <style type="text/css">
                                      table.boocking-check-celander {
                                          border-bottom: solid 1px #e8e8e8;
                                      }
                                      table.boocking-check-celander td{border-bottom: 1px solid #d1d1d1;}
                                      table.boocking-check-celander td.selected{background: #eee;}
                                      table.boocking-check-celander th.date_navigation,th.date_nav,table.boocking-check-celander table th {
                                          background-color: inherit !important;
                                      }
                                      table.boocking-check-celander .hot {
                                          color: #f60;
                                      }
                                      table.boocking-check-celander th.weakend {
                                          background-color: #2a2f34;
                                      }
                                      table.boocking-check-celander th {
                                          background-color: #434a50;
                                          height: 60px;
                                          padding: 0 5px;
                                          text-align: left;
                                          vertical-align: middle;
                                          color: #fff;
                                      }
                                      table.boocking-check-celander th.date {
                                          width: 40px;
                                      }
                                      table.boocking-check-celander th .day_name {
                                          color: #fff;
                                          font-size: 11px;
                                          font-weight: bold;
                                          white-space: nowrap;
                                      }
                                      table.boocking-check-celander th .day {
                                          color: #fff;
                                          font-size: 15px;
                                          margin: -5px 0;
                                      }
                                      table.boocking-check-celander th .month {
                                          color: #a8a8a8;
                                          font-size: 11px;
                                          font-weight: normal;
                                      }
                                      .disabled{
                                          opacity: .5;
                                          pointer-events: none;
                                      }
                                  </style>
                                <div id="availablity-celander" style="display:none;"></div>
                                <br><br>
                                <h2><?php echo $_LANGUAGE['choose_room_occupants']; ?></h2>
                                
                                <div class="pricing"  id="imbed_rooms"></div>
                                </div>
                                </form>
                            </div>
                        
                        </div>
                      <div class="tab-container" style="margin-top: 10px;">
                      
                      <?php if(!logged()){ ?>
                            <ul class="tabs">
                                <li class="active"><a data-toggle="tab" href="#login-tab"><?php echo $_LANGUAGE['login']; ?></a></li>
                                <li><a data-toggle="tab" href="#register-tab"><?php echo $_LANGUAGE['register']; ?></a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="login-tab" class="tab-pane fade in active">
                                <form  id="au_login_form" style="background:#fefefe;padding:20px;">
                                      <div class="person-information">
                                          <h2><?php echo $_LANGUAGE['login']; ?></h2>
                                          <div class="form-group row">
                                              <div class="col-sm-6 col-md-5">
                                                  <label><?php echo $_LANGUAGE['email_address']; ?></label>
                                                  <input type="email" name="email" required class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['email_address']; ?>" />
                                              </div>
                                              <div class="col-sm-6 col-md-5">
                                                  <label><?php echo $_LANGUAGE['password']; ?></label>
                                                  <input type="password" name="pass" required class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['password']; ?>" />
                                              </div>
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                          <div class="col-sm-6 col-md-5">
                                              <button name="signin" type="submit" class="full-width btn-large"><?php echo $_LANGUAGE['login']; ?></button>
                                          </div>
                                      </div>
                                    </form>
                                </div>
                                <div id="register-tab" class="tab-pane fade">
                                  
                                         <form id="au_signup_form" style="background:#fefefe;padding:20px;">
                                          <div class="person-information">
                                              <h2><?php echo $_LANGUAGE['register']; ?></h2>
                                              <div class="form-group row">
                                                  <div class="col-sm-12 col-md-12">
                                                      <label><?php echo $_LANGUAGE['title']; ?></label>
                                                      <div class="selector">
                                                          <select name="title" class="full-width" id="title">
                                                            <option value="Mr.">Mr.</option>
                                                            <option value="Ms.">Ms.</option>
                                                            <option value="Mrs.">Mrs.</option>
                                                            <option value="Miss.">Miss.</option>
                                                            <option value="Dr.">Dr.</option>
                                                            <option value="Prof.">Prof.</option>
                                                          </select>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="form-group row">
                                                  <div class="col-sm-6 col-md-6">
                                                      <label><?php echo $_LANGUAGE['first_name']; ?></label>
                                                      <input type="text" name="fname" required id="fname" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['first_name']; ?>" />
                                                  </div>
                                                  <div class="col-sm-6 col-md-6">
                                                      <label><?php echo $_LANGUAGE['last_name']; ?></label>
                                                      <input type="text" name="lname" required id="lname" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['last_name']; ?>" />
                                                  </div>
                                              </div>
                                              <div class="form-group row">
                                                  <div class="col-sm-6 col-md-12">
                                                      <label><?php echo $_LANGUAGE['street_address']; ?></label>
                                                      <input type="text" name="str_addr" required id="str_addr" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['street_address']; ?>" />
                                                  </div>
                                              </div>
                                              <div class="form-group row">
                                                  <div class="col-sm-6 col-md-6">
                                                    <label><?php echo $_LANGUAGE['city']; ?> &AMP; <?php echo $_LANGUAGE['zipcode']; ?></label>
                                                      <div class="constant-column-2">
                                                          <div class="selector">
                                                              <input type="text" required name="city"  id="city" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['city']; ?>" />
                                                          </div>
                                                          <div class="selector">
                                                              <input type="text" name="zipcode"  id="zipcode" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['zipcode']; ?>" />
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-6 col-md-6">
                                                    <label><?php echo $_LANGUAGE['state']; ?> &AMP; <?php echo $_LANGUAGE['country']; ?></label>
                                                      <div class="constant-column-2">
                                                          <div class="selector">
                                                              <input type="text" required name="state"  id="state" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['state']; ?>" />
                                                          </div>
                                                          <div class="selector">
                                                              <select required name="country" class="full-width">
                                                                <option value=""><?php echo $_LANGUAGE['select_country']; ?></option>
                                                                <?php echo countries_list(); ?>
                                                              </select>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="form-group row">
                                                  <div class="col-sm-6 col-md-6">
                                                      <label><?php echo $_LANGUAGE['phone']; ?></label>
                                                      <input type="text" required name="phone" id="phone" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['phone']; ?>" />
                                                  </div>
                                                  <div class="col-sm-6 col-md-6">
                                                      <label><?php echo $_LANGUAGE['fax']; ?></label>
                                                      <input type="text" name="fax" id="fax" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['fax']; ?>" />
                                                  </div>
                                              </div>
                                          </div>
                                         <!--  <hr /> -->
                                          <div class="extra-information">
                                              <!-- <h2>Extra Details</h2> -->
                                              <div class="form-group row">
                                                  <div class="col-sm-6 col-md-6">
                                                      <label><?php echo $_LANGUAGE['identity_type']; ?></label>
                                                      <input name="id_type" id="id_type" required type="text" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['identity_type']; ?>" />
                                                  </div>
                                                  <div class="col-sm-6 col-md-6">
                                                      <label><?php echo $_LANGUAGE['identity_number']; ?></label>
                                                      <input type="text" name="id_number" required id="id_number" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['identity_number']; ?>" />
                                                  </div>
                                              </div>
                                              <div class="form-group row">
                                                  <div class="col-sm-6 col-md-6">
                                                      <label><?php echo $_LANGUAGE['email_address']; ?></label>
                                                      <input type="email" name="email" required id="email" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['email_address']; ?>" />
                                                  </div>
                                                  <div class="col-sm-6 col-md-6">
                                                      <label><?php echo $_LANGUAGE['password']; ?></label>
                                                      <input type="password" required name="password" id="password" class="input-text full-width" value="" placeholder="<?php echo $_LANGUAGE['password']; ?>" />
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="form-group row">
                                              <div class="col-sm-10 col-md-12">
                                                  <button id="btn_exisitng_cust" type="submit" class="full-width btn-large"><?php echo $_LANGUAGE['signup']; ?></button>
                                              </div>
                                          </div>
                                          
                                        </form>
                                </div> <!-- register end  -->
                                </div> <!-- tab-content end  -->
                                <?php } ?>
                                 <!-- required start -->
                            <div class="tab-content" id="reservation_required_info" style="padding:20px;<?php if(!logged()){ echo "display:none;"; } ?>">
                            <form action="" method="post" id="reservation_form_required">
                              <input type="hidden" name="user" value="<?php if(logged()){ echo base64_encode($_SESSION['user_id']+123);} ?>" />
                              <input type="hidden" name="hotel" value="<?php echo $room->hotel_id; ?>">
                              <div class="form-group row">
                                  <div class="col-sm-6 col-md-5">
                                      <label><h2><?php echo $_LANGUAGE['payment_by']; ?></h2></label>
                                      <div class="col-sm-6 col-md-10">
                                        <?php $sql_query = $DB->query('select gateway_code,gateway_name from bsi_payment_gateway where enabled = 1 and hotel_id='.$room->hotel_id); ?>
                                        <?php while($result_payment_gateway = mysqli_fetch_array($sql_query)): ?>
                                          <?php if($result_payment_gateway['gateway_code']=="ipay88"): ?>
                                            <label class="radio"><input type="radio" required name="payment_type" id="payment_type_<?php echo $result_payment_gateway['gateway_code']; ?>_cc" value="cc" class="required" /> Credit Card</label>
                                            <label class="radio"><input type="radio" required name="payment_type" id="payment_type_<?php echo $result_payment_gateway['gateway_code']; ?>_mc" value="mc" class="required" /> Mandiri Clickpay</label>
                                            <label class="radio"><input type="radio" required name="payment_type" id="payment_type_<?php echo $result_payment_gateway['gateway_code']; ?>_xt" value="xt" class="required" /> XL Tunai</label>
                                          <?php elseif($result_payment_gateway['gateway_code']=="manual"): ?>
                                            <label class="radio"><input type="radio" required name="payment_type" id="payment_type_<?php echo $result_payment_gateway['gateway_code']; ?>" value="<?php echo $result_payment_gateway['gateway_code']; ?>" class="required" /> Manual</label>
                                          <?php endif; ?>
                                        <?php endwhile; ?>
                                      </div>
                                  </div>
                              </div>
                              <!-- <div class="paypal-information">
                                    <h2><?php echo "Paypal ".$_LANGUAGE['email_address']; ?></h2>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-5">
                                            <label><?php echo "Paypal ".$_LANGUAGE['email_address']; ?></label>
                                            <input type="email" name="payment_method_detail" class="input-text full-width" value="" placeholder="" />
                                        </div>
                                    </div>
                              </div> -->
                                   <!-- <div class="card-information">
                                    <h2><?php echo $_LANGUAGE['your_card_information']; ?></h2>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-5">
                                            <label><?php echo $_LANGUAGE['credit_card_type']; ?></label>
                                            <div class="selector">
                                                <select name="card_type" class="full-width">
                                                    <option value=""><?php echo $_LANGUAGE['select_a_card']; ?></option>
                                                    <option value="visa">Visa Card</option>
                                                    <option value="master">Master Card</option>
                                                    <option value="discovery">Discovery Card</option>
                                                    <option value="jcb">JCB Card</option>
                                                    <option value="american_express">American Express Card</option>
                                                </select><span class="custom-select full-width"><?php echo $_LANGUAGE['select_a_card']; ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-5">
                                            <label><?php echo $_LANGUAGE['card_holder_name']; ?></label>
                                            <input type="text" name="card_holder_name" class="input-text full-width" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-5">
                                            <label><?php echo $_LANGUAGE['card_number']; ?> (xxxxxxxxxxxxxxxx)</label>
                                            <input type="text" name="card_number" class="input-text full-width" value="" placeholder="">
                                        </div>
                                        <div class="col-sm-6 col-md-5">
                                            <label><?php echo $_LANGUAGE['card_identification_number']; ?> (Card CVV)</label>
                                            <input type="text" name="card_code" class="input-text full-width" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-5">
                                            <label><?php echo $_LANGUAGE['expiration_date']; ?></label>
                                            <div class="constant-column-2">
                                                <div class="selector">
                                                    <select name="card_expire_month" class="full-width">
                                                        <option value=""><?php echo $_LANGUAGE['month']; ?></option>
                                                        <option value="1">January</option>
                                                        <option value="2">February</option>
                                                        <option value="3">March</option>
                                                        <option value="4">April</option>
                                                        <option value="5">May</option>
                                                        <option value="6">June</option>
                                                        <option value="7">July</option>
                                                        <option value="8">August</option>
                                                        <option value="9">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select><span class="custom-select full-width"><?php echo $_LANGUAGE['month']; ?></span>
                                                </div>
                                                <div class="selector">
                                                    <select name="card_expire_year" class="full-width">
                                                        <option value=""><?php echo $_LANGUAGE['year']; ?></option>
                                                        <?php 
                                                            // for($y=0; $y<20; $y++){
                                                            //   $year = date("Y")+$y;
                                                            //   echo "<option value='$year'>$year</option>";
                                                            // }
                                                        ?>
                                                    </select><span class="custom-select full-width"><?php echo $_LANGUAGE['year']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div> -->
                                
                                    <div class="form-group row">
                                        <div class="col-sm-6 col-md-10">
                                            <label><?php echo $_LANGUAGE['message']; ?></label>
                                            <textarea rows="3" id='ar' name="message" class="input-text full-width" placeholder="<?php echo $_LANGUAGE['message']; ?>"></textarea>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                          <div class="checkbox">
                                              <label>
                                                  <input required type="checkbox" name="tos" id="tos" class="required"> <?php echo $_LANGUAGE['by_continuing_you_agree_terms']; ?> <a href="#"><span class="skin-color"><?php echo $_LANGUAGE['terms_conditions']; ?></span></a>.
                                              </label>
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                          <div class="col-sm-6 col-md-5">
                                          <input type="hidden" name="room" value="<?php echo $room_id; ?>" />
                                          <input type="hidden" name="promo" value="<?php if(isset($_SESSION['coupon_added']) and $_SESSION['coupon_added']!=""){ $promo = getCoupon($_SESSION['coupon_added']); echo $promo->promo_id; } ?>" />
                                              <button type="submit" id="btn_confirm_booking" class="full-width btn-large"><?php echo $_LANGUAGE['confirm_booking']; ?></button>
                                          </div>
                                      </div>
                                      </form>
                                    </div>
                            <!-- required end -->
                            
                        </div>                            
                    </div>
                 <?php require("sidebar.php"); ?>
                </div>
            </div>
        </section>
        
<div id="au_tooltips" style="position:absolute;border-radius:4px;border:2px solid red;width:330px;background:#000;color:white;display:none;">
 <p class="col-md-12 hot-deal-tooltip" style="background:#ff6106;"><em><b>Hot Deal!</b></em></p>
 <table class="table"><tr><td style="border-top:0px;padding:0px;padding-left:8px;">Minimum Stay</td>
 <td style="border-top:0px;padding:0px;padding-left:8px;" class="min-stay-tooltip"></td></tr>
<tr><td style="border-top:0px;padding:0px;padding-left:8px;">Availability</td>
   <td style="border-top:0px;padding:0px;padding-left:8px;" class="availability-tooltip"></td></tr>
<tr><td style="border-top:0px;padding:0px;padding-left:8px;">Included Occupancy</td>
   <td style="border-top:0px;padding:0px;padding-left:8px;" class="inc-occu-tooltip"></td></tr>
<tr><td style="border-top:0px;padding:0px;padding-left:8px;">Maximum Occupancy</td>
   <td style="border-top:0px;padding:0px;padding-left:8px;" class="max-occu-tooltip"></td></tr>
</table>
 <div class="col-md-offset-1" >
 <p class="details-tooltip"></p>
  <p style="font-size:small;">SUBJECT TO SERVICE CHARGE & GOVT TAX</p>
 </div>
</div>
        
    </div>
   <?php require("footer.php"); ?>
   
<script type="text/javascript">
      
	function embed_availablity_celander(datetime,room_id, to){
	jQuery.post("ajax-celander.php", {on: datetime, room: room_id, upto: to}, function(data){
	  jQuery('#availablity-celander').html(data);});
	}
	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
    function do_action(){
		tjq('#check_in_date').val(readCookie("cookie_check_in"));
		tjq('#check_out_date').val(readCookie("cookie_check_out"));
		tjq('#num_rooms').val(readCookie("cookie_jumlah_kamar"));
		
      var num_rooms = tjq('#num_rooms').val();
      var check_in_date = tjq('#check_in_date').val();
      var check_out_date = tjq('#check_out_date').val();
      if(check_in_date!=""){
        tjq('#check_out_date').attr('min', check_in_date);
        if(Date.parse(check_in_date)>=Date.parse(check_out_date)){
          var t = new Date(new Date(check_in_date).getTime() + 86400000);
          var ne = ("0" + (t.getMonth()+1)).slice(-2)+"/"+("0" + t.getDate()).slice(-2)+"/"+t.getFullYear();
          tjq('#check_out_date').val(ne);
          check_out_date = ne;
        }
      }
      if(check_out_date && check_in_date){
        tjq.post("jquery_imbed_rooms.php", {room: <?php echo $room_id;?>,num_rooms : num_rooms,check_in_date : check_in_date,check_out_date : check_out_date}, function(data){
          tjq('#imbed_rooms').html(data);});
		   jQuery.post("ajax-celander.php", {on: check_in_date, room: '<?php echo $room_id;?>', upto: check_out_date}, function(data){
          jQuery('#availablity-celander').html(data);});
      }
    }
    tjq(function(){
      embed_availablity_celander('<?php echo $datetime;?>','<?php echo $room_id;?>','<?php echo date("m/d/Y",strtotime($datetime)+86400);?>');
      tjq.post("jquery_imbed_rooms.php", {room: <?php echo $room_id;?>,num_rooms:1,check_in_date:'<?php echo $datetime;?>',check_out_date:'<?php $next=strtotime($datetime)+86400; echo date("m/d/Y", $next); ?>'}, function(data){
          tjq('#imbed_rooms').html(data);});
      
      function amount(text){
        return parseInt(text.replace(/\./g,'').replace("<?php echo user_currency($_COOKIE['currency']); ?>",""));
      }

	  function format_currency(angka){
		var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
		var rev2    = '';
		for(var i = 0; i < rev.length; i++){
			rev2  += rev[i];
			if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
				rev2 += '<?php echo user_currency($_COOKIE['currency'], "format"); ?>';
			}
		}
		return '<?php echo user_currency($_COOKIE['currency']); ?>' + rev2.split('').reverse().join('');
	  }

      // working for extras calculation
      tjq(document).on("change", ".extras_checkbox", function(){
        if(this.checked){
          var price = amount(tjq(this).data("price"));
          var id = tjq(this).attr("id");
          tjq("#"+id+"_quantity").val(1);
          tjq(this).parents("tr").children(".extra_amount").text(format_currency(price));
        }else{
          var price = amount(tjq(this).data("price"));
          var id = tjq(this).attr("id");
          tjq("#"+id+"_quantity").val("");
          var already = amount(tjq(this).parents("tr").children(".extra_amount").text());
          tjq(this).parents("tr").children(".extra_amount").text('<?php echo user_currency($_COOKIE['currency']); ?>0');
        }
        updateCalc();
      })
      tjq(document).on("change", ".qty", function(){
        var qty = parseInt(tjq(this).val());
        var id = tjq(this).attr("id").replace("_quantity","");
        var price = amount(tjq("#"+id).data("price"));
        if(qty==0 || qty=="0"){ 
          tjq("#"+id).attr('checked', false);
          var already = amount(tjq(this).parents("tr").children(".extra_amount").text());
          tjq(this).parents("tr").children(".extra_amount").text('<?php echo user_currency($_COOKIE['currency']); ?>0');
        } else {
          tjq("#"+id).attr('checked', true);
          var already = amount(tjq(this).parents("tr").children(".extra_amount").text());
          var total = qty*price;
          tjq(this).parents("tr").children(".extra_amount").text(format_currency(total));
        }
        updateCalc();
      })
      
      tjq(document).on("change", ".checkmembers", function(){
        var room = tjq(this).parents("ol").data("room");
        var already = amount(tjq(".extra_child_"+room+"_0").text());
        var adults = parseInt(tjq("#number_adults_"+room).val());
        var children = parseInt(tjq("#number_children_"+room).val());
        var allowed = tjq(this).parents("ol").data("allowed");
        var diff = (children+adults)-allowed;
        var price = diff*amount(tjq(this).parents("ol").data("extra-charge")); //2*50
        var pric_diff = price-already;
        var days = tjq(this).parents("ol").data("days");
        var grand_amount = price*days;
        if(diff>0){
          var c;
          for(c=0; c<days; c++){
            tjq(".extra_child_"+room+"_"+c).text(format_currency(price));
            var alreadysub = amount(tjq(".rrt_date_total_"+room+"_"+c).text());
            tjq(".rrt_date_total_"+room+"_"+c).text(format_currency(alreadysub+pric_diff));
          }
        } else {
          for(c=0; c<days; c++){
            tjq(".extra_child_"+room+"_"+c).text(format_currency(0));
            //var alreadysub = amount(tjq(".rrt_date_total_"+room+"_"+c).text());
            tjq(".rrt_date_total_"+room+"_"+c).text(tjq(".rrt_rate_date_"+room+"_"+c).text());
          }
        }
        updateCalc();
      })
      
      function updateCalc(){
        var total=0;
        var extras=0;
        var rooms=0;
        tjq("td[class^='extra_']").each(function(){
          extras += amount(tjq(this).text());
        })
        tjq("td[class^='rrt_rate_date']").each(function(){
          rooms += amount(tjq(this).text());
        })
        tjq("td.extra_total").text(format_currency(extras));
        tjq("td.room_total").text(format_currency(rooms));
        tjq("td>span#total_payment").text(format_currency(extras+rooms));
        tjq("#reservation_total").val(amount(tjq("#total_payment").text()));
      }
      // tjq(".card-information").hide();
      // tjq(".paypal-information").hide();
      // tjq(document).on("change", "#payment_type_co,#payment_type_auth,#payment_type_stripe,#payment_type_cc", function(e){
      //   tjq("select[name^='card_'], input[name^='card_']").attr("required", "required");
      //   tjq(".card-information").show('slow');
      //   tjq(".paypal-information").hide();
      // })
      // tjq(document).on("change", "#payment_type_pp,#payment_type_manual", function(e){
      //   tjq("select[name^='card_'], input[name^='card_']").removeAttr("required");
      //   tjq(".card-information").hide('slow');
      //   tjq(".paypal-information").hide();
      //   if(tjq(this).attr("id")=="payment_type_pp"){ 
      //     tjq(".paypal-information").show('slow');
      //     tjq(".card-information").hide();
      //     tjq("input[name=payment_method_detail]").attr("required", "required");
      //   }
      // })
      tjq(document).on("submit", "#reservation_form_required", function(e){
        e.preventDefault();
        tjq("#btn_confirm_booking").prop('disabled', true);
        var first = tjq("#reservation_form_top").serializeArray();
        var second = tjq("#reservation_form_required").serializeArray();
        var first = tjq.merge(first, second);
        first[first.length] = {name:"reserveIt",value:"1"};
        tjq.ajax({
          type:'POST',
          data: first,
          url:'includes/ajax_func.php',
          success:function(data) {
            //alert(data);
            console.log(data);
            data = data.split("||");
            if (data[0] == 1) {
              var mtd = data[1];
              if (mtd == "ippg") {
                window.location.href = data[2];
              }
            } else {
              alert("There is some problem in reservation. Please try again later!");
              window.location.href="hotel-detailed.php";
            }
            // if(data[0]==1){
            //   if(tjq("input[name=payment_type]:checked").val()=="pp"){ // testpp999@gmail.com
            //     var usd = data[1]; //https://www.sandbox.paypal.com/cgi-bin/webscr https://www.paypal.com/cgi-bin/webscr
            //     var pp = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick&business="+data[2]+"&lc=US&item_name=Booking%20ID:%20"+data[3]+"&amount="+usd+"%2e00&currency_code=USD&button_subtype=services&cancel_return=http://localhost/custom/hotel/hotel_page/new/hotel-detailed.php&return=http://localhost/custom/hotel/hotel_page/new/thank-you.php?booking="+data[3]+"&notify_url=http://localhost/custom/hotel/hotel_page/new/pp_process.php&no_note=0&bn=PP%2dBuyNowBF%3abtn_buynowCC_LG%2egif%3aNonHostedGuest";
            //       window.location.href=pp;
            //   }
            //   else if(tjq("input[name=payment_type]:checked").val()=="co"){
            //     var co = "https://www.2checkout.com/checkout/purchase?sid="+data[2]+"&mode=2CO&li_0_type=product&li_0_price="+data[1]+"%2e00&li_0_quantity=1&li_0_name=Booking%20ID:%20"+data[3]+"&demo=Y&"+data[4]; //demo=Y
            //       window.location.href=co;
            //   }
            //   else if(tjq("input[name=payment_type]:checked").val()=="manual"){
            //     alert("Your booking has been made, please cek your email to receive confirmation letter");
            //     window.location.href = "http://localhost/custom/hotel/profile.php#booking"
            //   }
            //   //alert("Your reservation has been received, we will review and contact you soon.");
            // }
            // else{
            //   alert("There is some problem in reservation. Please try again later!");
            //   window.location.href="hotel-detailed.php";
            // }
         }
        });
      })
	  do_action();
    })
    </script>
</body>
</html>
