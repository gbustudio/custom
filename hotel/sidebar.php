<div class="sidebar col-md-3">
    <article class="detailed-logo">
        <figure>
            <img width="114" height="85" src="<?php echo imageCheck($hotel->hotel_logo, "src", "logo"); ?>" alt="">
        </figure>
        <div class="details">
            <h2 class="box-title"><?php echo $hotel->hotel_name; ?><small><i class="soap-icon-departure yellow-color"></i><span class="fourty-space"><?php echo $hotel->hotel_address.", ".$hotel->hotel_info_city.", ".country($hotel->hotel_info_country_id); ?></span></small></h2>
            <span class="price clearfix">
                <small class="pull-left">avg/<?php echo $_LANGUAGE['night']; ?></small>
                <span class="pull-right"><?php echo amount(avg_hotel_rate($hotel->hotel_id)); ?></span>
            </span>
            <div class="feedback clearfix">
                <div class="five-stars-container" title="<?php echo hotel_reviews($hotel->hotel_id); ?>%"><span class="five-stars" style="width: <?php echo hotel_reviews($hotel->hotel_id); ?>%;"></span></div>
                <span class="review pull-right"><?php echo hotel_reviews($hotel->hotel_id,"count"); ?> <?php echo $_LANGUAGE['reviews']; ?></span>
            </div>
            <p class="description"><?php echo $hotel->hotel_short_description; ?></p>
            <a href="<?php echo $hotel->website; ?>" class="button yellow full-width uppercase btn-small"><?php echo $_LANGUAGE['visit_main_website']; ?></a>
        </div>
    </article>
    <div class="travelo-box contact-box">
        <h4><?php echo $_LANGUAGE['need']." ".$_LANGUAGE['help']; ?>?</h4>
        <p><?php echo $hotel->help_box_text; ?></p>
        <address class="contact-details">
            <span class="contact-phone"><i class="soap-icon-phone"></i> <?php echo $hotel->phone; ?></span>
            <br>
            <a class="contact-email" href="#"><?php echo $hotel->email; ?></a>
        </address>
    </div>
    <?php if($hotel->why_book_with_us){ ?>
    <div class="travelo-box book-with-us-box">
        <h4>Why choose us?</h4>
        <?php echo $hotel->why_book_with_us; ?>
    </div>
    <?php } ?>
</div>